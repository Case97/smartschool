/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.model;

import com.developerfriends.edu.smartschool.pojo.Classrooms;
import com.developerfriends.edu.smartschool.util.Dialogs;
import com.developerfriends.edu.smartschool.util.UtilClass;
import java.util.Iterator;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Tanieska
 */
public class ClassroomsModel implements EntitiesInterface<Classrooms> {

    Dialogs dialog = new Dialogs();

    UtilClass util = new UtilClass();

    private Session session;

    @Override
    public void save(Classrooms object) {
        Transaction tx = null;
        try {
            tx = getSession().beginTransaction();
            getSession().save(object);

            tx.commit();
            dialog.Alerta("Completo", "", "Se guardo correctamente el registro", Alert.AlertType.INFORMATION);

        } catch (RuntimeException e) {
            if (tx != null) {
                tx.rollback();
            }
            dialog.Alerta("Error", "", "Sucedio un error al guardar", Alert.AlertType.ERROR);
            throw e;
        } finally {
            closeSession();
        }
    }

    public ObservableList<Classrooms> readClassrooms(Boolean state) {
        assert getSession() != null;
        List results = getSession().createCriteria(Classrooms.class)
                .setProjection(Projections.projectionList()
                        .add(Projections.property("classroomId")) // 0
                        .add(Projections.property("codeClassroom"), "code") //1
                        .add(Projections.property("capacity"), "name") //2
                )
                .add(Restrictions.eq("state", state))
                .addOrder(Order.desc("code"))
                .list();
        return setClassrooms(results);
    }

    public ObservableList<Classrooms> findClassrooms(Boolean state, String text) {
        assert getSession() != null;
        List results = getSession().createCriteria(Classrooms.class)
                .setProjection(Projections.projectionList()
                        .add(Projections.property("classroomId")) // 0
                        .add(Projections.property("codeClassroom"), "code") //1
                        .add(Projections.property("capacity"), "name") //2
                )
                .add(Restrictions.eq("state", state))
                .add(Restrictions.like("codeClassroom", text + "%"))
                .addOrder(Order.desc("code"))
                .list();
        return setClassrooms(results);
    }

    private ObservableList<Classrooms> setClassrooms(List results) throws NumberFormatException {
        ObservableList<Classrooms> classrooms = FXCollections.observableArrayList();
        try {

            Iterator i = results.iterator();

            while (i.hasNext()) {
                Object[] obj = (Object[]) i.next();

                classrooms.add(
                        new Classrooms(
                                (Integer.parseInt(String.valueOf(obj[0]))),
                                String.valueOf(obj[1]),
                                (Integer.parseInt(String.valueOf(obj[2]))))
                );

            }
        } catch (java.lang.ClassCastException ex) {
            System.err.println("Error en el setClassrooms " + ex.getMessage());
        }

        return classrooms;
    }

    @Override
    public void update(Classrooms object) {
        Transaction tx = null;
        try {
            tx = getSession().beginTransaction();
            getSession().update(object);

            tx.commit();
            dialog.Alerta("Completo", "", "Se actualizo el registro", Alert.AlertType.INFORMATION);

        } catch (RuntimeException e) {
            if (tx != null) {
                tx.rollback();
            }
            dialog.Alerta("Error", "", "Sucedio un error al actualizar", Alert.AlertType.ERROR);
            throw e;
        } finally {
            closeSession();
        }
    }

    public void active(Classrooms object) {
        Transaction tx = null;
        try {
            tx = getSession().beginTransaction();
            getSession().update(object);

            tx.commit();
            dialog.Alerta("Completo", "", "Se habilito el registro", Alert.AlertType.INFORMATION);

        } catch (RuntimeException e) {
            if (tx != null) {
                tx.rollback();
            }
            dialog.Alerta("Error", "", "Sucedio un error al habilitar", Alert.AlertType.ERROR);
            throw e;
        } finally {
            closeSession();
        }
    }

    @Override
    public void delete(Classrooms object) {
        Transaction tx = null;
        try {
            tx = getSession().beginTransaction();
            getSession().update(object);

            tx.commit();
            dialog.Alerta("Completo", "", "Se deshabilito esa aula", Alert.AlertType.INFORMATION);

        } catch (RuntimeException e) {
            if (tx != null) {
                tx.rollback();
            }
            dialog.Alerta("Error", "", "Sucedio un error al actualizar", Alert.AlertType.ERROR);
            throw e;
        } finally {
            closeSession();
        }
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public void closeSession() {
        if ((session != null) && (session.isOpen())) {
            session.close();
        }
    }

}
