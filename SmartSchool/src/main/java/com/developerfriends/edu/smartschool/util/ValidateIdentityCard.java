/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Tanieska
 */
public class ValidateIdentityCard {

    private final String LETTER = "ABCDEFGHJKLMNPQRSTUVWXY";

    private String identityCard = null;

    /**
     * Retorna true si la cédula es valida false en caso contrario
     *
     * @return
     */
    public boolean isValidIdentityCard() {
        if (!isValidPrefix()) {
            return false;
        }

        if (!isValidDate()) {
            return false;
        }

        if (!isValidSuffix()) {
            return false;
        }

        if (!isValidLetter()) {
            return false;
        }

        return true;
    }

    /**
     * Retorna true si el prefijo de la c&eacute;dula es v&aacute;lida false en
     * caso contrario
     *
     * @return
     */
    public boolean isValidPrefix() {
        String prefix = this.getPrefixIdentityCard();

        if (prefix == null) {
            return false;
        }

        Pattern p = Pattern.compile("^[0-9]{3}$");
        Matcher m = p.matcher(prefix);
        if (!m.find()) {
            return false;
        }

        return true;
    }

    /**
     * Retorna true si la fecha de la c&eacute;dula es v&aacute;lida false en
     * caso contrario
     *
     * @return
     */
    public boolean isValidDate() {
        String date = this.getDateIdentiyCard();

        if (date == null) {
            return false;
        }

        Pattern pattern = Pattern.compile("^(0[1-9]|[12][0-9]|3[01])(0[1-9]|1[012])([0-9]{2})$");
        Matcher matcher = pattern.matcher(date);
        if (!matcher.find()) {
            return false;
        }

        // verificar si la fecha existe en el calendario
        try {
            DateFormat df = new SimpleDateFormat("ddMMyy");
            if (!date.equals(df.format(df.parse(date)))) {
                return false;
            }
        } catch (ParseException ex) {
            return false;
        }

        return true;
    }

    /**
     * Retorna true si el sufijo de la c&eacute;dula es v&aacute;lida false en
     * caso contrario
     *
     * @return
     */
    public boolean isValidSuffix() {
        String sufix = this.getSuffixIdentityCard();

        if (sufix == null) {
            return false;
        }

        Pattern p = Pattern.compile("^[0-9]{4}[A-Y]$");
        Matcher m = p.matcher(sufix);
        if (!m.find()) {
            return false;
        }

        return true;
    }

    /**
     * Retorna true si la letra de la c&eacute;dula es v&aacute;lida false en
     * caso contrario
     *
     * @return
     */
    public boolean isValidLetter() {
        String letterValid = null;
        String letter = this.getLetterIdentityCard();

        if (letter == null) {
            return false;
        }

        letterValid = String.valueOf(this.calculateLetter());

        return letterValid.equals(letter);
    }

    /**
     * Retorna un entero que representa la posici&oacute;n en la cadena LETRAS
     * de la letra final de una c&eacute;dula
     *
     * @return
     */
    public int getIndexLetter() {
        int indexLetter = 0;
        String identityWithoutLettter = this.getIdentiyCardWithoutLetter();
        long numberWithoutLetter = 0;

        if (identityWithoutLettter == null) {
            return -1;
        }

        try {
            numberWithoutLetter = Long.parseLong(identityWithoutLettter);
        } catch (NumberFormatException e) {
            return -1;
        }

        indexLetter = (int) (numberWithoutLetter - Math.floor((double) numberWithoutLetter / 23.0) * 23);

        return indexLetter;
    }

    /**
     * <p>
     * Calcular la letra al final de la c&eacute;dula nicaraguense.
     * </p><p>
     * Basado en el trabajo de: Arnoldo Suarez Bonilla - arsubo@yahoo.es
     * </p><p>
     * <a href="http://espanol.groups.yahoo.com/group/ptic-nic/message/873">http://espanol.groups.yahoo.com/group/ptic-nic/message/873</a>
     * Mie, 6 de Feb, 2008 2:39 pm
     * </p><p>
     * Es correcto, los ultimos 4 numeros de la c&eacute;dula son un consecutivo
     * de las personas que nacen en la misma fecha y municipio. La letra se
     * calcula con el siguiente algoritmo (yo se los envío en SQL).
     * <p>
     * <
     * pre>
     * declare  @cedula      varchar(20),
     *          &#64;val         numeric(13, 0),
     *          &#64;letra       char(1),
     *          &#64;Letras      varchar(20)
     *
     *          select @Letras = 'ABCDEFGHJKLMNPQRSTUVWXY'
     *          select @cedula = '0012510750012' --PARTE NUMERICA DE LA CEDULA SIN GUIONES
     *          --CALCULO DE LA LETRA DE LA CEDULA
     *          select @val = convert(numeric(13, 0), @cedula) - floor(convert(numeric(13, 0), @cedula) / 23) * 23
     *          select @letra = SUBSTRING(@Letras, @val + 1, 1)
     *          select @letra
     * </pre>
     *
     * @return Letra v&aacute;lida de c&eacute;dula dada
     */
    public char calculateLetter() {
        int indexLetter = getIndexLetter();

        if (indexLetter < 0 || indexLetter >= LETTER.length()) {
            return '?';
        }

        return LETTER.charAt(indexLetter);
    }

    /**
     * Fiajr el N&uacute;mero de C&eacute;dula
     *
     * @param identityCard N&uacute;mero de C&eacute;dula con o sin guiones
     */
    public void setIdentityCard(String identityCard) {
        identityCard = identityCard.trim().replaceAll("-", "");

        if (identityCard == null || identityCard.length() != 14) {
            this.identityCard = null;
        } else {
            this.identityCard = identityCard.trim().replaceAll("-", "").toUpperCase();
        }
    }

    /**
     * Retorna el N&uacute;mero de C&eacute;dula
     *
     * @return
     */
    public String getIdentityCard() {
        return identityCard;
    }

    /**
     * Retorna el prefijo de la c&eacute;dula
     *
     * @return
     */
    public String getPrefixIdentityCard() {
        if (identityCard != null) {
            return identityCard.substring(0, 3);
        } else {
            return null;
        }
    }

    /**
     * Retorna la fecha en la c&eacute;dula
     *
     * @return
     */
    public String getDateIdentiyCard() {
        if (identityCard != null) {
            return identityCard.substring(3, 9);
        } else {
            return null;
        }
    }

    /**
     * Retorna el sufijo en la c&eacute;dula
     *
     * @return
     */
    public String getSuffixIdentityCard() {
        if (identityCard != null) {
            return identityCard.substring(9, 14);
        } else {
            return null;
        }
    }

    /**
     * Retorna la letra de la c&eacute;dula
     *
     * @return
     */
    public String getLetterIdentityCard() {
        if (identityCard != null) {
            return identityCard.substring(13, 14);
        } else {
            return null;
        }
    }

    /**
     * Retorna la c&eacute;dula sin la letra de validaci&oacute;n
     *
     * @return
     */
    public String getIdentiyCardWithoutLetter() {
        if (identityCard != null) {
            return identityCard.substring(0, 13);
        } else {
            return null;
        }
    }

}
