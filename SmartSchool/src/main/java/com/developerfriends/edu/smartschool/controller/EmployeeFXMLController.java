/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.controller;

import com.crazycode.custom.elements.SearchField;
import com.crazycode.mask.elements.EmailField;
import com.crazycode.mask.elements.MaskField;
import com.developerfriends.edu.smartschool.model.EmployeesModel;
import com.developerfriends.edu.smartschool.pojo.Employees;
import com.developerfriends.edu.smartschool.pojo.Persons;
import com.developerfriends.edu.smartschool.util.Dialogs;
import com.developerfriends.edu.smartschool.util.HibernateUtil;
import com.developerfriends.edu.smartschool.util.ImageClass;
import com.developerfriends.edu.smartschool.util.UtilClass;
import com.developerfriends.edu.smartschool.util.StageController;
import com.developerfriends.edu.smartschool.util.ValidateIdentityCard;
import com.developerfriends.edu.smartschool.util.Validation;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author 27gma
 */
public class EmployeeFXMLController {

    Dialogs dialog = new Dialogs();
    PrincipalFXMLController principal = new PrincipalFXMLController();
    StageController stController = new StageController();
    EmployeesModel employeesModel = new EmployeesModel();
    ValidateIdentityCard validCard = new ValidateIdentityCard();
    Validation validation = new Validation();
    UtilClass util = new UtilClass();
    private Stage primaryStage;
    private SessionFactory sFactory;
    private int personId;
    private int employeeId;
    private boolean newImage;
    private final Boolean ACTIVE = Boolean.TRUE;
    private final Boolean INACTIVE = Boolean.FALSE;
    @FXML
    private TextField txtFirstName;
    @FXML
    private TextField txtSecondName;
    @FXML
    private TextField txtFirstLastName;
    @FXML
    private TextField txtSecondLastName;
    @FXML
    private MaskField txtIdentityCard;
    @FXML
    private MaskField txtPhone;
    @FXML
    private EmailField txtEmail;
    @FXML
    private TextArea txtAddress;
    @FXML
    private JFXRadioButton rbMale;
    @FXML
    private JFXRadioButton rbFemale;
    @FXML
    private AnchorPane apElements;
    @FXML
    protected ImageView imgViewEmployee;
    @FXML
    private ComboBox<String> cmbRol;
    @FXML
    private TableView<Employees> tableEmployee;
    @FXML
    private TableColumn<Employees, String> colEmployee;
    @FXML
    private TableColumn<Employees, String> colPosition;
    @FXML
    private TableColumn<Employees, String> colGender;
    @FXML
    private TableColumn<Employees, String> colIdentityCard;
    @FXML
    private TableColumn<Employees, String> colEmail;
    @FXML
    private TableColumn<Employees, String> colPhone;
    @FXML
    private TableColumn<Employees, String> colAddress;
    @FXML
    private TableColumn<Employees, String> colAge;
    @FXML
    private JFXButton btnSave;
    @FXML
    private JFXButton btnSaveChange;
    @FXML
    private JFXButton btnCancel;
    @FXML
    private JFXButton btnNew;
    @FXML
    private JFXButton btnUpate;
    @FXML
    private JFXButton btnDismiss;
    @FXML
    private JFXButton btnBack;
    @FXML
    private HBox hBBtn;
    @FXML
    private JFXButton btnSelect;
    @FXML
    private JFXButton btnTake;
    @FXML
    private SearchField txtSearch;

    public void initialize() {
        Icons();
        getSessionFactory();
        apElements.setVisible(false);
        cmbRol.setItems(roles());
        imgViewEmployee.setImage(imageDefault());
        this.btnUpate.setDisable(true);
        this.btnDismiss.setDisable(true);
        colEmployee.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getPersons().getPersonName()));
        colPosition.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(util.getRoles(cellData.getValue().getRole())));
        colPhone.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getPhone()));
        colAddress.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getPersons().getAddress()));
        colAge.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(String.valueOf(util.age(cellData.getValue().getPersons().getBirthday()))));
        colGender.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(util.gender(cellData.getValue().getPersons().getGender())));
        colIdentityCard.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getIdentityCard()));
        colEmail.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getEmail()));
        search();
        loadEmployee();
        tableEmployee.setOnMousePressed((MouseEvent event) -> {
            if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
                employeeFile();
            }
        });
    }

    public void employeeFile() {
        FXMLLoader load = new FXMLLoader();
        URL url = EmployeeFileFXMLController.class.getResource("/fxml/EmployeeFileFXML.fxml");
        Stage st = stController.showStage(url, "Empleado", primaryStage, load);
        EmployeeFileFXMLController controller = load.getController();
        controller.setEmployee((Employees) tableEmployee.getSelectionModel().getSelectedItem());
        st.initModality(Modality.APPLICATION_MODAL);
        st.resizableProperty().setValue(Boolean.FALSE);
        st.showAndWait();
    }

    public void Icons() {
        stController.Icon(btnNew, ImageClass.newElement);
        stController.Icon(btnUpate, ImageClass.updateElement);
        stController.Icon(btnDismiss, ImageClass.disableElement);
        stController.Icon(btnCancel, ImageClass.cancel);
        stController.Icon(btnSave, ImageClass.save);
        stController.Icon(btnSaveChange, ImageClass.saveChange);
        stController.Icon(btnSelect, ImageClass.selectPicture);
        stController.Icon(btnTake, ImageClass.camera);
        stController.Icon(btnBack, ImageClass.back);
    }

    private void loadEmployee() {
        try {
            employeesModel.setSession(openSession());
            tableEmployee.setItems(employeesModel.readEmployee());
        } catch (NullPointerException ex) {
            System.err.println("Error en loadEmployee " + ex.getMessage());
        }

    }

    private void findEmployee(String text) {
        try {
            employeesModel.setSession(openSession());
            tableEmployee.setItems(employeesModel.findEmployees(text));
        } catch (NullPointerException ex) {
            System.err.println("Error en findEmployee " + ex.getMessage());
        }
    }

    @FXML
    void dismissEmployee(ActionEvent event) {
        Employees selectEmployee = (Employees) tableEmployee.getSelectionModel().getSelectedItem();
        if (selectEmployee != null) {
            if (dialog.Confirm("Despedir", "", "¿Esta seguro que quiere despedir a este empleado?")) {
                selectEmployee.getPersons().setState(INACTIVE);
                employeesModel.setSession(openSession());
                employeesModel.delete(selectEmployee);
                loadEmployee();
            }
        } else {
            dialog.Alerta("Advertencia", "No se ha encontrado el empleado", "Por favor seleccione un empleado en la tabla", Alert.AlertType.WARNING);
        }
    }

    @FXML
    void newEmployee(ActionEvent event) {
        this.apElements.setVisible(true);
        enableButton(1, true);
        enableButton(2, true);
        this.tableEmployee.setDisable(true);
        showButton(1);
    }

    @FXML
    void updateEmployee(ActionEvent event) {
        Employees selectEmployee = (Employees) tableEmployee.getSelectionModel().getSelectedItem();
        if (selectEmployee != null) {
            this.apElements.setVisible(true);
            enableButton(1, true);
            enableButton(2, true);
            this.tableEmployee.setDisable(true);
            showButton(2);
            setEmployee(selectEmployee);
        } else {
            dialog.Alerta("Advertencia", "No se ha encontrado el empleado", "Por favor seleccione un empleado en la tabla", Alert.AlertType.WARNING);
        }
    }

    @FXML
    void save(ActionEvent event) {
        if (isEmpty()) {
            return;
        }
        if (isOkFiled()) {
            return;
        }
        if (isOkIdentityCard()) {
            return;
        }
        if (isPhoto()) {
            return;
        }

        Persons p = getPerson();
        Employees e = getEmployee(p);
        employeesModel.setSession(openSession());
        employeesModel.save(e);
        apElements.setVisible(false);
        enableButton(2, false);
        loadEmployee();
        clear();
    }

    private void setEmployee(Employees emp) {
        this.txtFirstName.setText(emp.getPersons().getFirstName());
        this.txtSecondName.setText(emp.getPersons().getSecondName());
        this.txtFirstLastName.setText(emp.getPersons().getFirstLastname());
        this.txtSecondLastName.setText(emp.getPersons().getSecondLastname());
        this.txtPhone.setText(emp.getPhone());
        this.txtEmail.setText(emp.getEmail());
        this.txtIdentityCard.setText(emp.getIdentityCard());
        this.txtAddress.setText(emp.getPersons().getAddress());
        this.cmbRol.getSelectionModel().select(emp.getRole());
        this.imgViewEmployee.setImage(util.getImage(emp.getPersons().getPhoto()));
        this.rbMale.setSelected(emp.getPersons().getGender());
        this.rbFemale.setSelected(!emp.getPersons().getGender());
        setPersonId(emp.getPersons().getPersonId());
        setEmployeeId(emp.getEmployeeId());
    }

    private Persons getPerson() {
        BufferedImage img = SwingFXUtils.fromFXImage(imgViewEmployee.getImage(), null);
        Boolean gender = (this.rbMale.isSelected()) ? Boolean.TRUE : Boolean.FALSE;
        Persons person = new Persons(this.txtFirstName.getText(),
                this.txtSecondName.getText(),
                this.txtFirstLastName.getText(),
                this.txtSecondLastName.getText(),
                gender,
                util.getBirthday(this.txtIdentityCard.getText()),
                this.txtAddress.getText(), util.saveImage(img),
                ACTIVE);
        return person;
    }

    private Employees getEmployee(Persons person) {
        Employees employess = new Employees(person,
                cmbRol.getSelectionModel().getSelectedIndex(),
                this.txtIdentityCard.getText(),
                this.txtPhone.getText(),
                this.txtEmail.getText());
        return employess;
    }

    @FXML
    void saveChange(ActionEvent event) {
        if (isEmpty()) {
            return;
        }
        if (isOkFiled()) {
            return;
        }
        if (isOkIdentityCard()) {
            return;
        }

        Persons person = getPerson();
        person.setPersonId(getPersonId());
        Employees employees = getEmployee(person);
        employees.setEmployeeId(getEmployeeId());
        employeesModel.setSession(openSession());
        employeesModel.update(employees);
        apElements.setVisible(false);
        enableButton(2, false);
        loadEmployee();
        clear();
    }

    private void search() {
        txtSearch.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (newValue.isEmpty()) {
                loadEmployee();
            } else {
                findEmployee(newValue);
            }
        });
    }

    @FXML
    private void Cancel(ActionEvent event) {
        apElements.setVisible(false);
        enableButton(1, true);
        enableButton(2, false);
        clear();
    }

    @FXML
    private void back(ActionEvent event) {
        principal.back(event);
    }

    @FXML
    private void checkMale(ActionEvent event) {
        util.enableRB(rbMale, rbFemale);
    }

    @FXML
    private void checkFemale(ActionEvent event) {
        util.enableRB(rbFemale, rbMale);
    }

    @FXML
    private void openFile(ActionEvent event) {
        Image img = util.openFile(event);
        if (img != null) {
            imgViewEmployee.setImage(img);
            setNewImage(true);
        }
    }

    @FXML
    private void openCamera(ActionEvent event) {
        FXMLLoader load = new FXMLLoader();
        URL url = CameraFXMLController.class.getResource("/fxml/CameraFXML.fxml");
        Stage stage = stController.showStage(url, "Camara", primaryStage, load);
        CameraFXMLController controller = load.getController();
        controller.setStage(stage);
        controller.setEmployee(this);
        controller.setType(1);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.resizableProperty().setValue(Boolean.FALSE);
        stage.show();
    }

    private boolean isEmpty() {
        if (this.txtFirstName.getText().isEmpty()
                || this.txtSecondName.getText().isEmpty()
                || this.txtFirstLastName.getText().isEmpty()
                || this.txtSecondLastName.getText().isEmpty()
                || this.txtPhone.getText().isEmpty()
                || this.txtIdentityCard.getText().isEmpty()
                || this.cmbRol.getSelectionModel().isEmpty()
                || this.txtAddress.getText().isEmpty()
                || ((!this.rbFemale.isSelected()) && (!this.rbMale.isSelected()))) {
            dialog.Alerta("Smart School", "Error", "No puede dejar campos vacios", Alert.AlertType.ERROR);
            return true;
        }
        return false;
    }

    private boolean isOkFiled() {
        List<Boolean> okField = new ArrayList<>();
        okField.add(validation.isEmail(this.txtEmail.getText()));
        okField.add(validation.isIdentityCard(this.txtIdentityCard.getText()));
        okField.add(validation.isPhone(this.txtPhone.getText()));
        if (okField.contains(false)) {
            dialog.Alerta("Smart School", "Campos mal escritos",
                    ((okField.get(0)) ? "" : "Correo mal escrito") + "\n"
                    + ((okField.get(1)) ? "" : "Cédula mal escrita") + "\n"
                    + ((okField.get(2)) ? "" : "Teléfono mal escrito"),
                    Alert.AlertType.WARNING);
            return true;
        }
        return false;
    }

    private boolean isOkIdentityCard() {
        validCard.setIdentityCard(this.txtIdentityCard.getText());
        if (!validCard.isValidIdentityCard()) {
            dialog.Alerta("Smart School", "Cédula", "La cédula insertada no es valida", Alert.AlertType.WARNING);
            return true;
        }
        return false;
    }

    private boolean isPhoto() {
        if (!isNewImage()) {
            dialog.Alerta("Smart School", "Foto", "Debe insertar una foto del empleado", Alert.AlertType.WARNING);
            return true;
        }
        return false;
    }

    private void clear() {
        this.txtFirstName.setText("");
        this.txtSecondName.setText("");
        this.txtFirstLastName.setText("");
        this.txtSecondLastName.setText("");
        this.txtEmail.setText("");
        clearMask(txtPhone);
        clearMask(txtIdentityCard);
        this.txtAddress.setText("");
        this.cmbRol.getSelectionModel().clearSelection();
        this.imgViewEmployee.setImage(imageDefault());
        this.tableEmployee.setDisable(false);
        this.rbFemale.setSelected(false);
        this.rbMale.setSelected(false);
        setNewImage(false);
    }

    @FXML
    private void enable(MouseEvent event) {
        this.btnUpate.setDisable(false);
        this.btnDismiss.setDisable(false);
    }

    private Image imageDefault() {
        return new Image(getClass().getResource(ImageClass.defaultUser_A()).toExternalForm());
    }

    private Session openSession() {
        return sFactory.openSession();
    }

    private void getSessionFactory() {
        sFactory = HibernateUtil.getSessionFactory();
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public boolean isNewImage() {
        return newImage;
    }

    public void setNewImage(boolean isNewImage) {
        this.newImage = isNewImage;
    }

    private ObservableList<String> roles() {
        ObservableList<String> roles = FXCollections.observableArrayList();
        roles.addAll(util.setRoles());
        return roles;
    }

    private void enableButton(int type, boolean opc) {
        switch (type) {
            case 1:
                this.btnUpate.setDisable(opc);
                this.btnDismiss.setDisable(opc);
                break;
            case 2:
                this.btnNew.setDisable(opc);
                this.btnBack.setDisable(opc);
                this.txtSearch.setDisable(opc);
                break;
        }
    }

    private void showButton(int opc) {
        switch (opc) {
            //Nuevo
            case 1:
                add_remove(2, btnCancel);
                add_remove(2, btnSaveChange);
                add_remove(1, btnSave);
                add_remove(1, btnCancel);
                break;
            //Actualizar    
            case 2:
                add_remove(2, btnCancel);
                add_remove(2, btnSave);
                add_remove(1, btnSaveChange);
                add_remove(1, btnCancel);

                break;
        }
    }

    private void add_remove(int opc, Node node) {
        switch (opc) {
            //Agregar
            case 1:
                if (!this.hBBtn.getChildren().contains(node)) {
                    this.hBBtn.getChildren().add(node);
                }
                break;
            //Remover    
            case 2:
                if (this.hBBtn.getChildren().contains(node)) {
                    this.hBBtn.getChildren().remove(node);
                }
                break;
        }
    }

    private void clearMask(MaskField txt) {
        if (txt.getText().isEmpty()) {
            txt.setText("");
        } else {
            txt.replaceText(0, txt.lengthProperty().get(), "");
        }
    }

}
