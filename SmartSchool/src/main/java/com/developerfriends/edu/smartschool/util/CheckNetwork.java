/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.util;

import java.io.IOException;
import java.net.Socket;

/**
 *
 * @author Tanieska
 */
public class CheckNetwork {

    String dirWeb = "www.google.com";

    int puerto = 80;

    public CheckNetwork() {
    }  

    public boolean isConnected() {
        try {
            Socket s = new Socket(dirWeb, puerto);

            return s.isConnected();

        } catch (IOException ex) {
            System.err.println("Error en la conexión"+ex.getMessage());
            return false;
        }
    }
}
