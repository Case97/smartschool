/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.model;

import com.developerfriends.edu.smartschool.pojo.Grades;
import com.developerfriends.edu.smartschool.pojo.Sections;
import com.developerfriends.edu.smartschool.util.Dialogs;
import com.developerfriends.edu.smartschool.util.UtilClass;
import java.util.Iterator;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Tanieska
 */
public class SectionsModel implements EntitiesInterface<Sections> {

    Dialogs dialog = new Dialogs();
    UtilClass util = new UtilClass();

    private Session session;

    private int isSave;
    private int isDelete;

    @Override
    public void save(Sections object) {
        Transaction tx = null;
        try {
            tx = getSession().beginTransaction();
            getSession().save(object);

            tx.commit();
            setIsSave(1);

        } catch (RuntimeException e) {
            if (tx != null) {
                tx.rollback();
            }
             setIsSave(0);
            throw e;
        } finally {
            closeSession();
        }
    }

    public ObservableList<Sections> readSections(Grades grade) {
        assert getSession() != null;
        //     grade.setState(Boolean.FALSE);
        List results = getSession().createCriteria(Sections.class)
                .setProjection(Projections.projectionList()
                        .add(Projections.property("sectionId")) // 0
                        .add(Projections.property("letter"), "name") //1
                )
                .add(Restrictions.eq("state", Boolean.TRUE))
                .add(Restrictions.eq("grades", grade))
                //.addOrder(Order.desc("name"))
                .list();
        return setSections(results);
    }

    private ObservableList<Sections> setSections(List results) throws NumberFormatException {
        ObservableList<Sections> sections = FXCollections.observableArrayList();
        try {

            Iterator i = results.iterator();

            while (i.hasNext()) {
                Object[] obj = (Object[]) i.next();

                sections.add(
                        new Sections((Integer.parseInt(String.valueOf(obj[0]))),
                                (Integer.parseInt(String.valueOf(obj[1]))))
                );

            }
        } catch (java.lang.ClassCastException ex) {
            System.err.println("Error en el setSections " + ex.getMessage());
        }

        return sections;
    }

    public ObservableList<String> getOnlySection(Grades grade) {
        ObservableList<String> sections = FXCollections.observableArrayList();
        ObservableList<Sections> read = readSections(grade);

        for (int i = 0; i < read.size(); i++) {
            sections.add(util.getLetter(read.get(i).getLetter()));
        }
        return sections;
    }

    @Override //No se usara
    public void update(Sections object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Sections object) {
        Transaction tx = null;
        try {
            tx = getSession().beginTransaction();
            getSession().update(object);

            tx.commit();
            setIsDelete(1);

        } catch (RuntimeException e) {
            if (tx != null) {
                tx.rollback();
            }
            setIsDelete(0);
            throw e;
        } finally {
            closeSession();
        }
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public void closeSession() {
        if ((session != null) && (session.isOpen())) {
            session.close();
        }
    }

    public int getIsSave() {
        return isSave;
    }

    public void setIsSave(int isSave) {
        this.isSave = isSave;
    }

    public int getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(int isDelete) {
        this.isDelete = isDelete;
    }
    

}
