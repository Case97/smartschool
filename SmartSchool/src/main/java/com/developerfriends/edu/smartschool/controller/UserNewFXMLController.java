/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.controller;

import com.crazycode.custom.elements.SearchField;
import com.developerfriends.edu.smartschool.model.EmployeesModel;
import com.developerfriends.edu.smartschool.pojo.Employees;
import com.developerfriends.edu.smartschool.util.Dialogs;
import com.developerfriends.edu.smartschool.util.HibernateUtil;
import com.developerfriends.edu.smartschool.util.ImageClass;
import com.developerfriends.edu.smartschool.util.StageController;
import com.developerfriends.edu.smartschool.util.UtilClass;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 * FXML Controller class
 *
 * @author Enmnauel Calero
 */
public class UserNewFXMLController implements Initializable {

    @FXML
    private TableView<Employees> tableEmployee;
    @FXML
    private TableColumn<Employees, String> colNames;
    @FXML
    private TableColumn<Employees, String> colPosition;
    @FXML
    private TableColumn<Employees, String> colGender;
    @FXML
    private TableColumn<Employees, String> colAge;
    @FXML
    private TableColumn<Employees, String> colIdentityCard;
    @FXML
    private TableColumn<Employees, String> colPhone;
    @FXML
    private TableColumn<Employees, String> colEmail;
    @FXML
    private TableColumn<Employees, String> colAddress;
    @FXML
    private JFXButton btnAddUserConfiguration;

    EmployeesModel employeesModel = new EmployeesModel();
    StageController stageController = new StageController();
    Dialogs dialogs = new Dialogs();
    UtilClass util = new UtilClass();
    private SessionFactory sFactory;
    private Stage stage;
    @FXML
    private SearchField txtSearch;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        icon();
        getSessionFactory();
        colNames.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getPersons().getFirstName() + " "
                + cellData.getValue().getPersons().getSecondName() + " "
                + cellData.getValue().getPersons().getFirstLastname() + " "
                + cellData.getValue().getPersons().getSecondLastname()));
        colPosition.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(util.getRoles(cellData.getValue().getRole())));
        colPhone.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getPhone()));
        colAddress.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getPersons().getAddress()));
        colAge.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(String.valueOf(util.age(cellData.getValue().getPersons().getBirthday()))));
        colGender.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(util.gender(cellData.getValue().getPersons().getGender())));
        colIdentityCard.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getIdentityCard()));
        colEmail.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getEmail()));
        search();
        loadEmployee();
    }

    private void icon() {
        stageController.Icon(btnAddUserConfiguration, ImageClass.newElement);
    }

    private void loadEmployee() {
        try {
            employeesModel.setSession(openSession());
            tableEmployee.setItems(employeesModel.readEmployeeWhitoutUser());
        } catch (NullPointerException ex) {

            System.err.println("Error al mostrar la tabla " + ex.getMessage());
        }

    }

    private void search() {
        txtSearch.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (newValue.isEmpty()) {
                loadEmployee();
            } else {
                findEmployee(newValue);
            }
        });
    }

    private void findEmployee(String text) {
        try {
            employeesModel.setSession(openSession());
            tableEmployee.setItems(employeesModel.findEmployeeWhitoutUser(text));
        } catch (NullPointerException ex) {
            System.err.println("Error al mostrar la tabla " + ex.getMessage());
        }
    }

    private Session openSession() {
        return sFactory.openSession();
    }

    private void getSessionFactory() {
        sFactory = HibernateUtil.getSessionFactory();
    }

    @FXML
    private void configurationUser(ActionEvent ae) {
        FXMLLoader load = new FXMLLoader();
        URL url = UserConfigurationFXMLController.class.getResource("/fxml/UserConfigurationFXML.fxml");
        Stage st = stageController.showStage(url, "Usuario", stage, load);
        UserConfigurationFXMLController controller = load.getController();
        controller.setEmployee((Employees) tableEmployee.getSelectionModel().getSelectedItem());
        st.initModality(Modality.APPLICATION_MODAL);
        st.resizableProperty().setValue(Boolean.FALSE);
        st.showAndWait();
        Node source = (Node) ae.getSource();
        Stage theStage = (Stage) source.getScene().getWindow();
        theStage.close();
    }

    @FXML
    private void userSelected() {
        Employees employeeTemp = (Employees) tableEmployee.getSelectionModel().getSelectedItem();
        if (employeeTemp != null) {
            this.btnAddUserConfiguration.setDisable(false);
        }
    }

}
