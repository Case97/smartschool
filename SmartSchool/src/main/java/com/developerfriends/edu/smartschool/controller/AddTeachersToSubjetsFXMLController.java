/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.controller;

import com.crazycode.custom.elements.SearchField;
import com.developerfriends.edu.smartschool.model.TeachersSubjectsModel;
import com.developerfriends.edu.smartschool.pojo.Employees;
import com.developerfriends.edu.smartschool.pojo.Subjects;
import com.developerfriends.edu.smartschool.pojo.TeachersSubjects;
import com.developerfriends.edu.smartschool.util.Dialogs;
import com.developerfriends.edu.smartschool.util.HibernateUtil;
import com.developerfriends.edu.smartschool.util.ImageClass;
import com.developerfriends.edu.smartschool.util.StageController;
import com.developerfriends.edu.smartschool.util.UtilClass;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 * FXML Controller class
 *
 * @author Tanieska
 */
public class AddTeachersToSubjetsFXMLController implements Initializable {

    SubjectFXMLController controller = new SubjectFXMLController();
    TeachersSubjectsModel teachersSubjectsModel = new TeachersSubjectsModel();
    UtilClass util = new UtilClass();
    Dialogs dialog = new Dialogs();
    StageController sc = new StageController();

    private Stage dialogStage;
    private SessionFactory sFactory;
    Subjects subjects;
    private int subjectId;
    private final Boolean ACTIVE = Boolean.TRUE;

    @FXML
    private TableView<Employees> tableTeacher;
    @FXML
    private TableColumn<Employees, String> colTeacher;
    @FXML
    private SearchField txtSearch;
    @FXML
    private JFXButton btnSave;
    @FXML
    private JFXButton btnCancel;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        Icons();

        getSessionFactory();

        colTeacher.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getPersons().getPersonName()));

        search();
    }

    public void Icons() {
        sc.Icon(btnSave, ImageClass.save);
        sc.Icon(btnCancel, ImageClass.cancel);
    }

    protected void load() {
        try {
            teachersSubjectsModel.setSession(openSession());
            tableTeacher.setItems(teachersSubjectsModel.readTeacherWithoutSubject(getSubjectId()));
        } catch (NullPointerException ex) {
            System.err.println("Error en load " + ex.getMessage());
        }
    }

    private void find(String text) {
        try {
            teachersSubjectsModel.setSession(openSession());
            tableTeacher.setItems(teachersSubjectsModel.findTeacherWithoutSubject(getSubjectId(), text));
        } catch (NullPointerException ex) {
            System.err.println("Error en load " + ex.getMessage());
        }
    }

    @FXML
    private void save(ActionEvent event) {
        Employees selectSections = (Employees) tableTeacher.getSelectionModel().getSelectedItem();
        if (selectSections != null) {
            TeachersSubjects teacherSubjects = new TeachersSubjects(selectSections, getSubjects(), ACTIVE);
            teachersSubjectsModel.setSession(openSession());
            teachersSubjectsModel.save(teacherSubjects);
            dialogStage.close();
            controller.loadTeacher(getSubjects());
        } else {
            dialog.Alerta("Advertencia", "No se ha encontrado el profesor", "Por favor seleccione un profesor en la tabla", Alert.AlertType.WARNING);
        }
    }

    private void search() {
        txtSearch.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (newValue.isEmpty()) {
                load();
            } else {
                find(newValue);
            }
        });
    }

    @FXML
    private void cancel(ActionEvent event) {
        getStage().close();
    }

    public Stage getStage() {
        return dialogStage;
    }

    public void setStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    private Session openSession() {
        return sFactory.openSession();
    }

    private void getSessionFactory() {
        sFactory = HibernateUtil.getSessionFactory();
    }

    public SubjectFXMLController getSubject() {
        return controller;
    }

    public void setController(SubjectFXMLController controller) {
        this.controller = controller;
    }

    public Subjects getSubjects() {
        return subjects;
    }

    public void setSubjects(Subjects subjects) {
        this.subjects = subjects;
    }

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

}
