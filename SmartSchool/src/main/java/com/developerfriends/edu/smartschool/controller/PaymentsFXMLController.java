/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.controller;

import com.developerfriends.edu.smartschool.model.DutyModel;
import com.developerfriends.edu.smartschool.pojo.Duty;
import com.developerfriends.edu.smartschool.pojo.Employees;
import com.developerfriends.edu.smartschool.util.Dialogs;
import com.developerfriends.edu.smartschool.util.HibernateUtil;
import com.developerfriends.edu.smartschool.util.ImageClass;
import com.developerfriends.edu.smartschool.util.StageController;
import com.developerfriends.edu.smartschool.util.UtilClass;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 * FXML Controller class
 *
 * @author Tanieska
 */
public class PaymentsFXMLController implements Initializable {

    PrincipalFXMLController principal = new PrincipalFXMLController();
    StageController stController = new StageController();
    Dialogs dialog = new Dialogs();
    DutyModel dutyModel = new DutyModel();
    UtilClass util = new UtilClass();

    private Stage primaryStage;
    private SessionFactory sFactory;
    private static Employees employees;
    private Duty duty;

    @FXML
    private JFXButton btnPreEnrollmentPayments;
    @FXML
    private JFXButton btnEnrollmentPayments;
    @FXML
    private JFXButton btnMonthlyPayments;
    @FXML
    private JFXButton btnOthers;
    @FXML
    private JFXButton btnBack;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        Icons();

        getSessionFactory();
    }

    public void Icons() {
        stController.Icon2(btnPreEnrollmentPayments, ImageClass.bigPayment);
        stController.Icon2(btnEnrollmentPayments, ImageClass.bigPayment);
        stController.Icon2(btnMonthlyPayments, ImageClass.bigPayment);
        stController.Icon2(btnOthers, ImageClass.bigPayment);
        stController.Icon(btnBack, ImageClass.back);
    }

    @FXML
    private void showPreEnrollmentPayments(ActionEvent event) {
        if(isExistDuty(0)) return;
        FXMLLoader load = new FXMLLoader();
        Stage stage = stController.showStage(
                PreEnrollmentPaymentFXMLController.class.getResource("/fxml/PreEnrollmentPaymentFXML.fxml"),
                "Pago de Pre-Matricula",
                primaryStage, load);
        PreEnrollmentPaymentFXMLController controller = load.getController();
        controller.setStage(stage);
        controller.setEmployees(getEmployees());
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.resizableProperty().setValue(Boolean.FALSE);
        stage.show();

    }

    @FXML
    private void showEnrollmentPayments(ActionEvent event) {
        if(isExistDuty(1)) return;
        FXMLLoader load = new FXMLLoader();
        Stage stage = stController.showStage(
                EnrollmentPaymentFXMLController.class.getResource("/fxml/EnrollmentPaymentFXML.fxml"),
                "Pago Matricula",
                primaryStage, load);
        EnrollmentPaymentFXMLController controller = load.getController();
        controller.setStage(stage);
        controller.setEmployees(getEmployees());
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.resizableProperty().setValue(Boolean.FALSE);
        stage.show();
    }

    private boolean isExistDuty(int type) {
        dutyModel.setSession(openSession());
        duty = dutyModel.getDuty(type);
        if (duty == null) {
            dialog.Alerta("Smart School",
                    "No tiene información del arancel " + util.fillPayment().get(type),
                    "Se le recomienda ingresar esa información en la sección Catálogo en Aranceles", Alert.AlertType.ERROR);
            return true;
        }
        return false;
    }

    public Employees getEmployees() {
        return employees;
    }

    public void setEmployees(Employees employees) {
        PaymentsFXMLController.employees = employees;
    }

    private Session openSession() {
        return sFactory.openSession();
    }

    private void getSessionFactory() {
        sFactory = HibernateUtil.getSessionFactory();
    }

    @FXML
    private void back(ActionEvent event) {
        principal.back(event);
    }

    @FXML
    private void showMonthlyPayments(ActionEvent event) {
    }

    @FXML
    private void showOthers(ActionEvent event) {
    }

}
