package com.developerfriends.edu.smartschool.pojo;
// Generated 27-jun-2017 19:15:47 by Hibernate Tools 4.3.1

import java.util.HashSet;
import java.util.Set;

/**
 * Users generated by hbm2java
 */
public class Users implements java.io.Serializable {

    private Integer userId;
    private Persons persons;
    private String userU;
    private String passwordU;
    private Boolean state;
    private Set roles = new HashSet(0);

    public Users() {
    }

    public Users(Persons persons, String userU, String passwordU, Boolean state, Set roles) {
        this.persons = persons;
        this.userU = userU;
        this.passwordU = passwordU;
        this.state = state;
        this.roles = roles;
    }

    public Users(int userId ,Persons persons,String userU, String passwordU, Boolean state) {
        this.userId = userId;
        this.persons =persons;
        this.userU = userU;
        this.passwordU = passwordU;
        this.state = state;
    }

    public Integer getUserId() {
        return this.userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Persons getPersons() {
        return this.persons;
    }

    public void setPersons(Persons persons) {
        this.persons = persons;
    }

    public String getUserU() {
        return this.userU;
    }

    public void setUserU(String userU) {
        this.userU = userU;
    }

    public String getPasswordU() {
        return this.passwordU;
    }

    public void setPasswordU(String passwordU) {
        this.passwordU = passwordU;
    }

    public Boolean getState() {
        return this.state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    public Set getRoles() {
        return this.roles;
    }

    public void setRoles(Set roles) {
        this.roles = roles;
    }

}
