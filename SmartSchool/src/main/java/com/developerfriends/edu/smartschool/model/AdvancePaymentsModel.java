/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.model;

import com.developerfriends.edu.smartschool.pojo.Advancepayments;
import com.developerfriends.edu.smartschool.pojo.Duty;
import com.developerfriends.edu.smartschool.util.Dialogs;
import com.developerfriends.edu.smartschool.util.UtilClass;
import java.util.Iterator;
import java.util.List;
import javafx.scene.control.Alert;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Tanieska
 */
public class AdvancePaymentsModel implements EntitiesInterface<Advancepayments> {

    Dialogs dialog = new Dialogs();
    UtilClass util = new UtilClass();

    private Session session;
    private int isSave;

    @Override
    public void save(Advancepayments object) {
        Transaction tx = null;
        try {
            tx = getSession().beginTransaction();
            getSession().save(object.getPayments());
            getSession().save(object);

            tx.commit();
            setIsSave(1);
//            dialog.Alerta("Smart School", "Completo", "Se guardo completo el pago con No."
//                    + getLastId(), Alert.AlertType.INFORMATION);

        } catch (RuntimeException e) {
            if (tx != null) {
                tx.rollback();
            }
            setIsSave(0);
//            dialog.Alerta("Error", "", "Sucedio un error al guardar", Alert.AlertType.ERROR);
            throw e;
        } finally {
            closeSession();
        }
    }

     public Advancepayments getPreEnroollmentPayment(int id) {
        Advancepayments ap = null;
        List results = getSession().createCriteria(Advancepayments.class)
                .setProjection(Projections.projectionList()
                        .add(Projections.property("advancePaymentId")) // 0
                        .add(Projections.property("firstName")) //1
                        .add(Projections.property("firstLastname")) //2
                )
                .add(Restrictions.eq("advancePaymentId", id))
                .list();
        if (results.isEmpty()) {
            dialog.Alerta("Error", "", "No se encuentra esa factura", Alert.AlertType.ERROR);
        } else {
            Iterator i = results.iterator();

            while (i.hasNext()) {
                Object[] obj = (Object[]) i.next();

                ap = new Advancepayments((Integer.parseInt(String.valueOf(obj[0]))),
                        String.valueOf(obj[1]),
                        String.valueOf(obj[2]));
            }
        }
        return ap;
    }
    
    public Advancepayments getEnroollmentPayment(int id) {
        Advancepayments results = (Advancepayments) getSession().createCriteria(Advancepayments.class)
                .add(Restrictions.eq("advancePaymentId", id)).uniqueResult();
       
        return results;
    }

    public int getLastId() {
        assert getSession() != null;
        int id = 0;
        String sql = "SELECT MAX(advancePaymentId) FROM advancepayments";
        SQLQuery query = getSession().createSQLQuery(sql);

        List<Integer> results = query.list();

        id = results.get(0);
        return id;
    }

    @Override
    public void update(Advancepayments object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Advancepayments object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public void closeSession() {
        if ((session != null) && (session.isOpen())) {
            session.close();
        }
    }

    public int getIsSave() {
        return isSave;
    }

    public void setIsSave(int isSave) {
        this.isSave = isSave;
    }

}
