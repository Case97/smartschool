/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.model;

import com.developerfriends.edu.smartschool.pojo.Employees;
import com.developerfriends.edu.smartschool.pojo.Persons;
import com.developerfriends.edu.smartschool.util.Dialogs;
import com.developerfriends.edu.smartschool.util.PopUpMessage;
import com.developerfriends.edu.smartschool.util.StringMessage;
import com.developerfriends.edu.smartschool.util.UtilClass;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Tanieska
 */
public class EmployeesModel implements EntitiesInterface<Employees> {

    Dialogs dialog = new Dialogs();
    UtilClass util = new UtilClass();
    PopUpMessage popUpMessage = new PopUpMessage();

    private Session session;

    @Override
    public void save(Employees object) {
        Transaction tx = null;
        try {
            tx = getSession().beginTransaction();
            getSession().save(object.getPersons());
            getSession().save(object);
            tx.commit();
            popUpMessage.informationMessage(StringMessage.saveObject);
        } catch (RuntimeException e) {
            if (tx != null) {
                tx.rollback();
            }
            dialog.Alerta("Smart School", "Error", "Sucedio un error al guardar", Alert.AlertType.ERROR);
            throw e;
        } finally {
            closeSession();
        }
    }

    @Override
    public void update(Employees object) {
        Transaction tx = null;
        try {
            tx = getSession().beginTransaction();
            getSession().update(object.getPersons());
            getSession().update(object);
            tx.commit();
            popUpMessage.informationMessage(StringMessage.updateObject);
        } catch (RuntimeException e) {
            if (tx != null) {
                tx.rollback();
            }
            dialog.Alerta("Smart School", "Error", "Sucedio un error al actualizar", Alert.AlertType.ERROR);
            throw e;
        } finally {
            closeSession();
        }
    }

    @Override
    public void delete(Employees object) {
        Transaction tx = null;
        try {
            tx = getSession().beginTransaction();
            getSession().update(object.getPersons());
            getSession().update(object);
            tx.commit();
            popUpMessage.informationMessage(StringMessage.dismissEmployee);
        } catch (RuntimeException e) {
            if (tx != null) {
                tx.rollback();
            }
            dialog.Alerta("Smart School", "Error", "Sucedio un error al eliminar", Alert.AlertType.ERROR);
            throw e;
        } finally {
            closeSession();
        }
    }

    public ObservableList<Employees> readEmployeeWhitoutUser() {
        assert getSession() != null;
        String sql = "select "
                + "persons.personId, "
                + "firstName, "
                + "secondName, "
                + "firstLastname, "
                + "secondLastname, "
                + "gender, "
                + "birthday, "
                + "address, "
                + "employeeId, "
                + "role, "
                + "identityCard, "
                + "phone, "
                + "email, "
                + "photo "
                + "from persons "
                + "inner join employees "
                + "on employees.personId = persons.personId "
                + "where persons.personId not in (select persons.personId from users inner join persons on users.personId= persons.personId ) and persons.state = 1";
        SQLQuery query = getSession().createSQLQuery(sql);
        List<Object[]> results = query.list();
        return setEmployee(results);
    }

    public ObservableList<Employees> findEmployeeWhitoutUser(String text) {
        assert getSession() != null;
        String sql = "select "
                + "persons.personId, "
                + "firstName, "
                + "secondName, "
                + "firstLastname, "
                + "secondLastname, "
                + "gender, "
                + "birthday, "
                + "address, "
                + "employeeId, "
                + "role, "
                + "identityCard, "
                + "phone, "
                + "email, "
                + "photo "
                + "from persons "
                + "inner join employees "
                + "on employees.personId = persons.personId "
                + "where persons.personId not in (select persons.personId from users inner join persons on users.personId= persons.personId ) and persons.state = 1 "
                + "and  firstName like '" + text + "%'";
        SQLQuery query = getSession().createSQLQuery(sql);
        List<Object[]> results = query.list();
        return setEmployee(results);
    }

    public ObservableList<Employees> readEmployee() {
        assert getSession() != null;
        List results = getSession().createCriteria(Employees.class, "employees")
                .createAlias("persons", "person")
                .setProjection(Projections.projectionList()
                        .add(Projections.property("person.personId")) // 0
                        .add(Projections.property("person.firstName"), "name") //1
                        .add(Projections.property("person.secondName")) //2
                        .add(Projections.property("person.firstLastname")) //3
                        .add(Projections.property("person.secondLastname")) //4
                        .add(Projections.property("person.gender")) //5
                        .add(Projections.property("person.birthday")) //6
                        .add(Projections.property("person.address")) //7
                        .add(Projections.property("employees.employeeId")) //8
                        .add(Projections.property("employees.role")) //9
                        .add(Projections.property("employees.identityCard")) //10
                        .add(Projections.property("employees.phone")) //11
                        .add(Projections.property("employees.email")) //12
                        .add(Projections.property("person.photo")) //13
                )
                .add(Restrictions.eq("person.state", Boolean.TRUE))
                .addOrder(Order.desc("name"))
                .list();
        return setEmployee(results);
    }

    public ObservableList<Employees> findEmployees(String text) {
        assert getSession() != null;
        List results = getSession().createCriteria(Employees.class, "employees")
                .createAlias("persons", "person")
                .setProjection(Projections.projectionList()
                        .add(Projections.property("person.personId")) // 0
                        .add(Projections.property("person.firstName"), "name") //1
                        .add(Projections.property("person.secondName")) //2
                        .add(Projections.property("person.firstLastname")) //3
                        .add(Projections.property("person.secondLastname")) //4
                        .add(Projections.property("person.gender")) //5
                        .add(Projections.property("person.birthday")) //6
                        .add(Projections.property("person.address")) //7
                        .add(Projections.property("employees.employeeId")) //8
                        .add(Projections.property("employees.role")) //9
                        .add(Projections.property("employees.identityCard")) //10
                        .add(Projections.property("employees.phone")) //11
                        .add(Projections.property("employees.email")) //12
                        .add(Projections.property("person.photo")) //13
                )
                .add(Restrictions.eq("person.state", Boolean.TRUE))
                .add(Restrictions.or(
                        Restrictions.like("person.firstName", text + "%"),
                        Restrictions.like("person.secondName", text + "%"),
                        Restrictions.like("person.firstLastname", text + "%"),
                        Restrictions.like("person.secondLastname", text + "%")
                ))
                .addOrder(Order.desc("name"))
                .list();
        return setEmployee(results);
    }

    private ObservableList<Employees> setEmployee(List results) throws NumberFormatException {
        ObservableList<Employees> employee = FXCollections.observableArrayList();
        try {

            Iterator i = results.iterator();

            while (i.hasNext()) {
                Object[] obj = (Object[]) i.next();

                Persons p = new Persons(
                        (Integer.parseInt(String.valueOf(obj[0]))),
                        String.valueOf(obj[1]),
                        String.valueOf(obj[2]),
                        String.valueOf(obj[3]),
                        String.valueOf(obj[4]),
                        (Boolean) obj[5],
                        (Date) obj[6],
                        String.valueOf(obj[7]),
                        (byte[]) obj[13]);

                employee.add(new Employees(
                        Integer.parseInt(String.valueOf(obj[8])),
                        p,
                        Integer.parseInt(String.valueOf(obj[9])),//roles paso a ser int 
                        String.valueOf(obj[10]),
                        String.valueOf(obj[11]),
                        String.valueOf(obj[12])));

            }
        } catch (java.lang.ClassCastException ex) {
            System.err.println("Error en el setEmployee " + ex.getMessage());
        }

        return employee;
    }

    public Employees getEmployees(int userId) throws IndexOutOfBoundsException {
        assert getSession() != null;
        Persons persons;
        Employees employees = null;
        String sql = "select "
                + "e.employeeId, " //0
                + "p.firstName, " //1 
                + "p.firstLastname " //2 
                + "from employees e "
                + "inner join persons p "
                + "on e.personId = p.personId "
                + "inner join users u "
                + "on u.personId = p.personId "
                + "where u.userId=" + userId;
        SQLQuery query = getSession().createSQLQuery(sql);
        List<Object[]> results = query.list();

        for (Object[] obj : results) {
            persons = new Persons(String.valueOf(obj[1]),
                    String.valueOf(obj[2]));
            employees = new Employees(Integer.parseInt(String.valueOf(obj[0])), persons);
        }

        return employees;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public void closeSession() {
        if ((session != null) && (session.isOpen())) {
            session.close();
        }
    }

}
