/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.controller;

import com.crazycode.custom.elements.SearchField;
import com.developerfriends.edu.smartschool.util.ImageClass;
import com.developerfriends.edu.smartschool.util.StageController;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXToggleButton;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import org.controlsfx.control.textfield.CustomTextField;

/**
 * FXML Controller class
 *
 * @author 27gma
 */
public class ScholarShipFXMLController implements Initializable {

    StageController stController = new StageController();
    
    @FXML
    private SearchField txtSearchSchoolarShip;
    @FXML
    private JFXToggleButton tgEnable;
    @FXML
    private JFXButton btnAdd;
    @FXML
    private JFXButton btnRenove;
    @FXML
    private JFXButton btnClean;
    @FXML
    private JFXButton btnSave;    
    @FXML
    private JFXButton btnCancel;
    @FXML
    private JFXButton btnSaveChange;
    @FXML
    private JFXButton btnBack;

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        Icons();
    }    

    public void Icons() {
        stController.Icon(btnAdd, ImageClass.newElement);
        stController.Icon(btnRenove, ImageClass.pencil);
        stController.Icon(btnClean,ImageClass.disableSubElement);
        stController.Icon(btnSave, ImageClass.save);
        stController.Icon(btnSaveChange, ImageClass.saveChange);
        stController.Icon(btnCancel,ImageClass.cancel);
        stController.Icon(btnBack, ImageClass.back);
    }
    
    @FXML
    private void back(ActionEvent event) {      
    }

    @FXML
    private void showClassrooms(ActionEvent event) {
    }
    
}
