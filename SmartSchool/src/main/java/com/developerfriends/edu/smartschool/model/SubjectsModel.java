/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.model;

import com.developerfriends.edu.smartschool.pojo.Subjects;
import com.developerfriends.edu.smartschool.util.Dialogs;
import com.developerfriends.edu.smartschool.util.UtilClass;
import java.util.Iterator;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Tanieska
 */
public class SubjectsModel implements EntitiesInterface<Subjects> {

    Dialogs dialog = new Dialogs();

    UtilClass util = new UtilClass();

    private Session session;

    @Override
    public void save(Subjects object) {
        Transaction tx = null;
        try {
            tx = getSession().beginTransaction();
            getSession().save(object);

            tx.commit();
            dialog.Alerta("Completo", "", "Se guardo correctamente el registro", Alert.AlertType.INFORMATION);

        } catch (RuntimeException e) {
            if (tx != null) {
                tx.rollback();
            }
            dialog.Alerta("Error", "", "Sucedio un error al guardar", Alert.AlertType.ERROR);
            throw e;
        } finally {
            closeSession();
        }
    }

    public ObservableList<Subjects> readSubjects(Boolean state) {
        assert getSession() != null;
        List results = getSession().createCriteria(Subjects.class)
                .setProjection(Projections.projectionList()
                        .add(Projections.property("subjectId")) // 0
                        .add(Projections.property("subjectName"), "name") //1
                        .add(Projections.property("frecuencyClass")) //2
                )
                .add(Restrictions.eq("state", state))
                .addOrder(Order.desc("name"))
                .list();
        return setSubjects(results);
    }

    public ObservableList<Subjects> findSubjects(Boolean state, String text) {
        assert getSession() != null;
        List results = getSession().createCriteria(Subjects.class)
                .setProjection(Projections.projectionList()
                        .add(Projections.property("subjectId")) // 0
                        .add(Projections.property("subjectName"), "name") //1
                        .add(Projections.property("frecuencyClass")) //2
                )
                .add(Restrictions.eq("state", state))
                .add(Restrictions.like("subjectName", text + "%"))
                .addOrder(Order.desc("name"))
                .list();
        return setSubjects(results);
    }

    private ObservableList<Subjects> setSubjects(List results) throws NumberFormatException {
        ObservableList<Subjects> subjects = FXCollections.observableArrayList();
        try {

            Iterator i = results.iterator();

            while (i.hasNext()) {
                Object[] obj = (Object[]) i.next();

                subjects.add(
                        new Subjects(
                                (Integer.parseInt(String.valueOf(obj[0]))),
                                String.valueOf(obj[1]),
                                (Integer.parseInt(String.valueOf(obj[2]))))
                );

            }
        } catch (java.lang.ClassCastException ex) {
            System.err.println("Error en el setGrades " + ex.getMessage());
        }

        return subjects;
    }

    @Override
    public void update(Subjects object) {
        Transaction tx = null;
        try {
            tx = getSession().beginTransaction();
            getSession().update(object);

            tx.commit();
            dialog.Alerta("Completo", "", "Se actualizo el registro", Alert.AlertType.INFORMATION);

        } catch (RuntimeException e) {
            if (tx != null) {
                tx.rollback();
            }
            dialog.Alerta("Error", "", "Sucedio un error al actualizar", Alert.AlertType.ERROR);
            throw e;
        } finally {
            closeSession();
        }
    }

    public void active(Subjects object) {
        Transaction tx = null;
        try {
            tx = getSession().beginTransaction();
            getSession().update(object);

            tx.commit();
            dialog.Alerta("Completo", "", "Se habilito el registro", Alert.AlertType.INFORMATION);

        } catch (RuntimeException e) {
            if (tx != null) {
                tx.rollback();
            }
            dialog.Alerta("Error", "", "Sucedio un error al habilitar", Alert.AlertType.ERROR);
            throw e;
        } finally {
            closeSession();
        }
    }

    @Override
    public void delete(Subjects object) {
        Transaction tx = null;
        try {
            tx = getSession().beginTransaction();
            getSession().update(object);

            tx.commit();
            dialog.Alerta("Completo", "", "Se deshabilito esa materia", Alert.AlertType.INFORMATION);

        } catch (RuntimeException e) {
            if (tx != null) {
                tx.rollback();
            }
            dialog.Alerta("Error", "", "Sucedio un error al desahibilitar", Alert.AlertType.ERROR);
            throw e;
        } finally {
            closeSession();
        }
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public void closeSession() {
        if ((session != null) && (session.isOpen())) {
            session.close();
        }
    }
}
