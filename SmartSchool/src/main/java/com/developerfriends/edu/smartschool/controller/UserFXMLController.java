/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.controller;

import com.crazycode.custom.elements.SearchField;
import com.developerfriends.edu.smartschool.model.RoleModel;
import com.developerfriends.edu.smartschool.model.UserModel;
import com.developerfriends.edu.smartschool.pojo.Role;
import com.developerfriends.edu.smartschool.pojo.UserEmployee;
import com.developerfriends.edu.smartschool.util.Dialogs;
import com.developerfriends.edu.smartschool.util.HibernateUtil;
import com.developerfriends.edu.smartschool.util.ImageClass;
import com.developerfriends.edu.smartschool.util.StageController;
import com.developerfriends.edu.smartschool.util.UtilClass;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXToggleButton;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 * FXML Controller class
 *
 * @author Enmnauel Calero
 */
public class UserFXMLController implements Initializable {

    private Stage stage;
    private boolean isNew = true;
    PrincipalFXMLController principal = new PrincipalFXMLController();
    StageController stageController = new StageController();
    UtilClass util = new UtilClass();
    Dialogs dialogs = new Dialogs();
    @FXML
    private TableView<UserEmployee> userTable;
    @FXML
    private TableColumn<UserEmployee, String> colEmployeeName;
    @FXML
    private TableColumn<UserEmployee, String> colUserName;
    @FXML
    private TableColumn<UserEmployee, String> colCargo;
    @FXML
    private TableColumn<UserEmployee, String> colEmail;
    @FXML
    private JFXButton btnBack;
    @FXML
    private ImageView imgViewEmployee;
    @FXML
    private Label txtUserName;
    @FXML
    private Label txtEmployeeName;
    private SessionFactory sFactory;
    UserModel userModel = new UserModel();
    RoleModel roleModel = new RoleModel();
    public Role role;
    @FXML
    private JFXButton btnSaveRoles;
    @FXML
    private JFXButton btnAdd;
    @FXML
    private JFXButton btnSettings;
    @FXML
    private JFXToggleButton employe_access;
    @FXML
    private JFXToggleButton users_access;
    @FXML
    private JFXToggleButton subjects_access;
    @FXML
    private JFXToggleButton grades_access;
    @FXML
    private JFXToggleButton sections_access;
    @FXML
    private JFXToggleButton enrollment_access;
    @FXML
    private JFXToggleButton cash_access;
    @FXML
    private JFXToggleButton addGradeNote_access;
    @FXML
    private JFXToggleButton assignmentSubjects_access;
    @FXML
    private JFXToggleButton horary_access;
    @FXML
    private JFXToggleButton bestStudentsReport_access;
    @FXML
    private JFXToggleButton paymentsReport_access;
    @FXML
    private JFXToggleButton monthlyPaymentsReport_access;
    @FXML
    private JFXToggleButton recordReport_access;
    @FXML
    private JFXToggleButton duty_access;
    @FXML
    private AnchorPane rolePane;
    @FXML
    private SearchField txtSearch;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        getSessionFactory();
        icon();
        userModel.setSession(openSession());
        roleModel.setSession(openSession());
        colEmployeeName.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getName()));
        colUserName.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getUserName()));
        colCargo.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(util.getRoles(cellData.getValue().getRole())));
        colEmail.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getEmail()));
        search();
        loadUsers();
    }

    private void loadUsers() {
        try {
            userModel.setSession(openSession());
            userTable.setItems(userModel.readUsers());
        } catch (NullPointerException ex) {
            System.err.println("Error al mostrar la tabla " + ex.getMessage());
        }

    }

    private void icon() {
        stageController.Icon(btnBack, ImageClass.back);
        stageController.Icon(btnSaveRoles, ImageClass.save);
        stageController.Icon(btnAdd, ImageClass.newElement);
        stageController.Icon(btnSettings, ImageClass.setting);
    }

    private Session openSession() {
        return sFactory.openSession();
    }

    private void getSessionFactory() {
        sFactory = HibernateUtil.getSessionFactory();
    }

    @FXML
    private void back(ActionEvent event) {
        principal.back(event);
    }

    @FXML
    private void addNewUser() {
        FXMLLoader load = new FXMLLoader();
        URL url = UserNewFXMLController.class.getResource("/fxml/UserNewFXML.fxml");
        Stage st = stageController.showStage(url, "Empleados", stage, load);
        st.initModality(Modality.APPLICATION_MODAL);
        st.resizableProperty().setValue(Boolean.FALSE);
        st.showAndWait();
        loadUsers();
    }

    @FXML
    private void selectUser() {
        UserEmployee userSelected = (UserEmployee) userTable.getSelectionModel().getSelectedItem();
        if (userSelected != null) {
            this.rolePane.setDisable(false);
            this.btnSettings.setDisable(false);
            RoleModel rolebyUser = new RoleModel();
            rolebyUser.setSession(openSession());
            this.txtUserName.setText(userSelected.getUserName());
            this.txtEmployeeName.setText(userSelected.getName());
            this.imgViewEmployee.setImage(util.getImage(userSelected.getPhoto()));
            role = rolebyUser.getRoleByUserId(userSelected.getIdUsuario());
            if (role != null) {
                isNew = false;
                resetRole();
                employe_access.setSelected(role.getEmployeAccess());
                users_access.setSelected(role.getUsersAccess());
                subjects_access.setSelected(role.getSubjectsAccess());
                grades_access.setSelected(role.getGradesAccess());
                sections_access.setSelected(role.getSectionsAccess());
                duty_access.setSelected(role.getDutyAccess());
                enrollment_access.setSelected(role.getEnrollmentAccess());
                cash_access.setSelected(role.getCashAccess());
                addGradeNote_access.setSelected(role.getAddGradeNoteAccess());
                assignmentSubjects_access.setSelected(role.getAssignmentSubjectsAccess());
                horary_access.setSelected(role.getHoraryAccess());
                bestStudentsReport_access.setSelected(role.getBestStudentsReportAccess());
                paymentsReport_access.setSelected(role.getPaymentsReportAccess());
                monthlyPaymentsReport_access.setSelected(role.getMonthlyPaymentsReportAccess());
                recordReport_access.setSelected(role.getRecordReportAccess());
            } else {
                isNew = true;
                resetRole();

            }
        }
    }

    @FXML
    private void saveRole() {
        if (isNew) {
            UserEmployee userSelected = (UserEmployee) userTable.getSelectionModel().getSelectedItem();
            Role roleNew = new Role(userModel.getUserById(userSelected.getIdUsuario()),
                    employe_access.isSelected(),
                    users_access.isSelected(),
                    subjects_access.isSelected(),
                    grades_access.isSelected(),
                    sections_access.isSelected(),
                    duty_access.isSelected(),
                    enrollment_access.isSelected(),
                    cash_access.isSelected(),
                    addGradeNote_access.isSelected(),
                    assignmentSubjects_access.isSelected(),
                    horary_access.isSelected(),
                    bestStudentsReport_access.isSelected(),
                    paymentsReport_access.isSelected(),
                    monthlyPaymentsReport_access.isSelected(),
                    recordReport_access.isSelected());
            roleModel.save(roleNew);
        } else {
            RoleModel roleModelToUpdate = new RoleModel();
            roleModelToUpdate.setSession(openSession());
            UserEmployee userSelected = (UserEmployee) userTable.getSelectionModel().getSelectedItem();
            Role roleToUpdate = new Role(role.getRoleId(), userModel.getUserById(userSelected.getIdUsuario()),
                    employe_access.isSelected(),
                    users_access.isSelected(),
                    subjects_access.isSelected(),
                    grades_access.isSelected(),
                    sections_access.isSelected(),
                    duty_access.isSelected(),
                    enrollment_access.isSelected(),
                    cash_access.isSelected(),
                    addGradeNote_access.isSelected(),
                    assignmentSubjects_access.isSelected(),
                    horary_access.isSelected(),
                    bestStudentsReport_access.isSelected(),
                    paymentsReport_access.isSelected(),
                    monthlyPaymentsReport_access.isSelected(),
                    recordReport_access.isSelected());
            roleModelToUpdate.update(roleToUpdate);
        }
    }

    @FXML
    private void settingsUser(ActionEvent ae) {
        UserEmployee userEmployeeTemp = (UserEmployee) userTable.getSelectionModel().getSelectedItem();
        if (userEmployeeTemp != null) {
            FXMLLoader load = new FXMLLoader();
            URL url = UserConfigurationFXMLController.class.getResource("/fxml/UserConfigurationFXML.fxml");
            Stage st = stageController.showStage(url, "Actualizar Usuario", stage, load);
            UserConfigurationFXMLController controller = load.getController();
            controller.setUser((UserEmployee) userTable.getSelectionModel().getSelectedItem());
            st.initModality(Modality.APPLICATION_MODAL);
            st.resizableProperty().setValue(Boolean.FALSE);
            st.showAndWait();
        } else {
            dialogs.Alerta("SmartSchool", "Error", "Seleccione un usuario por favor ....!!", Alert.AlertType.ERROR);
        }

    }

    private void resetRole() {
        employe_access.setSelected(false);
        users_access.setSelected(false);
        subjects_access.setSelected(false);
        grades_access.setSelected(false);
        sections_access.setSelected(false);
        enrollment_access.setSelected(false);
        cash_access.setSelected(false);
        addGradeNote_access.setSelected(false);
        assignmentSubjects_access.setSelected(false);
        horary_access.setSelected(false);
        bestStudentsReport_access.setSelected(false);
        paymentsReport_access.setSelected(false);
        monthlyPaymentsReport_access.setSelected(false);
        recordReport_access.setSelected(false);
        duty_access.setSelected(false);
    }

    private void findUser(String text) {
        try {
            userModel.setSession(openSession());
            userTable.setItems(userModel.findUsers(text));
        } catch (NullPointerException ex) {
            System.err.println("Error al mostrar la tabla " + ex.getMessage());
        }
    }

    private void search() {
        txtSearch.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (newValue.isEmpty()) {
                loadUsers();
            } else {
                findUser(newValue);
            }
        });
    }
}
