/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.controller;

import com.crazycode.custom.elements.SearchField;
import com.developerfriends.edu.smartschool.model.SectionClassromTeacherModel;
import com.developerfriends.edu.smartschool.model.SectionsClassroomsModel;
import com.developerfriends.edu.smartschool.model.SubjectsModel;
import com.developerfriends.edu.smartschool.model.TeachersSubjectsModel;
import com.developerfriends.edu.smartschool.pojo.SectionsClassrooms;
import com.developerfriends.edu.smartschool.pojo.SectionsclassroomsTeachers;
import com.developerfriends.edu.smartschool.pojo.Subjects;
import com.developerfriends.edu.smartschool.pojo.TeachersSubjects;
import com.developerfriends.edu.smartschool.util.Dialogs;
import com.developerfriends.edu.smartschool.util.HibernateUtil;
import com.developerfriends.edu.smartschool.util.ImageClass;
import com.developerfriends.edu.smartschool.util.StageController;
import com.developerfriends.edu.smartschool.util.UtilClass;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 * FXML Controller class
 *
 * @author Tanieska
 */
public class SectionsClassroomsTeachersFXMLController implements Initializable {

    PrincipalFXMLController principal = new PrincipalFXMLController();

    Dialogs dialog = new Dialogs();

    SubjectsModel subjectsModel = new SubjectsModel();

    TeachersSubjectsModel teachersSubjectsModel = new TeachersSubjectsModel();

    SectionsClassroomsModel sectionsClassroomsModel = new SectionsClassroomsModel();

    SectionClassromTeacherModel sectionClassromTeacherModel = new SectionClassromTeacherModel();

    StageController stageController = new StageController();

    UtilClass util = new UtilClass();

    private SessionFactory sFactory;

    private final Boolean ACTIVE = Boolean.TRUE;

    @FXML
    private JFXButton btnBack;
    @FXML
    private TableView<Subjects> tableSubject;
    @FXML
    private TableColumn<Subjects, String> colSubject;
    @FXML
    private TableView<TeachersSubjects> tableTeacher;
    @FXML
    private TableColumn<TeachersSubjects, String> colTeacher;
    @FXML
    private TableView<SectionsClassrooms> tableSectionClassroms;
    @FXML
    private TableColumn<SectionsClassrooms, String> colClassroom;
    @FXML
    private TableColumn<SectionsClassrooms, String> colGrade;
    @FXML
    private TableColumn<SectionsClassrooms, String> colSection;
    @FXML
    private TableColumn<SectionsClassrooms, String> colTurn;
    @FXML
    private TableView<SectionsclassroomsTeachers> tableSectionClassroomTeacher;
    @FXML
    private TableColumn<SectionsclassroomsTeachers, String> colSCTSubject;
    @FXML
    private TableColumn<SectionsclassroomsTeachers, String> colSCTTeacher;
    @FXML
    private TableColumn<SectionsclassroomsTeachers, String> colSCTClassroom;
    @FXML
    private TableColumn<SectionsclassroomsTeachers, String> colSCTGrade;
    @FXML
    private TableColumn<SectionsclassroomsTeachers, String> colSCTSection;
    @FXML
    private TableColumn<SectionsclassroomsTeachers, String> colSCTTurn;
    @FXML
    private SearchField txtSearchSubject;
    @FXML
    private SearchField txtSearchTeacher;
    @FXML
    private SearchField txtSearchSectionClassroomTeacher;
    @FXML
    private JFXButton btnSave;
    @FXML
    private SearchField txtSearchSectionClassroom;
    @FXML
    private JFXButton btnCancel;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        getSessionFactory();

        colSubject.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getSubjectName()));
        colTeacher.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getEmployees().getPersons().getPersonName()));
        colClassroom.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getClassrooms().getCodeClassroom()));
        colGrade.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(util.getGrade(cellData.getValue().getSections().getGrades().getGradeName())));
        colSection.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(util.getLetter(cellData.getValue().getSections().getLetter())));
        colTurn.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(util.getTurn(cellData.getValue().getTurn())));
        colSCTTeacher.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getTeachersSubjects().getEmployees().getPersons().getPersonName()));
        colSCTSubject.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getTeachersSubjects().getSubjects().getSubjectName()));
        colSCTClassroom.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getSectionsClassrooms().getClassrooms().getCodeClassroom()));
        colSCTGrade.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(util.getGrade(cellData.getValue().getSectionsClassrooms().getSections().getGrades().getGradeName())));
        colSCTSection.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(util.getLetter(cellData.getValue().getSectionsClassrooms().getSections().getLetter())));
        colSCTTurn.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(util.getTurn(cellData.getValue().getSectionsClassrooms().getTurn())));
        icon();
        searchSubject();
        searchSectionsClassroomTeacher();
        loadSubject();
        loadSectionsClassroomTeacher();
        start();
    }

    private void icon() {
        stageController.Icon(btnBack, ImageClass.back);
    }

    private void loadSubject() {
        try {
            subjectsModel.setSession(openSession());
            tableSubject.setItems(subjectsModel.readSubjects(ACTIVE));
        } catch (NullPointerException ex) {
            System.err.println("Error en loadSubject " + ex.getMessage());
        }
    }

    protected void loadTeacher(Subjects subjects) {
        try {
            teachersSubjectsModel.setSession(openSession());
            tableTeacher.setItems(teachersSubjectsModel.readTeacherSubject(subjects));
        } catch (NullPointerException ex) {
            System.err.println("Error en loadTeacher " + ex.getMessage());
        }
    }

    protected void loadSectionsClassroom(int subjectId) {
        try {
            sectionsClassroomsModel.setSession(openSession());
            tableSectionClassroms.setItems(sectionsClassroomsModel.readSectionsClassroomsWhitoutTeacher(subjectId));
        } catch (NullPointerException ex) {
            System.err.println("Error en loadSectionsClassroom " + ex.getMessage());
        }
    }

    protected void loadSectionsClassroomTeacher() {
        try {
            sectionClassromTeacherModel.setSession(openSession());
            tableSectionClassroomTeacher.setItems(sectionClassromTeacherModel.readSectionsClassroomTeacherSubject());
        } catch (NullPointerException ex) {
            System.err.println("Error en loadSectionsClassroomTeacher " + ex.getMessage());
        }
    }

    private void findSubject(String text) {
        try {
            subjectsModel.setSession(openSession());
            tableSubject.setItems(subjectsModel.findSubjects(ACTIVE, text));
        } catch (NullPointerException ex) {
            System.err.println("Error en findSubject " + ex.getMessage());
        }
    }

    private void findTeacher(Subjects subjects, String text) {
        try {
            teachersSubjectsModel.setSession(openSession());
            tableTeacher.setItems(teachersSubjectsModel.findTeacherSubject(subjects, text));
        } catch (NullPointerException ex) {
            System.err.println("Error en findTeacher " + ex.getMessage());
        }
    }

    private void findSectionsClassroom(int subjectId, String text) {
        try {
            sectionsClassroomsModel.setSession(openSession());
            tableSectionClassroms.setItems(sectionsClassroomsModel.findSectionsClassroomWhitoutTeacher(subjectId, text));
        } catch (NullPointerException ex) {
            System.err.println("Error en findSectionsClassroom " + ex.getMessage());
        }
    }

    protected void findSectionsClassroomTeacher(String text) {
        try {
            sectionClassromTeacherModel.setSession(openSession());
            tableSectionClassroomTeacher.setItems(sectionClassromTeacherModel.findSectionsClassroomTeacherSubject(text));
        } catch (NullPointerException ex) {
            System.err.println("Error en findSectionsClassroomTeacher " + ex.getMessage());
        }
    }

    private void start() {
        txtSearchTeacher.setDisable(true);
        tableTeacher.getItems().clear();
        txtSearchSectionClassroom.setDisable(true);
        tableSectionClassroms.getItems().clear();
        btnSave.setDisable(true);
        txtSearchTeacher.setText("");
        txtSearchSectionClassroom.setText("");
    }

    @FXML
    private void enableOptionSubject(MouseEvent event) {
        Subjects selectSubjects = (Subjects) tableSubject.getSelectionModel().getSelectedItem();
        if (selectSubjects != null) {
            loadTeacher(selectSubjects);
            txtSearchTeacher.setDisable(false);
            searchTeacherSubject(selectSubjects);
            txtSearchSectionClassroom.setText("");
            txtSearchSectionClassroom.setDisable(true);
            tableSectionClassroms.getItems().clear();
        }
    }

    @FXML
    private void enableOptionTeacher(MouseEvent event) {
        TeachersSubjects teacherSubjects = (TeachersSubjects) tableTeacher.getSelectionModel().getSelectedItem();
        if (teacherSubjects != null) {
            Subjects selectSubjects = (Subjects) tableSubject.getSelectionModel().getSelectedItem();
            loadSectionsClassroom(selectSubjects.getSubjectId());
            txtSearchSectionClassroom.setDisable(false);
            searchSectionsClassroom(selectSubjects.getSubjectId());
        }
    }

    @FXML
    private void enablesOptionSectionClassrom(MouseEvent event) {
        Subjects selectSubjects = (Subjects) tableSubject.getSelectionModel().getSelectedItem();
        if (selectSubjects != null) {
            TeachersSubjects teacherSubjects = (TeachersSubjects) tableTeacher.getSelectionModel().getSelectedItem();
            if (teacherSubjects != null) {
                SectionsClassrooms sectionsClassrooms = (SectionsClassrooms) tableSectionClassroms.getSelectionModel().getSelectedItem();
                if (sectionsClassrooms != null) {
                    btnSave.setDisable(false);
                }
            }
        }
    }

    private void searchSubject() {
        txtSearchSubject.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (newValue.isEmpty()) {
                loadSubject();
            } else {
                findSubject(newValue);
            }
            start();
        });
    }

    private void searchTeacherSubject(Subjects subject) {
        txtSearchTeacher.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (newValue.isEmpty()) {
                loadTeacher(subject);
            } else {
                findTeacher(subject, newValue);
            }
            this.txtSearchSectionClassroom.setDisable(true);
            tableSectionClassroms.getItems().clear();

        });
    }

    private void searchSectionsClassroom(int teacherId) {
        txtSearchSectionClassroom.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (newValue.isEmpty()) {
                loadSectionsClassroom(teacherId);
            } else {
                findSectionsClassroom(teacherId, newValue);
            }
            btnSave.setDisable(true);
        });
    }

    private void searchSectionsClassroomTeacher() {
        txtSearchSectionClassroomTeacher.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (newValue.isEmpty()) {
                loadSectionsClassroomTeacher();
            } else {
                findSectionsClassroomTeacher(newValue);
            }
            btnSave.setDisable(true);
        });
    }

    @FXML
    private void save(ActionEvent event) {
        TeachersSubjects teacherSubjects = (TeachersSubjects) tableTeacher.getSelectionModel().getSelectedItem();
        SectionsClassrooms sectionsClassrooms = (SectionsClassrooms) tableSectionClassroms.getSelectionModel().getSelectedItem();
        if (teacherSubjects != null && sectionsClassrooms != null) {
            sectionClassromTeacherModel.setSession(openSession());
            SectionsclassroomsTeachers sct = new SectionsclassroomsTeachers(
                    sectionsClassrooms,
                    teacherSubjects,
                    util.getYear());
            sectionClassromTeacherModel.save(sct);
            loadSectionsClassroomTeacher();
            start();
            loadSubject();
        }
    }

    @FXML
    private void cancel(ActionEvent event) {
        start();
        loadSubject();
        txtSearchSubject.setText("");
    }

    @FXML
    private void back(ActionEvent event) {
        principal.back(event);
    }

    private Session openSession() {
        return sFactory.openSession();
    }

    private void getSessionFactory() {
        sFactory = HibernateUtil.getSessionFactory();
    }

}
