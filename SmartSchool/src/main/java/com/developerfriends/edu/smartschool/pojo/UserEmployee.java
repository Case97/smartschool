/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.pojo;

/**
 *
 * @author Enmnauel Calero
 */
public class UserEmployee {
    
    private int idUsuario;
    private String name;
    private int role;
    private String email;
    private String userName;
    private byte[] photo;

    public UserEmployee() {
}

    public UserEmployee(int idUsuario, String name, int role, String email, String userName, byte[] photo) {
        this.idUsuario = idUsuario;
        this.name = name;
        this.role = role;
        this.email = email;
        this.userName = userName;
        this.photo = photo;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

   

}
