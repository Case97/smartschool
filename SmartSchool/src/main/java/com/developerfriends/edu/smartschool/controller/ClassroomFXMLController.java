package com.developerfriends.edu.smartschool.controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.crazycode.custom.elements.SearchField;
import com.developerfriends.edu.smartschool.model.ClassroomsModel;
import com.developerfriends.edu.smartschool.model.SectionsClassroomsModel;
import com.developerfriends.edu.smartschool.model.SectionsModel;
import com.developerfriends.edu.smartschool.pojo.Classrooms;
import com.developerfriends.edu.smartschool.pojo.SectionsClassrooms;
import com.developerfriends.edu.smartschool.util.Dialogs;
import com.developerfriends.edu.smartschool.util.HibernateUtil;
import com.developerfriends.edu.smartschool.util.ImageClass;
import com.developerfriends.edu.smartschool.util.StageController;
import com.developerfriends.edu.smartschool.util.UtilClass;
import com.developerfriends.edu.smartschool.util.Validation;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXToggleButton;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 * FXML Controller class
 *
 * @author 27gma
 */
public class ClassroomFXMLController implements Initializable {

    Dialogs dialog = new Dialogs();
    PrincipalFXMLController principal = new PrincipalFXMLController();
    ClassroomsModel classroomsModel = new ClassroomsModel();
    SectionsClassroomsModel sectionsClassroomsModel = new SectionsClassroomsModel();
    StageController stController = new StageController();
    SectionsModel sectionModel = new SectionsModel();
    UtilClass util = new UtilClass();
    Validation validation = new Validation();

    private SessionFactory sFactory;
    private Stage primaryStage;
    private int classroomId;
    private String code;
    private final Boolean ACTIVE = Boolean.TRUE;
    private final Boolean INACTIVE = Boolean.FALSE;

    final SpinnerValueFactory.IntegerSpinnerValueFactory capacityValue
            = new SpinnerValueFactory.IntegerSpinnerValueFactory(15, 60);
    
    @FXML
    private TableView<Classrooms> tableClassroom;
    @FXML
    private TableColumn<Classrooms, String> colCode;
    @FXML
    private TableColumn<Classrooms, String> colCapacity;
    @FXML
    private AnchorPane apElements;
    @FXML
    private TextField txtCode;
    @FXML
    private Spinner<Integer> spCapacity;
    @FXML
    private HBox hBBtn;
    @FXML
    private TableView<SectionsClassrooms> tableSectionsClassroom;
    @FXML
    private TableColumn<SectionsClassrooms, String> colGrade;
    @FXML
    private TableColumn<SectionsClassrooms, String> colSection;
    @FXML
    private TableColumn<SectionsClassrooms, String> colTurn;
    @FXML
    private JFXToggleButton tgEnable;
    @FXML
    private SearchField txtSearchClassrooms;
    @FXML
    private SearchField txtSearchSectionsClassroom;
    @FXML
    private JFXButton btnNew;
    @FXML
    private JFXButton btnUpdate;
    @FXML
    private JFXButton btnDisable;
    @FXML
    private JFXButton btnSave;
    @FXML
    private JFXButton btnSaveChange;
    @FXML
    private JFXButton btnCancel;
    @FXML
    private JFXButton btnAdd;
    @FXML
    private JFXButton btnDelete;
    @FXML
    private JFXButton btnBack;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        Icons();
        
        getSessionFactory();

        apElements.setVisible(false);
        txtSearchSectionsClassroom.setDisable(true);
        spCapacity.setValueFactory(capacityValue);

        enableButton(1, true);
        enableButton(2, true);

        colCode.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getCodeClassroom()));
        colCapacity.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(String.valueOf(cellData.getValue().getCapacity())));
        colGrade.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(util.getGrade(cellData.getValue().getSections().getGrades().getGradeName())));
        colSection.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(util.getLetter(cellData.getValue().getSections().getLetter())));
        colTurn.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(util.getTurn(cellData.getValue().getTurn())));

        searchClassrooms();

        loadClassrooms(ACTIVE);
    }
    
    public void Icons() {
        stController.Icon(btnNew, ImageClass.newElement);
        stController.Icon(btnUpdate, ImageClass.updateElement);
        stController.Icon(btnDisable, ImageClass.disableElement);
        stController.Icon(btnSave, ImageClass.save);
        stController.Icon(btnSaveChange, ImageClass.saveChange);
        stController.Icon(btnCancel, ImageClass.cancel);
        stController.Icon(btnAdd, ImageClass.newElement);
        stController.Icon(btnDelete, ImageClass.disableSubElement);
        stController.Icon(btnBack,ImageClass.back);
    }

    private void loadClassrooms(Boolean state) {
        try {
            classroomsModel.setSession(openSession());
            tableClassroom.setItems(classroomsModel.readClassrooms(state));
        } catch (NullPointerException ex) {
            System.err.println("Error en loadClassrooms " + ex.getMessage());
        }
    }

    protected void loadSectionsClassroom(Classrooms classrooms) {
        try {
            sectionsClassroomsModel.setSession(openSession());
            tableSectionsClassroom.setItems(sectionsClassroomsModel.readSectionsClassrooms(classrooms));
        } catch (NullPointerException ex) {
            System.err.println("Error en loadSectionsClassroom " + ex.getMessage());
        }
    }

    private void findClassrooms(Boolean state, String text) {
        try {
            classroomsModel.setSession(openSession());
            tableClassroom.setItems(classroomsModel.findClassrooms(state, text));
        } catch (NullPointerException ex) {
            System.err.println("Error en load " + ex.getMessage());
        }
    }

    private void findSectionsClassroom(Classrooms classrooms, String text) {
        try {
            sectionsClassroomsModel.setSession(openSession());
            tableSectionsClassroom.setItems(sectionsClassroomsModel.findSectionsClassrooms(classrooms, text));
        } catch (NullPointerException ex) {
            System.err.println("Error en load " + ex.getMessage());
        }
    }

    @FXML
    private void newClassroom(ActionEvent event) {
        this.apElements.setVisible(true);
        enableButton(1, true);
        enableButton(2, true);
        enableButton(3, true);
        showButton(1);
    }

    @FXML
    private void updateClassroom(ActionEvent event) {
        Classrooms selectClassroom = (Classrooms) tableClassroom.getSelectionModel().getSelectedItem();
        if (selectClassroom != null) {
            setClassrooms(selectClassroom);
            this.apElements.setVisible(true);
            enableButton(1, true);
            enableButton(2, true);
            enableButton(3, true);
            showButton(2);
        } else {
            dialog.Alerta("Advertencia", "No se ha encontrado el aula", "Por favor seleccione un aula en la tabla", Alert.AlertType.WARNING);
        }
    }

    @FXML
    private void disableClassroom(ActionEvent event) {
        Classrooms selectClassroom = (Classrooms) tableClassroom.getSelectionModel().getSelectedItem();
        if (selectClassroom != null) {
            if (tgEnable.isSelected()) {
                if (dialog.Confirm("Smart School", "Deshabilitar", "¿Esta seguro que quiere deshabilitar esta aula?")) {
                    if (tableSectionsClassroom.getItems().isEmpty()) {
                        active_inactive(selectClassroom, INACTIVE, 0);
                    } else {
                        dialog.Alerta("Smart School", "Error al deshabiitar", "Esta aula tiene asignada grupos", Alert.AlertType.ERROR);
                    }
                }
            } else {
                if (dialog.Confirm("Smart School", "Habilitar", "¿Esta seguro que quiere habilitar esta aula?")) {
                    active_inactive(selectClassroom, ACTIVE, 1);
                }
            }
        } else {
            dialog.Alerta("Advertencia", "No se ha encontrado el aula", "Por favor seleccione un aula en la tabla", Alert.AlertType.WARNING);
        }
    }

    private void active_inactive(Classrooms selectClassroom, Boolean state, int type) {
        selectClassroom.setState(state);
        classroomsModel.setSession(openSession());
        if (type == 0) {
            classroomsModel.delete(selectClassroom);
        } else {
            classroomsModel.active(selectClassroom);
        }
        loadClassrooms(!state);
        tableSectionsClassroom.getItems().clear();
    }

    @FXML
    private void showClassrooms(ActionEvent event) {
        String tgText = (tgEnable.isSelected()) ? "Habilitados" : "Deshabilitados";
        String btnText = (tgEnable.isSelected()) ? "Deshabilitar" : "Habilitar";
        boolean enable = !(tgEnable.isSelected());
        Boolean state = (tgEnable.isSelected()) ? ACTIVE : INACTIVE;

        tgEnable.setText(tgText);
        btnDisable.setText(btnText);
        enableButton(4, true);
        enableButton(6, enable);
        loadClassrooms(state);
        tableSectionsClassroom.getItems().clear();
    }

    private void searchClassrooms() {
        txtSearchClassrooms.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            Boolean state = (tgEnable.isSelected()) ? ACTIVE : INACTIVE;
            if (newValue.isEmpty()) {
                loadClassrooms(state);
            } else {
                findClassrooms(state, newValue);
            }
            txtSearchSectionsClassroom.setDisable(true);
            tableSectionsClassroom.getItems().clear();
        });
    }

    private void searchSectionsClassroom(Classrooms selectClassroom) {
            txtSearchSectionsClassroom.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
                if (newValue.isEmpty()) {
                    loadSectionsClassroom(selectClassroom);
                } else {
                    findSectionsClassroom(selectClassroom, newValue);
                }
            });
    }

    @FXML
    private void enableClassroomOption(MouseEvent event) {
        if (tgEnable.isSelected()) {
            enableButton(1, false);
        } else {
            enableButton(5, false);
        }
        Classrooms selecteClassroom = (Classrooms) tableClassroom.getSelectionModel().getSelectedItem();
        if (selecteClassroom != null) {
            loadSectionsClassroom(selecteClassroom);
            this.txtSearchSectionsClassroom.setDisable(false);
            searchSectionsClassroom(selecteClassroom);
        }
    }

    @FXML
    private void enableDetailsOption(MouseEvent event) {
        enableButton(2, false);
    }

    @FXML
    private void save(ActionEvent event) {
        if (isEmpty()) return;
        if (repeat_disable(0, 0)) return;
        if (repeat_disable(0, 1)) return;

        Classrooms c = getClassroom();
        classroomsModel.setSession(openSession());
        classroomsModel.save(c);
        clear();
        enableButton(3, false);
        apElements.setVisible(false);
        loadClassrooms(ACTIVE);
    }

    @FXML
    private void saveChange(ActionEvent event) {
        if (isEmpty()) return;
        if (repeat_disable(1, 0)) return;
        if (repeat_disable(1, 1)) return;

        Classrooms classrooms = getClassroom();
        classrooms.setClassroomId(getClassroomId());
        classroomsModel.setSession(openSession());
        classroomsModel.update(classrooms);
        clear();
        enableButton(3, false);
        apElements.setVisible(false);
        loadClassrooms(ACTIVE);
    }

    @FXML
    private void cancel(ActionEvent event) {
        apElements.setVisible(false);
        enableButton(3, false);
        clear();
    }

    @FXML
    private void addSectionsClassroom(ActionEvent event) {
        Classrooms selectClassroom = (Classrooms) tableClassroom.getSelectionModel().getSelectedItem();
        if (selectClassroom != null) {
            FXMLLoader load = new FXMLLoader();
            URL url = SectionsClassroomsFXMLController.class.getResource("/fxml/SectionsClassroomsFXML.fxml");
            Stage stage = stController.showStage(url, "Selección de grado", primaryStage, load);
            SectionsClassroomsFXMLController controller = load.getController();
            controller.setStage(stage);
            controller.setController(this);
            controller.setClassrooms(selectClassroom);
            controller.validRd(fillList());
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.resizableProperty().setValue(Boolean.FALSE);
            stage.show();
        } else {
            dialog.Alerta("Advertencia", "No se ha encontrado el aula", "Por favor seleccione un aula en la tabla", Alert.AlertType.WARNING);
        }

    }

    @FXML
    private void deleteSectionsClassroom(ActionEvent event) {
        Classrooms selectClassroom = (Classrooms) tableClassroom.getSelectionModel().getSelectedItem();
        SectionsClassrooms selectSectionClassrom = (SectionsClassrooms) tableSectionsClassroom.getSelectionModel().getSelectedItem();
        if (selectSectionClassrom != null) {
            selectSectionClassrom.setState(0);
            sectionsClassroomsModel.setSession(openSession());
            sectionsClassroomsModel.delete(selectSectionClassrom);
            loadSectionsClassroom(selectClassroom);
        } else {
            dialog.Alerta("Advertencia", "No se ha encontrado el aula", "Por favor seleccione un aula en la tabla", Alert.AlertType.WARNING);
        }
    }

    private Classrooms getClassroom() {
        return new Classrooms(this.txtCode.getText().toUpperCase(), spCapacity.getValue(), ACTIVE);
    }

    private void setClassrooms(Classrooms c) {
        this.txtCode.setText(c.getCodeClassroom());
        this.spCapacity.getValueFactory().setValue(c.getCapacity());
        setClassroomId(c.getClassroomId());
        setCode(c.getCodeClassroom());
    }

    private List<String> fillList() {
        List<String> turn = new ArrayList<>();
        for (SectionsClassrooms item : tableSectionsClassroom.getItems()) {
            turn.add(util.getTurn(item.getTurn()));
        }
        return turn;
    }

    private boolean isEmpty() {
        if (this.txtCode.getText().isEmpty()) {
            dialog.Alerta("Smart School", "Error", "No puede dejar campos vacios", Alert.AlertType.ERROR);
            return true;
        }
        return false;
    }

    private boolean repeat_disable(int type, int opc) {
        String code = this.txtCode.getText().toUpperCase();
        Boolean state = (opc == 0) ? INACTIVE : ACTIVE;
        boolean isDisable = (opc == 0);

        if (type == 0) {
            return isInsert(fillToCheck(state), code, isDisable);
        } else {
            if (!code.equals(getCode())) {
                return isInsert(fillToCheck(state), code, isDisable);
            }
        }
        return false;
    }

    private List<String> fillToCheck(Boolean state) {
        List<String> oldCode = new ArrayList<>();

        for (Classrooms item : classroomsModel.readClassrooms(state)) {
            oldCode.add(item.getCodeClassroom());
        }
        return oldCode;
    }

    private boolean isInsert(List<String> oldCode, String code, boolean isDisable) {
        if (oldCode.contains(code)) {
            String message = (isDisable)
                    ? "Esa aula esta deshabilitada, chequee en la sección de deshabilitados"
                    : "Esa aula ya existe";
            dialog.Alerta("Smart School", "Error",message, AlertType.ERROR);
            return true;
        }
        return false;
    }

    private void clear() {
        this.txtCode.setText("");
        this.spCapacity.getValueFactory().setValue(15);
    }

    @FXML
    private void back(ActionEvent event) {
        principal.back(event);
    }

    private Session openSession() {
        return sFactory.openSession();
    }

    private void getSessionFactory() {
        sFactory = HibernateUtil.getSessionFactory();
    }

    public int getClassroomId() {
        return classroomId;
    }

    public void setClassroomId(int classroomId) {
        this.classroomId = classroomId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    private void enableButton(int type, boolean opc) {
        switch (type) {
            case 1:
                this.btnUpdate.setDisable(opc);
                this.btnDisable.setDisable(opc);
                this.btnAdd.setDisable(opc);
                break;
            case 2:
                this.btnDelete.setDisable(opc);
                break;
            case 3:
                this.btnNew.setDisable(opc);
                this.btnBack.setDisable(opc);
                this.txtSearchClassrooms.setDisable(opc);
                this.txtSearchSectionsClassroom.setDisable(opc);
                break;
            case 4:
                this.btnUpdate.setDisable(opc);
                this.btnAdd.setDisable(opc);
                this.btnDisable.setDisable(true);
                break;
            case 5:
                this.btnDisable.setDisable(opc);
                break;
            case 6:
                this.btnNew.setDisable(opc);
                break;
        }
    }

    private void showButton(int opc) {
        switch (opc) {
            //Nuevo
            case 1:
                add_remove(2, btnCancel);
                add_remove(2, btnSaveChange);
                add_remove(1, btnSave);
                add_remove(1, btnCancel);
                break;
            //Actualizar    
            case 2:
                add_remove(2, btnCancel);
                add_remove(2, btnSave);
                add_remove(1, btnSaveChange);
                add_remove(1, btnCancel);
                break;
        }
    }

    private void add_remove(int opc, Node node) {
        switch (opc) {
            //Agregar
            case 1:
                if (!this.hBBtn.getChildren().contains(node)) {
                    this.hBBtn.getChildren().add(node);
                }
                break;
            //Remover    
            case 2:
                if (this.hBBtn.getChildren().contains(node)) {
                    this.hBBtn.getChildren().remove(node);
                }
                break;
        }
    }
}
