/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.controller;

import com.crazycode.custom.elements.SearchField;
import com.crazycode.mask.elements.EmailField;
import com.crazycode.mask.elements.IntegerField;
import com.crazycode.mask.elements.MaskField;
import com.developerfriends.edu.smartschool.model.AdvancePaymentsModel;
import com.developerfriends.edu.smartschool.model.EnrollmentModel;
import com.developerfriends.edu.smartschool.model.SectionsClassroomsModel;
import com.developerfriends.edu.smartschool.model.UserModel;
import com.developerfriends.edu.smartschool.pojo.Advancepayments;
import com.developerfriends.edu.smartschool.pojo.Classrooms;
import com.developerfriends.edu.smartschool.pojo.Employees;
import com.developerfriends.edu.smartschool.pojo.Enrollment;
import com.developerfriends.edu.smartschool.pojo.Persons;
import com.developerfriends.edu.smartschool.pojo.SectionsClassrooms;
import com.developerfriends.edu.smartschool.pojo.Students;
import com.developerfriends.edu.smartschool.pojo.Tutors;
import com.developerfriends.edu.smartschool.util.Dialogs;
import com.developerfriends.edu.smartschool.util.HibernateUtil;
import com.developerfriends.edu.smartschool.util.ImageClass;
import com.developerfriends.edu.smartschool.util.StageController;
import com.developerfriends.edu.smartschool.util.UtilClass;
import com.developerfriends.edu.smartschool.util.ValidateIdentityCard;
import com.developerfriends.edu.smartschool.util.Validation;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.Set;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 * FXML Controller class
 *
 * @author Enmnauel Calero
 */
public class EnrollmentFXMLController implements Initializable {

    StageController stageController = new StageController();
    PrincipalFXMLController principal = new PrincipalFXMLController();
    UtilClass util = new UtilClass();
    Dialogs dialog = new Dialogs();
    ObservableList<Tutors> tutorsList = FXCollections.observableArrayList();
    Tutors tutorsTemp = new Tutors();
    int tutorIdTemp = 0;
    boolean isNew = false;
    ValidateIdentityCard validCard = new ValidateIdentityCard();
    Validation validation = new Validation();
    private SessionFactory sFactory;
    private Stage primaryStage;
    SectionsClassroomsModel sectionsClassroomsModel = new SectionsClassroomsModel();
    Advancepayments advancepaymentsToEnrollment = new Advancepayments();
    @FXML
    protected ImageView ImageViewPerson;
    @FXML
    private JFXRadioButton rbMale;
    @FXML
    private JFXRadioButton rbFemale;
    @FXML
    private JFXButton btnSelect;
    @FXML
    private JFXButton btnTake;
    @FXML
    private JFXButton btnBack;
    @FXML
    private AnchorPane apEmployee;
    @FXML
    private AnchorPane apElements;
    @FXML
    private TextField txtFirstName;
    @FXML
    private TextField txtSecondName;
    @FXML
    private TextField txtFirstLastName;
    @FXML
    private TextField txtSecondLastName;
    @FXML
    private MaskField txtIdentityCard;
    @FXML
    private MaskField txtPhone;
    @FXML
    private EmailField txtEmail;
    @FXML
    private TextField txtjob;
    @FXML
    private TextField txtWorkPlace;
    @FXML
    private TextArea txtAddress;
    @FXML
    private HBox hBBtn;
    @FXML
    private TableView<Tutors> tableTutors;
    @FXML
    private TableColumn<Tutors, String> colName;
    @FXML
    private TableColumn<Tutors, String> colIdentityCard;
    @FXML
    private TableColumn<Tutors, String> colGender;
    @FXML
    private TableColumn<Tutors, String> colAge;
    @FXML
    private TableColumn<Tutors, String> colPhone;
    @FXML
    private TableColumn<Tutors, String> colEmail;
    @FXML
    private TableColumn<Tutors, String> colAddress;
    @FXML
    private JFXRadioButton rbMaleTutors;
    @FXML
    private JFXRadioButton rbFemaleTutors;
    @FXML
    protected ImageView imgViewTutors;
    @FXML
    private JFXButton btnSelectTutor;
    @FXML
    private JFXButton btnTakeTutors;
    @FXML
    private JFXButton btnSaveTutors;
    @FXML
    private JFXButton btnCancelTutors;
    @FXML
    private JFXButton btnNewTutors;
    @FXML
    private JFXButton btnUpateTutors;
    @FXML
    private TextField txtFistNameStudent;
    @FXML
    private TextField txtSecondNameStudent;
    @FXML
    private TextField txtFirstLastnameStudent;
    @FXML
    private TextField txtSecondLastnameStudent;
    @FXML
    private DatePicker birthdayStudent;
    @FXML
    private TextArea txtAddressStudent;
    @FXML
    private TabPane tapPaneEnrollment;
    @FXML
    private TableView<SectionsClassrooms> tableSectionsClassroom;
    @FXML
    private TableColumn<SectionsClassrooms, String> colGrade;
    @FXML
    private TableColumn<SectionsClassrooms, String> colSection;
    @FXML
    private TableColumn<SectionsClassrooms, String> colTurn;
    @FXML
    private SearchField txtSearchSectionsClassroom;
    @FXML
    private TextField txtName;
    @FXML
    private TextField txtLastName;
    @FXML
    private Label lblPrice;
    @FXML
    private FlowPane panelClassroom;
    @FXML
    private Label classroomCode;
    @FXML
    private Label classroomCapacity;
    @FXML
    private Label classroomCapacity1;
    @FXML
    private IntegerField txtEnrrollment;
    @FXML
    private JFXButton btnGenerateEnrollment;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        getSessionFactory();
        icon();
        initTableTutor();
        initTableSectionsClassroom();
        searchSectionsClassroom();
        loadSectionsClassroom();
    }

    private Session openSession() {
        return sFactory.openSession();
    }

    private void getSessionFactory() {
        sFactory = HibernateUtil.getSessionFactory();
    }

    public void icon() {
        stageController.Icon(btnSelect, ImageClass.selectPicture);
        stageController.Icon(btnTake, ImageClass.camera);
        stageController.Icon(btnSelectTutor, ImageClass.selectPicture);
        stageController.Icon(btnTakeTutors, ImageClass.camera);
        stageController.Icon(btnBack, ImageClass.back);
        stageController.Icon(btnCancelTutors, ImageClass.cancel);
        stageController.Icon(btnSaveTutors,ImageClass.save);
        stageController.Icon(btnNewTutors, ImageClass.newElement);
        stageController.Icon(btnUpateTutors, ImageClass.updateElement);
        stageController.Icon(btnGenerateEnrollment, ImageClass.clipboard);
    }

    private Employees getCurrentEmployee() {
        UserModel userModel = new UserModel();
        userModel.setSession(openSession());
        Persons p = userModel.getPersonByUserId(UserModel.getCurrentUser().getUserId());
        Set<Employees> em = p.getEmployeeses();
        Employees employee = (Employees) em.toArray()[0];
        return employee;
    }

    /*Datos Personales*/
    private boolean isEmpty() {
        return this.txtFistNameStudent.getText().isEmpty()
                || this.txtSecondNameStudent.getText().isEmpty()
                || this.txtFirstLastnameStudent.getText().isEmpty()
                || this.txtSecondLastnameStudent.getText().isEmpty()
                || this.txtAddressStudent.getText().isEmpty()
                || ((!this.rbFemale.isSelected()) && (!this.rbMale.isSelected()));
    }

    @FXML
    private void checkMale(ActionEvent event) {
        if (rbFemale.isSelected()) {
            rbFemale.setSelected(false);
            rbMale.setSelected(true);
        }
    }

    @FXML
    private void checkFemale(ActionEvent event) {
        if (rbMale.isSelected()) {
            rbMale.setSelected(false);
            rbFemale.setSelected(true);
        }
    }

    @FXML
    private void back(ActionEvent event) {
        principal.back(event);
    }

    @FXML
    private void openFile(ActionEvent event) {
        if (util.openFile(event) != null) {
            ImageViewPerson.setImage(util.openFile(event));
        }
    }

    @FXML
    private void openCamera(ActionEvent event) {
        FXMLLoader load = new FXMLLoader();
        URL url = CameraFXMLController.class.getResource("/fxml/CameraFXML.fxml");
        Stage stage = stageController.showStage(url, "Camara", primaryStage, load);
        CameraFXMLController controller = load.getController();
        controller.setEmrollment(this);
        controller.setStage(stage);
        controller.setType(2);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(StageStyle.TRANSPARENT);
        stage.show();
    }


    /*Tutores*/
    @FXML
    private void checkMaleTutors(ActionEvent event) {
        if (rbFemaleTutors.isSelected()) {
            rbFemaleTutors.setSelected(false);
            rbMaleTutors.setSelected(true);
        }
    }

    @FXML
    private void checkFemaleTutors(ActionEvent event) {
        if (rbMaleTutors.isSelected()) {
            rbMaleTutors.setSelected(false);
            rbFemaleTutors.setSelected(true);
        }
    }

    @FXML
    private void openFileTutors(ActionEvent event) {
        if (util.openFile(event) != null) {
            imgViewTutors.setImage(util.openFile(event));
        }
    }

    @FXML
    private void openCameraTutors(ActionEvent event) {
        FXMLLoader load = new FXMLLoader();
        URL url = CameraFXMLController.class.getResource("/fxml/CameraFXML.fxml");
        Stage stage = stageController.showStage(url, "Camara", primaryStage, load);
        CameraFXMLController controller = load.getController();
        controller.setEmrollment(this);
        controller.setStage(stage);
        controller.setType(3);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(StageStyle.TRANSPARENT);
        stage.show();
    }

    private void initTableTutor() {
        colName.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getPersons().getPersonName()));
        colIdentityCard.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getIdentityCard()));
        colGender.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(util.gender(cellData.getValue().getPersons().getGender())));
        colAge.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(String.valueOf(util.age(cellData.getValue().getPersons().getBirthday()))));
        colPhone.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getPhone()));
        colEmail.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getEmail()));
        colAddress.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getPersons().getAddress()));
    }

    private void clear() {
        this.txtFirstName.setText("");
        this.txtSecondName.setText("");
        this.txtFirstLastName.setText("");
        this.txtSecondLastName.setText("");
        this.txtEmail.setText("");
        clearMask(txtPhone);
        clearMask(txtIdentityCard);
        this.txtAddress.setText("");
        this.imgViewTutors.setImage(imageDefault());
        this.tableTutors.setDisable(false);
    }

    private void clearMask(MaskField txt) {
        if (txt.getText().isEmpty()) {
            txt.setText("");
        } else {
            txt.replaceText(0, txt.lengthProperty().get(), "");
        }
    }

    private Image imageDefault() {
        return new Image(getClass().getResource("/images/default-user-image.png").toExternalForm());
    }

    private void setTutor(Tutors tutor) {
        this.txtFirstName.setText(tutor.getPersons().getFirstName());
        this.txtSecondName.setText(tutor.getPersons().getSecondName());
        this.txtFirstLastName.setText(tutor.getPersons().getFirstLastname());
        this.txtSecondLastName.setText(tutor.getPersons().getSecondLastname());
        this.txtEmail.setText(tutor.getEmail());
        this.txtPhone.setText(tutor.getPhone());
        this.txtIdentityCard.setText(tutor.getIdentityCard());
        this.txtAddress.setText(tutor.getPersons().getAddress());
        this.txtjob.setText(tutor.getJob());
        this.txtWorkPlace.setText(tutor.getWorkPlace());
        this.rbMaleTutors.setSelected(tutor.getPersons().getGender());
        this.rbFemaleTutors.setSelected(!tutor.getPersons().getGender());
        this.imgViewTutors.setImage(util.getImage(tutor.getPersons().getPhoto()));
    }

    private boolean isEmptyTutors() {
        if (this.txtFirstName.getText().isEmpty()
                || this.txtSecondName.getText().isEmpty()
                || this.txtFirstLastName.getText().isEmpty()
                || this.txtSecondLastName.getText().isEmpty()
                || this.txtPhone.getText().isEmpty()
                || this.txtIdentityCard.getText().isEmpty()
                || this.txtAddress.getText().isEmpty()
                || this.txtjob.getText().isEmpty()
                || this.txtWorkPlace.getText().isEmpty()
                || ((!this.rbFemaleTutors.isSelected()) && (!this.rbMaleTutors.isSelected()))) {
            dialog.Alerta("SmartSchool", "Error", "No puede dejar campos vacios", Alert.AlertType.ERROR);
            return true;
        }
        return false;
    }

    private boolean isOkFiled() {
        List<Boolean> okField = new ArrayList<>();
        okField.add(validation.isEmail(this.txtEmail.getText()));
        okField.add(validation.isIdentityCard(this.txtIdentityCard.getText()));
        okField.add(validation.isPhone(this.txtPhone.getText()));
        if (okField.contains(false)) {
            dialog.Alerta("Smart School", "Campos mal escritos",
                    ((okField.get(0)) ? "" : "Correo mal escrito") + "\n"
                    + ((okField.get(1)) ? "" : "Cédula mal escrita") + "\n"
                    + ((okField.get(2)) ? "" : "Teléfono mal escrito"),
                    Alert.AlertType.WARNING);
            return true;
        }
        return false;
    }

    private boolean isOkIdentityCard() {
        validCard.setIdentityCard(this.txtIdentityCard.getText());
        if (!validCard.isValidIdentityCard()) {
            dialog.Alerta("Smart School", "Cédula", "La cédula insertada no es valida", Alert.AlertType.WARNING);
            return true;
        }
        return false;
    }

    @FXML
    private void newTutor() {
        this.isNew = true;
        this.apElements.setVisible(true);
        this.tableTutors.setDisable(true);
    }

    @FXML
    private void addTutor() {
        if (isEmptyTutors()) {
            return;
        }
        if (isOkFiled()) {
            return;
        }
        if (isOkIdentityCard()) {
            return;
        }
        if (isNew) {
            BufferedImage img = SwingFXUtils.fromFXImage(imgViewTutors.getImage(), null);
            Boolean gender = (this.rbMaleTutors.isSelected()) ? Boolean.TRUE : Boolean.FALSE;
            Persons person = new Persons(this.txtFirstName.getText(),
                    this.txtSecondName.getText(),
                    this.txtFirstLastName.getText(),
                    this.txtSecondLastName.getText(),
                    gender,
                    util.getBirthday(this.txtIdentityCard.getText()),
                    this.txtAddress.getText(),
                    util.saveImage(img),
                    Boolean.TRUE);

            Tutors tutor = new Tutors(tutorIdTemp, person,
                    this.txtIdentityCard.getText(),
                    this.txtPhone.getText(),
                    this.txtEmail.getText(),
                    this.txtjob.getText(),
                    this.txtWorkPlace.getText());

            tutorIdTemp++;
            tutorsList.add(tutor);
            tableTutors.setItems(tutorsList);

        } else {
            BufferedImage img = SwingFXUtils.fromFXImage(imgViewTutors.getImage(), null);
            Boolean gender = (this.rbMaleTutors.isSelected()) ? Boolean.TRUE : Boolean.FALSE;
            Persons person = new Persons(this.txtFirstName.getText(),
                    this.txtSecondName.getText(),
                    this.txtFirstLastName.getText(),
                    this.txtSecondLastName.getText(),
                    gender,
                    util.getBirthday(this.txtIdentityCard.getText()),
                    this.txtAddress.getText(),
                    util.saveImage(img),
                    Boolean.TRUE);
            tutorsList.forEach((Tutors tutor) -> {
                if (Objects.equals(tutor.getTutorId(), tableTutors.getSelectionModel().getSelectedItem().getTutorId())) {
                    tutor.setPersons(person);
                    tutor.setIdentityCard(this.txtIdentityCard.getText());
                    tutor.setPhone(this.txtPhone.getText());
                    tutor.setEmail(this.txtEmail.getText());
                    tutor.setJob(this.txtjob.getText());
                    tutor.setWorkPlace(this.txtWorkPlace.getText());
                }

            });
            tableTutors.refresh();
        }
        Cancel();
        this.btnUpateTutors.setDisable(true);
    }

    @FXML
    private void Cancel() {
        apElements.setVisible(false);
        this.btnUpateTutors.setDisable(true);
        clear();
    }

    @FXML
    private void selectTutor() {
        tutorsTemp = tableTutors.getSelectionModel().getSelectedItem();
        if (tutorsTemp != null) {
            this.btnUpateTutors.setDisable(false);
        }
    }

    @FXML
    private void editTutuor() {
        this.isNew = false;
        this.apElements.setVisible(true);
        setTutor(tutorsTemp);
    }

    /*Grado*/
    private void initTableSectionsClassroom() {
        colGrade.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(util.getGrade(cellData.getValue().getSections().getGrades().getGradeName())));
        colSection.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(util.getLetter(cellData.getValue().getSections().getLetter())));
        colTurn.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(util.getTurn(cellData.getValue().getTurn())));
    }

    protected void loadSectionsClassroom() {
        try {
            sectionsClassroomsModel.setSession(openSession());
            tableSectionsClassroom.setItems(sectionsClassroomsModel.readAllSectionsClassrooms());
        } catch (NullPointerException ex) {
            System.err.println("Error en loadSectionsClassroom " + ex.getMessage());
        }
    }

    private void findSectionsClassroom(String text) {
        try {
            sectionsClassroomsModel.setSession(openSession());
            tableSectionsClassroom.setItems(sectionsClassroomsModel.findSectionsClassrooms(text));
        } catch (NullPointerException ex) {
            System.err.println("Error en load " + ex.getMessage());
        }
    }

    private void searchSectionsClassroom() {
        txtSearchSectionsClassroom.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (newValue.isEmpty()) {
                loadSectionsClassroom();
            } else {
                findSectionsClassroom(newValue);
            }
        });
    }

    @FXML
    private void selectSectionsClassroom() {
        SectionsClassrooms sectionsClassroom = tableSectionsClassroom.getSelectionModel().getSelectedItem();
        if (sectionsClassroom != null) {
            sectionsClassroomsModel.setSession(openSession());
            panelClassroom.setVisible(true);
            Classrooms classroom = sectionsClassroomsModel.getClassrooms(sectionsClassroom.getSectionClassroomId());
            classroomCapacity.setText(classroom.getCapacity().toString());
            classroomCode.setText(classroom.getCodeClassroom());
        }
    }

    
    private Classrooms getCurrentClassroom(){
        Classrooms classroom = new Classrooms();
        SectionsClassrooms sectionsClassroom = tableSectionsClassroom.getSelectionModel().getSelectedItem();
        if (sectionsClassroom != null) {
            sectionsClassroomsModel.setSession(openSession());
            classroom = sectionsClassroomsModel.getClassrooms(sectionsClassroom.getSectionClassroomId());
        }
                    return classroom;
        
    }
    /*Pago de Matricula*/
    @FXML
    private void active(ActionEvent event) {
        AdvancePaymentsModel advancePaymentsModel = new AdvancePaymentsModel();
        advancePaymentsModel.setSession(openSession());
        advancepaymentsToEnrollment = advancePaymentsModel.getEnroollmentPayment(
                Integer.parseInt(this.txtEnrrollment.getText()));
        if (advancepaymentsToEnrollment != null) {
            this.txtName.setText(advancepaymentsToEnrollment.getFirstName());
            this.txtLastName.setText(advancepaymentsToEnrollment.getFirstLastname());
            this.lblPrice.setText(advancepaymentsToEnrollment.getCost().toString());
        }
    }

    @FXML
    private void generateEntollment() {
        EnrollmentModel enrollmentModel = new EnrollmentModel();
        BufferedImage img = SwingFXUtils.fromFXImage(ImageViewPerson.getImage(), null);
        Boolean gender = (this.rbMale.isSelected()) ? Boolean.TRUE : Boolean.FALSE;
        Persons personStudent = new Persons(this.txtFistNameStudent.getText(),
                this.txtSecondNameStudent.getText(),
                this.txtFirstLastnameStudent.getText(),
                this.txtSecondLastnameStudent.getText(),
                gender,
                java.sql.Date.valueOf(this.birthdayStudent.getValue()),
                this.txtAddress.getText(),
                util.saveImage(img),
                Boolean.TRUE);
        Students student = new Students(personStudent,
                "",
                true);
        Enrollment enrollment = new Enrollment(advancepaymentsToEnrollment, 
                getCurrentClassroom(),
                getCurrentEmployee(), 
                student, new Date());
        enrollmentModel.setSession(openSession());
        enrollmentModel.save(enrollment);
    }

}
