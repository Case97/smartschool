/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.controller;

import com.crazycode.custom.elements.SearchField;
import com.crazycode.mask.elements.DoubleField;
import com.developerfriends.edu.smartschool.model.DutyModel;
import com.developerfriends.edu.smartschool.pojo.Duty;
import com.developerfriends.edu.smartschool.util.Dialogs;
import com.developerfriends.edu.smartschool.util.HibernateUtil;
import com.developerfriends.edu.smartschool.util.ImageClass;
import com.developerfriends.edu.smartschool.util.StageController;
import com.developerfriends.edu.smartschool.util.UtilClass;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXToggleButton;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 * FXML Controller class
 *
 * @author Tanieska
 */
public class DutyFXMLController implements Initializable {

    Dialogs dialog = new Dialogs();
    PrincipalFXMLController principal = new PrincipalFXMLController();
    DutyModel dutyModel = new DutyModel();
    UtilClass util = new UtilClass();
    StageController stController = new StageController();

    private SessionFactory sFactory;
    private int tariffId;
    private String name;
    private final Boolean ACTIVE = Boolean.TRUE;
    private final Boolean INACTIVE = Boolean.FALSE;

    @FXML
    private TableView<Duty> tableDuty;
    @FXML
    private TableColumn<Duty, String> colNameTariff;
    @FXML
    private TableColumn<Duty, String> colPrice;
    @FXML
    private ComboBox<String> cmbCommonPayments;
    @FXML
    private HBox hBBtn;
    @FXML
    private AnchorPane apElements;
    @FXML
    private TextField txtOtherPayments;
    @FXML
    private DoubleField txtPriceCommonPayments;
    @FXML
    private DoubleField txtPriceOtherPayments;
    @FXML
    private Tab tabCommonPayments;
    @FXML
    private Tab tabOtherPayments;
    @FXML
    private TabPane tab;
    @FXML
    private SearchField txtSearch;
    @FXML
    private JFXToggleButton tgEnable;
    @FXML
    private JFXButton btnNewDuty;
    @FXML
    private JFXButton btnUpdateDuty;
    @FXML
    private JFXButton btnDisableDuty;
    @FXML
    private JFXButton btnSave;
    @FXML
    private JFXButton btnSaveChange;
    @FXML
    private JFXButton btnCancel;
    @FXML
    private JFXButton btnBack;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        Icons();

        getSessionFactory();

        apElements.setVisible(false);
        enableButton(1, true);

        colNameTariff.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getNameTariff()));
        colPrice.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(String.valueOf(cellData.getValue().getPrice())));

        search();
        load(ACTIVE);

        cmbCommonPayments.setItems(payments());
    }

    public void Icons() {
        stController.Icon(btnNewDuty, ImageClass.newElement);
        stController.Icon(btnUpdateDuty, ImageClass.updateElement);
        stController.Icon(btnDisableDuty, ImageClass.disableElement);
        stController.Icon(btnSave, ImageClass.save);
        stController.Icon(btnSaveChange, ImageClass.saveChange);
        stController.Icon(btnCancel, ImageClass.cancel);
        stController.Icon(btnBack, ImageClass.back);
    }

    private void load(Boolean state) {
        try {
            dutyModel.setSession(openSession());
            tableDuty.setItems(dutyModel.readDuty(state));
        } catch (NullPointerException ex) {
            System.err.println("Error en el load " + ex.getMessage());
        }
    }

    private void find(String text, Boolean state) {
        try {
            dutyModel.setSession(openSession());
            tableDuty.setItems(dutyModel.findDuty(text, state));
        } catch (NullPointerException ex) {
            System.err.println("Error en el load " + ex.getMessage());
        }
    }

    @FXML
    private void showDuty(ActionEvent event) {
        String tgText = (tgEnable.isSelected()) ? "Habilitados" : "Deshabilitados";
        String btnText = (tgEnable.isSelected()) ? "Deshabilitar" : "Habilitar";
        boolean enable = !(tgEnable.isSelected());
        Boolean state = (tgEnable.isSelected()) ? ACTIVE : INACTIVE;

        tgEnable.setText(tgText);
        btnDisableDuty.setText(btnText);
        enableButton(3, true);
        enableButton(5, enable);
        load(state);
    }

    @FXML
    private void newDuty(ActionEvent event) {
        this.apElements.setVisible(true);
        enableButton(1, true);
        enableButton(2, true);
        showButton(1);
        if (payments().isEmpty()) {
            this.tab.getSelectionModel().select(tabOtherPayments);
            tabCommonPayments.setDisable(true);
        }
    }

    @FXML
    private void updateDuty(ActionEvent event) {
        Duty selectDuty = (Duty) tableDuty.getSelectionModel().getSelectedItem();
        if (selectDuty != null) {
            setDuty(selectDuty);
            this.apElements.setVisible(true);
            enableButton(1, true);
            enableButton(2, true);
            showButton(2);
        } else {
            dialog.Alerta("Advertencia", "No se ha encontrado el aula", "Por favor seleccione un aula en la tabla", Alert.AlertType.WARNING);
        }
    }

    @FXML
    private void disableDuty(ActionEvent event) {
        Duty selectDuty = (Duty) tableDuty.getSelectionModel().getSelectedItem();
        if (selectDuty != null) {
            if (tgEnable.isSelected()) {
                if (dialog.Confirm("Smart School", "Deshabilitar", "¿Esta seguro que quiere deshabilitar este arancel?")) {
                    active_inactive(selectDuty, INACTIVE, 0);
                }
            } else {
                if (dialog.Confirm("Smart School", "Habilitar", "¿Esta seguro que quiere habilitar este arancel?")) {
                    active_inactive(selectDuty, ACTIVE, 1);
                }
            }
        } else {
            dialog.Alerta("Advertencia", "No se ha encontrado el grado", "Por favor seleccione un grado en la tabla", Alert.AlertType.WARNING);
        }
    }

    private void active_inactive(Duty selectGrades, Boolean state, int type) {
        selectGrades.setState(state);
        dutyModel.setSession(openSession());
        if (type == 0) {
            dutyModel.delete(selectGrades);
        } else {
            dutyModel.active(selectGrades);
        }
        load(!state);
    }

    @FXML
    private void enableOption(MouseEvent event) {
        if (tgEnable.isSelected()) {
            enableButton(1, false);
        } else {
            enableButton(4, false);
        }
    }

    @FXML
    private void save(ActionEvent event) {
        if (isEmpty()) {
            return;
        }
        if (repeat_disable(0, 0)) {
            return;
        }
        if (repeat_disable(0, 1)) {
            return;
        }
        if (isMinPrice()) {
            return;
        }
        if (isCommonPayment()) {
            return;
        }

        Duty duty = getDuty();
        dutyModel.setSession(openSession());
        dutyModel.save(duty);
        clear();
        enableButton(2, false);
        apElements.setVisible(false);
        load(ACTIVE);
        cmbCommonPayments.setItems(payments());
    }

    @FXML
    private void saveChange(ActionEvent event) {
        if (isEmpty()) {
            return;
        }
        if (repeat_disable(1, 0)) {
            return;
        }
        if (repeat_disable(1, 1)) {
            return;
        }
        if (isMinPrice()) {
            return;
        }
        if (isCommonPayment()) {
            return;
        }

        Duty duty = getDuty();
        duty.setTariffId(getTariffId());
        dutyModel.setSession(openSession());
        dutyModel.update(duty);
        clear();
        enableButton(2, false);
        apElements.setVisible(false);
        load(ACTIVE);
    }

    private void search() {
        txtSearch.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            Boolean state = (tgEnable.isSelected()) ? ACTIVE : INACTIVE;
            if (newValue.isEmpty()) {
                load(state);
            } else {
                find(newValue, state);
            }
        });
    }

    @FXML
    private void cancel(ActionEvent event) {
        apElements.setVisible(false);
        enableButton(1, true);
        enableButton(2, false);
        clear();
    }

    @FXML
    private void back(ActionEvent event) {
        principal.back(event);
    }

    private void clear() {
        this.txtOtherPayments.setText("");
        this.txtPriceOtherPayments.setText("");
        this.cmbCommonPayments.getSelectionModel().select("");
        this.cmbCommonPayments.getSelectionModel().clearSelection();
        this.txtPriceCommonPayments.setText("");
        tab.getSelectionModel().clearAndSelect(0);
        tabCommonPayments.setDisable(false);
        tabOtherPayments.setDisable(false);
        this.cmbCommonPayments.setDisable(false);
    }

    private void setDuty(Duty duty) {
        setTariffId(duty.getTariffId());
        if (util.fillPayment().contains(duty.getNameTariff())) {
            this.tab.getSelectionModel().select(tabCommonPayments);
            tabOtherPayments.setDisable(true);
            this.txtPriceCommonPayments.setText(String.valueOf(duty.getPrice()));
            this.cmbCommonPayments.getSelectionModel().select(duty.getNameTariff());
            this.cmbCommonPayments.setDisable(true);
        } else {
            this.tab.getSelectionModel().select(tabOtherPayments);
            tabCommonPayments.setDisable(true);
            this.txtPriceOtherPayments.setText(String.valueOf(duty.getPrice()));
            this.txtOtherPayments.setText(duty.getNameTariff());
        }
    }

    private boolean isEmpty() {
        if (this.tabCommonPayments.isSelected()) {
            if (this.txtPriceCommonPayments.getText().isEmpty()
                    || this.cmbCommonPayments.getSelectionModel().isEmpty()) {
                dialog.Alerta("Smart School", "Error", "No puede dejar campos vacios", Alert.AlertType.ERROR);
                return true;
            }
        } else {
            if (this.txtOtherPayments.getText().isEmpty()
                    || this.txtPriceOtherPayments.getText().isEmpty()) {
                dialog.Alerta("Smart School", "Error", "No puede dejar campos vacios", Alert.AlertType.ERROR);
                return true;
            }
        }
        return false;
    }

    private boolean repeat_disable(int type, int opc) {
        String nameTariff = (this.tabCommonPayments.isSelected())
                ? this.cmbCommonPayments.getSelectionModel().getSelectedItem().toUpperCase()
                : this.txtOtherPayments.getText().toUpperCase();
        Boolean state = (opc == 0) ? INACTIVE : ACTIVE;
        boolean isDisable = (opc == 0);

        if (type == 0) {
            return isInsert(fillToCheck(state), nameTariff, isDisable);
        } else {
            if (!nameTariff.equals(getName())) {
                return isInsert(fillToCheck(state), nameTariff, isDisable);
            }
        }
        return false;
    }

    private List<String> fillToCheck(Boolean state) {
        List<String> oldDuty = new ArrayList<>();

        for (Duty item : dutyModel.readDuty(state)) {
            oldDuty.add(item.getNameTariff());
        }
        return oldDuty;
    }

    private boolean isInsert(List<String> oldDuty, String nameTariff, boolean isDisable) {
        if (oldDuty.contains(nameTariff)) {
            String message = (isDisable)
                    ? "Ese arancel deshabilitado, chequee en la sección de deshabilitados"
                    : "Ese arancel ya existe";
            dialog.Alerta("Smart School", "Error", message, Alert.AlertType.ERROR);
            return true;
        }
        return false;
    }

    private boolean isMinPrice() {
        float price = (tabCommonPayments.isSelected())
                ? Float.parseFloat(txtPriceCommonPayments.getText())
                : Float.parseFloat(txtPriceOtherPayments.getText());
        if (price == 0) {
            dialog.Alerta("Smart School", "Error", "Ingrese una cantidad valida", Alert.AlertType.ERROR);
            return true;
        }
        return false;
    }

    private boolean isCommonPayment() {
        if (util.fillPayment().contains(this.txtOtherPayments.getText())) {
            dialog.Alerta("Smart School", "Error", "Ese es un  pago común", Alert.AlertType.ERROR);
            return true;
        }
        return false;
    }

    private Duty getDuty() {
        String nameTariff;
        Float price;
        nameTariff = (this.tabCommonPayments.isSelected())
                ? this.cmbCommonPayments.getSelectionModel().getSelectedItem().toUpperCase()
                : this.txtOtherPayments.getText().toUpperCase();
        price = (this.tabCommonPayments.isSelected())
                ? Float.parseFloat(this.txtPriceCommonPayments.getText())
                : Float.parseFloat(this.txtPriceOtherPayments.getText());

        return new Duty(nameTariff, price, ACTIVE);
    }

    private List<String> selectedPayments() {
        List<String> payments = new ArrayList<>();
        for (Duty item : tableDuty.getItems()) {
            payments.add(item.getNameTariff());
        }
        return payments;
    }

    private Session openSession() {
        return sFactory.openSession();
    }

    private void getSessionFactory() {
        sFactory = HibernateUtil.getSessionFactory();
    }

    public int getTariffId() {
        return tariffId;
    }

    public void setTariffId(int tariffId) {
        this.tariffId = tariffId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private ObservableList<String> payments() {
        ObservableList<String> payments = FXCollections.observableArrayList();
        payments.addAll(util.setPaymentsAvaible(selectedPayments()));
        return payments;
    }

    private void enableButton(int type, boolean opc) {
        switch (type) {
            case 1:
                this.btnUpdateDuty.setDisable(opc);
                this.btnDisableDuty.setDisable(opc);
                break;
            case 2:
                this.btnNewDuty.setDisable(opc);
                this.btnBack.setDisable(opc);
                this.tgEnable.setDisable(opc);
                this.txtSearch.setDisable(opc);
                break;
            case 3:
                this.btnUpdateDuty.setDisable(opc);
                this.btnDisableDuty.setDisable(true);
                break;
            case 4:
                this.btnDisableDuty.setDisable(opc);
                break;
            case 5:
                this.btnNewDuty.setDisable(opc);
                break;

        }
    }

    private void showButton(int opc) {
        switch (opc) {
            //Nuevo
            case 1:
                add_remove(2, btnCancel);
                add_remove(2, btnSaveChange);
                add_remove(1, btnSave);
                add_remove(1, btnCancel);
                break;
            //Actualizar    
            case 2:
                add_remove(2, btnCancel);
                add_remove(2, btnSave);
                add_remove(1, btnSaveChange);
                add_remove(1, btnCancel);
                break;
        }
    }

    private void add_remove(int opc, Node node) {
        switch (opc) {
            //Agregar
            case 1:
                if (!this.hBBtn.getChildren().contains(node)) {
                    this.hBBtn.getChildren().add(node);
                }
                break;
            //Remover    
            case 2:
                if (this.hBBtn.getChildren().contains(node)) {
                    this.hBBtn.getChildren().remove(node);
                }
                break;
        }
    }

}
