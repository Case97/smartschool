/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.util;

import javafx.geometry.Pos;
import org.controlsfx.control.Notifications;

/**
 *
 * @author Enmnauel Calero
 */
public class PopUpMessage {

    public void informationMessage(String message) {
        Notifications n = Notifications.create()
                .title("Información")
                .text(message)
                .graphic(null)
                .hideAfter(javafx.util.Duration.seconds(5))
                .position(Pos.BASELINE_RIGHT);
        n.showInformation();
    }

    public void errorMessage(String message) {
        Notifications n = Notifications.create()
                .title("Error")
                .text(message)
                .graphic(null)
                .hideAfter(javafx.util.Duration.seconds(5))
                .position(Pos.BASELINE_RIGHT);
        n.showError();
    }

    public void warningMessage(String message) {
        Notifications n = Notifications.create()
                .title("Advertencia")
                .text(message)
                .graphic(null)
                .hideAfter(javafx.util.Duration.seconds(5))
                .position(Pos.BASELINE_RIGHT);
        n.showWarning();

    }
}

