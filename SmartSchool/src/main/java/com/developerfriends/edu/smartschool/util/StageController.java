package com.developerfriends.edu.smartschool.util;

import com.developerfriends.edu.smartschool.app.MainApp;
import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.net.URL;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 *
 * @author tani
 */
public class StageController {

    Dialogs dialog = new Dialogs();
    BorderPane borderPane = new BorderPane();

    public StageController() {
    }

    public Stage showStage(URL url, String Title, Stage stage, FXMLLoader load) {
        try {
            load.setLocation(url);
            AnchorPane page = (AnchorPane) load.load();
            page.setFocusTraversable(true);
            Stage dialogStage = new Stage();
            dialogStage.setTitle(Title);
            dialogStage.initOwner(stage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);
            dialog.IconStage(dialogStage);
            return dialogStage;
        } catch (IOException ex) {
            System.err.println("Error al mostrar Stage " + ex.getMessage());
            return null;
        }
    }

    public void changeStage(URL url, Node node) {
        try {
            Parent home = FXMLLoader.load(url);
            Scene scene = new Scene(home);
            Stage stage = (Stage) (node).getScene().getWindow();
            stage.setScene(scene);
            stage.centerOnScreen();
            stage.show();
        } catch (IOException ex) {
            System.err.println(ex.getMessage() + " Ocurrio en el cambio de stage ");
        }
    }

    public void MountScene(ActionEvent event, String url, String title) {
        try {
            BorderPane root = FXMLLoader.load(MainApp.class.getResource("/fxml/PrincipalFXML.fxml"));
            root.getChildren().clear();
            AnchorPane main = FXMLLoader.load(MainApp.class.getResource(url));
            root.setLeft(main);
            Scene scene = new Scene(root);
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.setTitle(title);
            dialog.IconStage(stage);
            stage.show();
        } catch (IOException ex) {
            System.err.println("Error al mostrar Scene " + ex.getMessage());
        }
    }

    public void clear(ActionEvent event) {
        try {
            BorderPane root = FXMLLoader.load(MainApp.class.getResource("/fxml/PrincipalFXML.fxml"));
            Scene scene = new Scene(root);
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            stage.setScene(scene);
            dialog.IconStage(stage);
            stage.show();
        } catch (IOException ex) {
            System.err.println("Error al mostrar Scene " + ex.getMessage());
        }
    }

    public void Icon(JFXButton button, String icon) {
        button.setGraphic(setImageView(icon));
    }

    public void Icon2(JFXButton button, String icon) {
        button.setGraphic(setImageView(icon));
        button.setContentDisplay(ContentDisplay.TOP);
    }

    public void IconToggle(ToggleButton button, String icon) {
        button.setGraphic(setImageView(icon));
    }

    private ImageView setImageView(String icon) {
        Image image = new Image(getClass().getResource("/images/" + icon + ".png").toExternalForm());
        ImageView imgView = new ImageView(image);
        return imgView;
    }

}
