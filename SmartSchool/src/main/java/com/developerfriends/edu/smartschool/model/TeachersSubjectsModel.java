/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.model;

import com.developerfriends.edu.smartschool.pojo.Employees;
import com.developerfriends.edu.smartschool.pojo.Persons;
import com.developerfriends.edu.smartschool.pojo.Subjects;
import com.developerfriends.edu.smartschool.pojo.TeachersSubjects;
import com.developerfriends.edu.smartschool.util.Dialogs;
import com.developerfriends.edu.smartschool.util.UtilClass;
import java.util.Iterator;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Tanieska
 */
public class TeachersSubjectsModel implements EntitiesInterface<TeachersSubjects> {

    Dialogs dialog = new Dialogs();

    UtilClass util = new UtilClass();

    private Session session;

    @Override
    public void save(TeachersSubjects object) {
        Transaction tx = null;
        try {
            tx = getSession().beginTransaction();
            getSession().save(object);

            tx.commit();
            dialog.Alerta("Completo", "", "Se guardo correctamente el registro", Alert.AlertType.INFORMATION);

        } catch (RuntimeException e) {
            if (tx != null) {
                tx.rollback();
            }
            dialog.Alerta("Error", "", "Sucedio un error al guardar", Alert.AlertType.ERROR);
            throw e;
        } finally {
            closeSession();
        }
    }

    public ObservableList<TeachersSubjects> readTeacherSubject(Subjects subjects) {
        assert getSession() != null;
        List results = getSession().createCriteria(TeachersSubjects.class, "ts")
                .createAlias("subjects", "s")
                .createAlias("employees", "e")
                .createAlias("e.persons", "p")
                .setProjection(Projections.projectionList()
                        .add(Projections.property("ts.teacherSubjectId")) // 0
                        .add(Projections.property("e.employeeId")) //1
                        .add(Projections.property("p.firstName")) //2
                        .add(Projections.property("p.secondName")) //3
                        .add(Projections.property("p.firstLastname")) //4
                        .add(Projections.property("p.secondLastname")) //5
                        .add(Projections.property("s.subjectId")) //6
                )
                .add(Restrictions.eq("state", Boolean.TRUE))
                .add(Restrictions.eq("ts.subjects", subjects))
                .list();
        return setTeacherClassrooms(results);
    }

    public ObservableList<TeachersSubjects> findTeacherSubject(Subjects subjects, String text) {
        assert getSession() != null;
        List results = getSession().createCriteria(TeachersSubjects.class, "ts")
                .createAlias("subjects", "s")
                .createAlias("employees", "e")
                .createAlias("e.persons", "p")
                .setProjection(Projections.projectionList()
                        .add(Projections.property("ts.teacherSubjectId")) // 0
                        .add(Projections.property("e.employeeId")) //1
                        .add(Projections.property("p.firstName")) //2
                        .add(Projections.property("p.secondName")) //3
                        .add(Projections.property("p.firstLastname")) //4
                        .add(Projections.property("p.secondLastname")) //5
                        .add(Projections.property("s.subjectId")) //6
                )
                .add(Restrictions.eq("state", Boolean.TRUE))
                .add(Restrictions.or(
                        Restrictions.like("p.firstName", text + "%"),
                        Restrictions.like("p.secondName", text + "%"),
                        Restrictions.like("p.firstLastname", text + "%"),
                        Restrictions.like("p.secondLastname", text + "%")
                ))
                .add(Restrictions.eq("ts.subjects", subjects))
                .list();
        return setTeacherClassrooms(results);
    }

    private ObservableList<TeachersSubjects> setTeacherClassrooms(List results) throws NumberFormatException {
        ObservableList<TeachersSubjects> classrooms = FXCollections.observableArrayList();
        try {

            Iterator i = results.iterator();

            while (i.hasNext()) {
                Object[] obj = (Object[]) i.next();

                Persons person = new Persons(String.valueOf(obj[2]), String.valueOf(obj[3]),
                        String.valueOf(obj[4]), String.valueOf(obj[5]));

                Employees employee = new Employees(Integer.parseInt(String.valueOf(obj[1])), person);

                Subjects subjects = new Subjects(Integer.parseInt(String.valueOf(obj[6])));

                classrooms.add(
                        new TeachersSubjects(
                                (Integer.parseInt(String.valueOf(obj[0]))),
                                employee,
                                subjects)
                );

            }
        } catch (java.lang.ClassCastException ex) {
            System.err.println("Error en el setTeachersSubjects " + ex.getMessage());
        }

        return classrooms;
    }

    public ObservableList<Employees> readTeacherWithoutSubject(int subjectId) {
        assert getSession() != null;
        String sql = "select "
                + "e.employeeId, " //0
                + "p.firstName, " //1 
                + "p.secondName, " //2 
                + "p.firstLastname, " //3
                + "p.secondLastname " //4
                + "from "
                + "persons p "
                + "inner join employees e "
                + "on e.personId=p.personId "
                + "where e.employeeId "
                + "not in "
                + "(select ts.employeeId from "
                + "teachers_subjects ts "
                + "inner join employees e "
                + "on e.employeeId = ts.employeeId "
                + "where ts.state=1 "
                + "and ts.subjectId="
                + subjectId
                + " )"
                + "and e.role = 6 "
                + "and p.state = 1";

        SQLQuery query = getSession().createSQLQuery(sql);
        List<Object[]> results = query.list();

        return setTeacher(results);
    }

    public ObservableList<Employees> findTeacherWithoutSubject(int subjectId, String text) {
        assert getSession() != null;

        String sql = "select "
                + "e.employeeId, " //0
                + "p.firstName, " //1
                + "p.secondName, " //2 
                + "p.firstLastname, " //3 
                + "p.secondLastname " //4
                + "from "
                + "persons p "
                + "inner join employees e "
                + "on e.personId=p.personId "
                + "where e.employeeId "
                + "not in "
                + "(select ts.employeeId from "
                + "teachers_subjects ts  "
                + "inner join employees e "
                + "on e.employeeId = ts.employeeId "
                + "where ts.state=1 "
                + "and ts.subjectId="
                + subjectId
                + " ) "
                + "and e.role = 6 "
                + "and p.state = 1 "
                + "and p.firstName like '"
                + text + "%' "
                + "or p.secondName like '"
                + text + "%' "
                + "or p.firstLastname like '"
                + text + "%' "
                + "or p.secondLastname like '"
                + text + "%'";
        SQLQuery query = getSession().createSQLQuery(sql);
        List<Object[]> results = query.list();

        return setTeacher(results);
    }

    private ObservableList<Employees> setTeacher(List results) throws NumberFormatException {
        ObservableList<Employees> employee = FXCollections.observableArrayList();
        try {

            Iterator i = results.iterator();

            while (i.hasNext()) {
                Object[] obj = (Object[]) i.next();

                Persons person = new Persons(
                        String.valueOf(obj[1]),
                        String.valueOf(obj[2]),
                        String.valueOf(obj[3]),
                        String.valueOf(obj[4]));

                employee.add(new Employees(
                        Integer.parseInt(String.valueOf(obj[0])),
                        person)
                );

            }
        } catch (java.lang.ClassCastException ex) {
            System.err.println("Error en el setTeacher " + ex.getMessage());
        }

        return employee;
    }

    @Override
    public void update(TeachersSubjects object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(TeachersSubjects object) {
        Transaction tx = null;
        try {
            tx = getSession().beginTransaction();
            getSession().update(object);

            tx.commit();
            dialog.Alerta("Completo", "", "Se elimo el registro", Alert.AlertType.INFORMATION);

        } catch (RuntimeException e) {
            if (tx != null) {
                tx.rollback();
            }
            dialog.Alerta("Error", "", "Sucedio un error al eliminar", Alert.AlertType.ERROR);
            throw e;
        } finally {
            closeSession();
        }
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public void closeSession() {
        if ((session != null) && (session.isOpen())) {
            session.close();
        }
    }
}
