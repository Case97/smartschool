/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.Event;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.RadioButton;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.imageio.ImageIO;

/**
 *
 * @author Enmnauel Calero
 */
public class UtilClass {

    public UtilClass() {
    }

    Dialogs dialog = new Dialogs();

    @SuppressWarnings("empty-statement")
    public Image openFile(Event event) {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();;
        Image image = null;
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Buscar Imagen");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png"),
                new FileChooser.ExtensionFilter("All Images", "*.*")
        );
        dialog.IconStage(stage);
        File imgFile = fileChooser.showOpenDialog(stage.getScene().getWindow());
        if (imgFile != null) {
            image = new Image("file:" + imgFile.getAbsolutePath());
        }
        return image;
    }

    public byte[] saveImage(BufferedImage bufImage) {
        try {
            try (
                    ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
                ImageIO.write(bufImage, "jpg", baos);
                baos.flush();
                return baos.toByteArray();
            }
        } catch (IOException ex) {
            Logger.getLogger(UtilClass.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }

    public Image getImage(byte[] imageData) {
        ByteArrayInputStream bais = new ByteArrayInputStream(imageData);
        try {
            BufferedImage bufImage = ImageIO.read(bais);
            return SwingFXUtils.toFXImage(bufImage, null);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public Date getBirthday(String identityCard) {
        String day = identityCard.substring(4, 6);
        String month = identityCard.substring(6, 8);
        String year = identityCard.substring(8, 10);

        Date birthday = new Date(Integer.parseInt(year),
                Integer.parseInt(month)-1, Integer.parseInt(day));
       
        return birthday;
    }

    public String gender(boolean type) {
        return (type) ? "Masculino" : "Femenino";
    }

    public int age(Date date) {
        LocalDate dateBorn = new java.sql.Date(date.getTime()).toLocalDate();
        LocalDate now = LocalDate.now();
        
        Period period = Period.between(dateBorn, now);

        return period.getYears();
    }

    /*role='Cajero' or role='Seguridad' or role='Afanador'role='Srío. Academcio' or 
    role='Director' or role='Sub-Director' or role='Profesor' or role='Supervisor',*/
    public List setRoles() {
        List roles = new ArrayList();
        roles.add("Cajero");
        roles.add("Seguridad");
        roles.add("Afanador");
        roles.add("Srío. Academcio");
        roles.add("Director");
        roles.add("Sub-Director");
        roles.add("Profesor");
        roles.add("Supervisor");

        return roles;
    }

    public String getRoles(int index) {
        return String.valueOf(setRoles().get(index));
    }

    public List<String> fillLetter() {
        List<String> letter = new ArrayList<>();
        for (char ABC = 'A'; ABC <= 'J'; ABC++) {
            letter.add(String.valueOf(ABC));
        }
        return letter;
    }

    public int setLetter(String letter) {
        int count = 0;
        int number = 0;
        for (char ABC = 'A'; ABC <= 'J'; ABC++) {
            count++;
            if (String.valueOf(ABC).equals(letter)) {
                number = count;
            }
        }
        return number;
    }

    public String getLetter(int index) throws IndexOutOfBoundsException {
        return String.valueOf(fillLetter().get(index - 1));
    }

    public String getGrade(int grade) {
        String value = "";
        switch (grade) {
            case 1:
            case 3:
                value = grade + " ero";
                break;
            case 2:
                value = grade + " do";
                break;
            case 4:
            case 5:
            case 6:
                value = grade + " to";
                break;
            case 7:
            case 10:
            case 11:
                value = grade + " mo";
                break;
            case 8:
                value = grade + " vo";
                break;
            case 9:
                value = grade + " no";
                break;
            default:
                break;
        }
        return value;
    }

    public List setTurn() {
        List turns = new ArrayList();
        turns.add("Matutino");
        turns.add("Vespertino");

        return turns;
    }

    public String getTurn(int index) {
        return String.valueOf(setTurn().get(index));
    }

    public void enableRB(RadioButton thisRB, RadioButton oldRB) {
        if (oldRB.isSelected()) {
            oldRB.setSelected(false);
            thisRB.setSelected(true);
        }
    }

    public List<String> setLetterAvaible(List<String> exist) {
        List<String> letter = new ArrayList<>();
        for (char ABC = 'A'; ABC <= 'J'; ABC++) {
            if (!exist.isEmpty()) {
                if (!exist.contains(String.valueOf(ABC))) {
                    letter.add(String.valueOf(ABC));
                }
            } else {
                letter.add(String.valueOf(ABC));
            }

        }
        return letter;
    }

    public void enableCB(CheckBox thisRB, CheckBox oldRB) {
        if (oldRB.isSelected()) {
            oldRB.setSelected(false);
            thisRB.setSelected(true);
        }
    }

    public List<String> fillPayment() {
        List payment = new ArrayList();
        payment.add("Pre-Matrícula");
        payment.add("Matrícula");
        payment.add("Mensualidad");

        return payment;
    }

    public List<String> setPaymentsAvaible(List<String> exist) {
        List<String> payments = new ArrayList<>();
        for (int i = 0; i < fillPayment().size(); i++) {
            if (!exist.isEmpty()) {
                if (!exist.contains(String.valueOf(fillPayment().get(i).toUpperCase()))) {
                    payments.add(String.valueOf(fillPayment().get(i)));
                }
            } else {
                payments.add(String.valueOf(fillPayment().get(i)));
            }
        }
        return payments;
    }

    public Date getDate() {
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }

    public Date getYear() {
        Calendar cal = Calendar.getInstance();
        cal.get(Calendar.YEAR);
        return cal.getTime();
    }
}
