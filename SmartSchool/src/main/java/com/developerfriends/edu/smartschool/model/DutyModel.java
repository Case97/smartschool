/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.model;

import com.developerfriends.edu.smartschool.pojo.Duty;
import com.developerfriends.edu.smartschool.util.Dialogs;
import com.developerfriends.edu.smartschool.util.UtilClass;
import java.util.Iterator;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Tanieska
 */
public class DutyModel implements EntitiesInterface<Duty> {

    Dialogs dialog = new Dialogs();

    UtilClass util = new UtilClass();

    private Session session;

    @Override
    public void save(Duty object) {
        Transaction tx = null;
        try {
            tx = getSession().beginTransaction();
            getSession().save(object);

            tx.commit();
            dialog.Alerta("Smart School", "Completo", "Se guardo correctamente el registro", Alert.AlertType.INFORMATION);

        } catch (RuntimeException e) {
            if (tx != null) {
                tx.rollback();
            }
            dialog.Alerta("Smart School", "Error", "Sucedio un error al guardar", Alert.AlertType.ERROR);
            throw e;
        } finally {
            closeSession();
        }
    }

    public ObservableList<Duty> readDuty(Boolean state) {
        assert getSession() != null;
        List results = getSession().createCriteria(Duty.class)
                .setProjection(Projections.projectionList()
                        .add(Projections.property("tariffId")) // 0
                        .add(Projections.property("nameTariff"), "name") //1
                        .add(Projections.property("price")) //2
                )
                .add(Restrictions.eq("state", state))
                .addOrder(Order.desc("name"))
                .list();
        return setDuty(results);
    }

    public ObservableList<Duty> findDuty(String text, Boolean state) {
        assert getSession() != null;
        List results = getSession().createCriteria(Duty.class)
                .setProjection(Projections.projectionList()
                        .add(Projections.property("tariffId")) // 0
                        .add(Projections.property("nameTariff"), "name") //1
                        .add(Projections.property("price")) //2
                )
                .add(Restrictions.eq("state", state))
                .add(Restrictions.like("nameTariff", text + "%"))
                .addOrder(Order.desc("name"))
                .list();
        return setDuty(results);
    }

    private ObservableList<Duty> setDuty(List results) throws NumberFormatException {
        ObservableList<Duty> duty = FXCollections.observableArrayList();
        try {

            Iterator i = results.iterator();

            while (i.hasNext()) {
                Object[] obj = (Object[]) i.next();

                duty.add(
                        new Duty((Integer.parseInt(String.valueOf(obj[0]))),
                                String.valueOf(obj[1]),
                                (Float.parseFloat(String.valueOf(obj[2])))
                        )
                );

            }
        } catch (java.lang.ClassCastException ex) {
            System.err.println("Error en el setDuty " + ex.getMessage());
        }

        return duty;
    }

    public Duty getDuty(int opc) {
        assert getSession() != null;
        Duty duty = null;
        List results = getSession().createCriteria(Duty.class)
                .setProjection(Projections.projectionList()
                        .add(Projections.property("tariffId")) // 0
                        .add(Projections.property("nameTariff"), "name") //1
                        .add(Projections.property("price")) //2
                )
                .add(Restrictions.eq("nameTariff", util.fillPayment().get(opc)))
                .add(Restrictions.eq("state", Boolean.TRUE))
                .list();

        Iterator i = results.iterator();

        while (i.hasNext()) {
            Object[] obj = (Object[]) i.next();

            duty = new Duty((Integer.parseInt(String.valueOf(obj[0]))),
                    String.valueOf(obj[1]),
                    (Float.parseFloat(String.valueOf(obj[2]))));
        }
        return duty;
    }

    @Override
    public void update(Duty object) {
        Transaction tx = null;
        try {
            tx = getSession().beginTransaction();
            getSession().update(object);

            tx.commit();
            dialog.Alerta("Completo", "", "Se actualizo el registro", Alert.AlertType.INFORMATION);

        } catch (RuntimeException e) {
            if (tx != null) {
                tx.rollback();
            }
            dialog.Alerta("Error", "", "Sucedio un error al actualizar", Alert.AlertType.ERROR);
            throw e;
        } finally {
            closeSession();
        }
    }

    @Override
    public void delete(Duty object) {
        Transaction tx = null;
        try {
            tx = getSession().beginTransaction();
            getSession().update(object);

            tx.commit();
            dialog.Alerta("Completo", "", "Se deshabilito ese arancel", Alert.AlertType.INFORMATION);

        } catch (RuntimeException e) {
            if (tx != null) {
                tx.rollback();
            }
            dialog.Alerta("Error", "", "Sucedio un error al eliminar", Alert.AlertType.ERROR);
            throw e;
        } finally {
            closeSession();
        }
    }

    public void active(Duty object) {
        Transaction tx = null;
        try {
            tx = getSession().beginTransaction();
            getSession().update(object);

            tx.commit();
            dialog.Alerta("Smart School", "Completo", "Se activo el registro", Alert.AlertType.INFORMATION);

        } catch (RuntimeException e) {
            if (tx != null) {
                tx.rollback();
            }
            dialog.Alerta("Smart School", "Error", "Sucedio un error al activar", Alert.AlertType.ERROR);
            throw e;
        } finally {
            closeSession();
        }
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public void closeSession() {
        if ((session != null) && (session.isOpen())) {
            session.close();
        }
    }

}
