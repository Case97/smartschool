/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.controller;

import com.developerfriends.edu.smartschool.model.UserModel;
import com.developerfriends.edu.smartschool.pojo.Employees;
import com.developerfriends.edu.smartschool.pojo.UserEmployee;
import com.developerfriends.edu.smartschool.pojo.Users;
import com.developerfriends.edu.smartschool.util.Dialogs;
import com.developerfriends.edu.smartschool.util.Hash;
import com.developerfriends.edu.smartschool.util.HibernateUtil;
import com.developerfriends.edu.smartschool.util.ImageClass;
import com.developerfriends.edu.smartschool.util.StageController;
import com.developerfriends.edu.smartschool.util.UtilClass;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 * FXML Controller class
 *
 * @author Enmnauel Calero
 */
public class UserConfigurationFXMLController implements Initializable {

    @FXML
    private ImageView imgViewEmployee;
    @FXML
    private Label employeeName;
    @FXML
    private Label positionEmployee;
    @FXML
    private Label emailEmployee;
    @FXML
    private TextField txtUserName;
    @FXML
    private PasswordField txtPassword;
    @FXML
    private PasswordField txtConfirmPassword;
    @FXML
    private TextField toShowPassword;
    @FXML
    private TextField toShowConfirmPassword;
    @FXML
    private ToggleButton btnShowPassword;
    @FXML
    private ToggleButton btnShowConfirmPassword;
    @FXML
    private JFXButton btnResetPassword;
    @FXML
    private JFXButton btnSave;
    @FXML
    private JFXButton btnCancel;

    StageController stageController = new StageController();
    private Employees employee = new Employees();
    private UserEmployee userEmployee = new UserEmployee();
    UtilClass util = new UtilClass();
    Hash hash = new Hash();
    Dialogs dialogs = new Dialogs();
    Users userToSave = new Users();
    UserModel userModel = new UserModel();
    private SessionFactory sFactory;
    private boolean isNew;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        getSessionFactory();
        icon();
        // TODO
    }

    public void icon() {
        stageController.IconToggle(btnShowPassword, ImageClass.lock);
        stageController.IconToggle(btnShowConfirmPassword, ImageClass.lock);
        stageController.Icon(btnSave, ImageClass.save);
        stageController.Icon(btnCancel, ImageClass.cancel);
        stageController.Icon(btnResetPassword, ImageClass.refresh);
    }

    public void setEmployee(Employees e) {
        isNew = true;
        this.employee = e;
        employeeName.setText(employee.getPersons().getPersonName());
        emailEmployee.setText(employee.getEmail());
        positionEmployee.setText(util.getRoles(employee.getRole()));
        imgViewEmployee.setImage(util.getImage(employee.getPersons().getPhoto()));
    }

    public void setUser(UserEmployee ue) {
        isNew = false;
        this.userEmployee = ue;
        employeeName.setText(userEmployee.getName());
        emailEmployee.setText(userEmployee.getEmail());
        positionEmployee.setText(util.getRoles(userEmployee.getRole()));
        imgViewEmployee.setImage(util.getImage(userEmployee.getPhoto()));
        txtUserName.setText(userEmployee.getUserName());
        txtUserName.setDisable(true);
    }

    @FXML
    private void showPass() {
        if (btnShowPassword.isSelected()) {
            this.toShowPassword.setText(this.txtPassword.getText());
            this.txtPassword.setVisible(false);
            this.toShowPassword.setVisible(true);
        } else {
            this.txtPassword.setText(this.toShowPassword.getText());
            this.toShowPassword.setVisible(false);
            this.txtPassword.setVisible(true);
        }
    }

    @FXML
    private void showConfirmPass() {
        if (btnShowConfirmPassword.isSelected()) {
            this.toShowConfirmPassword.setText(this.txtConfirmPassword.getText());
            this.txtConfirmPassword.setVisible(false);
            this.toShowConfirmPassword.setVisible(true);
        } else {
            this.txtConfirmPassword.setText(this.toShowConfirmPassword.getText());
            this.toShowConfirmPassword.setVisible(false);
            this.txtConfirmPassword.setVisible(true);
        }
    }

    @FXML
    private void addUser(ActionEvent ae) {
        if (isNew) {
            try {
                if (isEmpty()) {
                    dialogs.Alerta("SmartSchool", "Error", "No puede dejar campos vacios..!!", Alert.AlertType.ERROR);
                } else {
                    if (this.txtPassword.getText().equals(this.txtConfirmPassword.getText())) {
                        userToSave.setPersons(this.employee.getPersons());
                        userToSave.setUserU(txtUserName.getText());
                        userToSave.setPasswordU(hash.Sha256Hash(this.txtPassword.getText()));
                        userToSave.setState(Boolean.TRUE);
                        userModel.setSession(openSession());
                        userModel.save(userToSave);
                        Node source = (Node) ae.getSource();
                        Stage theStage = (Stage) source.getScene().getWindow();
                        theStage.close();
                    } else {
                        dialogs.Alerta("SmartSchool", "Error", "las contraseñas no coinciden. ¿Quieres volver a intentarlo?", Alert.AlertType.ERROR);
                    }

                }
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(UserConfigurationFXMLController.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            try {
                if (isEmpty()) {
                    dialogs.Alerta("SmartSchool", "Error", "No puede dejar campos vacios..!!", Alert.AlertType.ERROR);
                } else {
                    if (this.txtPassword.getText().equals(this.txtConfirmPassword.getText())) {
                        userModel.setSession(openSession());
                        Users userToUpdate = new Users(userEmployee.getIdUsuario(),
                                userModel.getPersonByUserId(userEmployee.getIdUsuario()),
                                userEmployee.getUserName(),
                                hash.Sha256Hash(this.txtPassword.getText()),
                                Boolean.TRUE);
                        userModel.update(userToUpdate);
                        Node source = (Node) ae.getSource();
                        Stage theStage = (Stage) source.getScene().getWindow();
                        theStage.close();
                    } else {
                        dialogs.Alerta("SmartSchool", "Error", "las contraseñas no coinciden. ¿Quieres volver a intentarlo?", Alert.AlertType.ERROR);
                    }

                }
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(UserConfigurationFXMLController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @FXML
    private void close(ActionEvent ae) {
        Node source = (Node) ae.getSource();
        Stage theStage = (Stage) source.getScene().getWindow();
        theStage.close();
    }

    private Session openSession() {
        return sFactory.openSession();
    }

    private void getSessionFactory() {
        sFactory = HibernateUtil.getSessionFactory();
    }

    private boolean isEmpty() {
        return (this.txtUserName.getText().isEmpty()
                || this.txtPassword.getText().isEmpty()
                || this.txtConfirmPassword.getText().isEmpty());
    }

    @FXML
    private void resetPassword() {
        if (dialogs.Confirm("SmartSchool", "Confirmación", "¿Desea restablecer la contraseña? Esta sera sustitudiada por: **smartschool**")) {
            this.txtPassword.setText("smartschool");
            this.txtConfirmPassword.setText("smartschool");
        }
    }
}
