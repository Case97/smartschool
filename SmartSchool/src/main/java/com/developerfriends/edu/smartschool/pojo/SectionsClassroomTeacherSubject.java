/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.pojo;

/**
 *
 * @author Tanieska
 */
public class SectionsClassroomTeacherSubject {

    private String subject;
    private Persons persons;
    private String classrooms;
    private int grade;
    private int sections;
    private int turn;

    public SectionsClassroomTeacherSubject(String subject, Persons persons, String classrooms, int grade, int sections, int turn) {
        this.subject = subject;
        this.persons = persons;
        this.classrooms = classrooms;
        this.grade = grade;
        this.sections = sections;
        this.turn = turn;
    }
    
    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Persons getPersons() {
        return persons;
    }

    public void setPersons(Persons persons) {
        this.persons = persons;
    }

    public String getClassrooms() {
        return classrooms;
    }

    public void setClassrooms(String classrooms) {
        this.classrooms = classrooms;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public int getSections() {
        return sections;
    }

    public void setSections(int sections) {
        this.sections = sections;
    }

    public int getTurn() {
        return turn;
    }

    public void setTurn(int turn) {
        this.turn = turn;
    }

}
