/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.util;

/**
 *
 * @author Enmnauel Calero
 */
public class StringMessage {

    public static String saveObject = "Registro Guardado Exitosamente";
    public static String updateObject = "Registro Actualizado Exitosamente";
    public static String deleteObject = "Registro Eliminado Exitosamente";
    public static String dismissEmployee = "Se despidio correctamente al empleado";
}
