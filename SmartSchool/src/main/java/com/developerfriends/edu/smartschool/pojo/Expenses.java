package com.developerfriends.edu.smartschool.pojo;
// Generated 27-jun-2017 19:15:47 by Hibernate Tools 4.3.1



/**
 * Expenses generated by hbm2java
 */
public class Expenses  implements java.io.Serializable {


     private Integer expenseId;
     private Payments payments;
     private Float cost;

    public Expenses() {
    }

    public Expenses(Payments payments, Float cost) {
       this.payments = payments;
       this.cost = cost;
    }
   
    public Integer getExpenseId() {
        return this.expenseId;
    }
    
    public void setExpenseId(Integer expenseId) {
        this.expenseId = expenseId;
    }
    public Payments getPayments() {
        return this.payments;
    }
    
    public void setPayments(Payments payments) {
        this.payments = payments;
    }
    public Float getCost() {
        return this.cost;
    }
    
    public void setCost(Float cost) {
        this.cost = cost;
    }




}


