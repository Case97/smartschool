/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.model;

import com.developerfriends.edu.smartschool.pojo.Persons;
import com.developerfriends.edu.smartschool.pojo.UserEmployee;
import com.developerfriends.edu.smartschool.pojo.Users;
import com.developerfriends.edu.smartschool.util.Dialogs;
import com.developerfriends.edu.smartschool.util.Hash;
import com.developerfriends.edu.smartschool.util.UtilClass;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Enmnauel Calero
 */
public class UserModel implements EntitiesInterface<Users> {

    private Session session;
    public static Users currentUser = new Users();
    Dialogs dialog = new Dialogs();
    UtilClass util = new UtilClass();
    Hash hash = new Hash();
    Users user = new Users();
    Persons person = new Persons();
    List<Users> usersList = new ArrayList<>();

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public static Users getCurrentUser() {
        return currentUser;
    }

    public static void setCurrentUser(Users currentUser) {
        UserModel.currentUser = currentUser;
    }

    public void closeSession() {
        if ((session != null) && (session.isOpen())) {
            session.close();
        }
    }

    @Override
    public void save(Users object) {
        Transaction transaction = null;
        try {
            transaction = getSession().beginTransaction();
            getSession().save(object);
            transaction.commit();
            dialog.Alerta("Completo", "", "Se guardo correctamente el registro", Alert.AlertType.INFORMATION);
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            dialog.Alerta("Error", "", "Sucedio un error al guardar", Alert.AlertType.ERROR);
            throw e;
        } finally {
            closeSession();
        }
    }

    @Override
    public void update(Users object) {
        Transaction transaction = null;
        try {
            transaction = getSession().beginTransaction();
            getSession().update(object);
            transaction.commit();
            dialog.Alerta("Completo", "", "Se actualizo correctamente el registro", Alert.AlertType.INFORMATION);
        } catch (RuntimeException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            dialog.Alerta("Error", "", "Sucedio un error al actualizar", Alert.AlertType.ERROR);
            throw e;
        } finally {
            closeSession();
        }
    }

    @Override
    public void delete(Users object) {
        Transaction transaction = null;
        try {
            transaction = getSession().beginTransaction();
            getSession().update(object);
            transaction.commit();
            dialog.Alerta("Completo", "", "Se eliminó correctamente el registro", Alert.AlertType.INFORMATION);
        } catch (RuntimeException e) {
            if (transaction != null) {
                transaction.rollback();
            }
            dialog.Alerta("Error", "", "Sucedio un error al eliminar", Alert.AlertType.ERROR);
            throw e;
        } finally {
            closeSession();
        }
    }

    public Users getUser(String UserName) {
        try {
            assert getSession() != null;
            user = (Users) getSession().createCriteria(Users.class).
                    add(Restrictions.eq("userU", UserName)).uniqueResult();
            setCurrentUser(user);
        } catch (HibernateException ex) {
            dialog.Alerta("Error", "Error en usuario Revisar UserModel getUser() ", ex.getMessage(), Alert.AlertType.ERROR);
        }
        return user;
    }

    public Users getUserById(int userId) {
        try {
            assert getSession() != null;
            user = (Users) getSession().createCriteria(Users.class).
                    add(Restrictions.eq("userId", userId)).uniqueResult();
        } catch (HibernateException ex) {
            dialog.Alerta("Error", "Error en usuario Revisar UserModel getUserByID() ", ex.getMessage(), Alert.AlertType.ERROR);
        }
        return user;
    }

    public String getEmailByUserId(int userId) {
        assert getSession() != null;
        String email = "";
        String sql = "select "
                + "email "
                + "from users "
                + "inner join employees "
                + "on users.personId = employees.personId "
                + "where userId = " + userId;
        SQLQuery query = getSession().createSQLQuery(sql);
        List<String> results = query.list();
        email = results.get(0);
        return email;
    }

    public Persons getPersonByUserId(int userId) {
        try {
            assert getSession() != null;
            person = (Persons) getSession().createCriteria(Users.class).
                    setProjection(Projections.projectionList()
                            .add(Projections.property("persons")))
                    .add(Restrictions.eq("userId", userId)).uniqueResult();
        } catch (HibernateException ex) {
            dialog.Alerta("Error", "Error en usuario Revisar UserModel getPersonByUserId() ", ex.getMessage(), Alert.AlertType.ERROR);
        }
        return person;
    }

    public ObservableList<UserEmployee> readUsers() {
        assert getSession() != null;
        String sql = "select "
                + "userId, "
                + "firstName, "
                + "secondName, "
                + "firstLastname, "
                + "secondLastname, "
                + "role, "
                + "email, "
                + "userU, "
                + "photo "
                + "from users "
                + "inner join persons "
                + "on users.personId = persons.personId "
                + "inner join employees "
                + "on employees.personId = users.personId ";
        SQLQuery query = getSession().createSQLQuery(sql);
        List<Object[]> results = query.list();
        return setUser(results);
    }

    public ObservableList<UserEmployee> findUsers(String text) {
        assert getSession() != null;
        String sql = "select "
                + "userId, "
                + "firstName, "
                + "secondName, "
                + "firstLastname, "
                + "secondLastname, "
                + "role, "
                + "email, "
                + "userU, "
                + "photo "
                + "from users "
                + "inner join persons "
                + "on users.personId = persons.personId "
                + "inner join employees "
                + "on employees.personId = users.personId "
                + "where firstName like '"+text+"%'";
        SQLQuery query = getSession().createSQLQuery(sql);
        List<Object[]> results = query.list();
        return setUser(results);
    }

    private ObservableList<UserEmployee> setUser(List results) throws NumberFormatException {
        ObservableList<UserEmployee> users = FXCollections.observableArrayList();
        try {
            Iterator i = results.iterator();
            while (i.hasNext()) {
                Object[] obj = (Object[]) i.next();
                UserEmployee userEmployee = new UserEmployee(
                        (Integer.parseInt(String.valueOf(obj[0]))),
                        String.valueOf(obj[1]) + " " + String.valueOf(obj[2]) + " " + String.valueOf(obj[3]) + " " + String.valueOf(obj[4]),
                        (Integer.parseInt(String.valueOf(obj[5]))),
                        String.valueOf(obj[6]),
                        String.valueOf(obj[7]),
                        (byte[]) obj[8]);
                users.add(userEmployee);
            }
        } catch (java.lang.ClassCastException ex) {
            System.err.println("Error en el setEmployee " + ex.getMessage());
        }

        return users;
    }

}
