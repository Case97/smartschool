/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.controller;

import com.developerfriends.edu.smartschool.util.ImageClass;
import com.developerfriends.edu.smartschool.util.StageController;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

/**
 * FXML Controller class
 *
 * @author Tanieska
 */
public class MonthlyPaymentFXMLController implements Initializable {

    StageController stController = new StageController();

    @FXML
    private JFXButton btnSave;
    @FXML
    private JFXButton btnCancel;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        Icons();
    }

    public void Icons() {
        stController.Icon(btnSave, ImageClass.save);
        stController.Icon(btnCancel, ImageClass.cancel);
    }

}
