/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.controller;

import com.developerfriends.edu.smartschool.util.Dialogs;
import com.developerfriends.edu.smartschool.util.ImageClass;
import com.developerfriends.edu.smartschool.util.StageController;
import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamException;
import com.github.sarxos.webcam.WebcamPanel;
import com.github.sarxos.webcam.WebcamResolution;
import com.jfoenix.controls.JFXButton;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.embed.swing.SwingFXUtils;
import javafx.embed.swing.SwingNode;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javax.swing.SwingUtilities;

/**
 * FXML Controller class
 *
 * @author Tanieska
 */
public class CameraFXMLController implements Initializable {

    private Webcam webcam = null;
    private WebcamPanel webcamPanel;
    private Stage dialogStage;
    protected static int type;

    Dialogs dialog = new Dialogs();
    StageController stController = new StageController();
    EmployeeFXMLController employee = new EmployeeFXMLController();
    EnrollmentFXMLController enrollment = new EnrollmentFXMLController();
    BufferedImage bufImage;

    @FXML
    private JFXButton btnOption;
    @FXML
    private JFXButton btnSave;
    @FXML
    private JFXButton btnCancel;
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private StackPane stackPane;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Icons();
        startCamera();
        btnSave.setDisable(true);
    }

    private void Icons() {
        stController.Icon(btnCancel, ImageClass.cancel);
        stController.Icon(btnOption, ImageClass.camera);
        stController.Icon(btnSave, ImageClass.save);
    }

    private void startCamera() {
        try {
            final SwingNode swingNode = new SwingNode();

            webcam = Webcam.getDefault();
            webcam.setViewSize(WebcamResolution.VGA.getSize());
            webcamPanel = new WebcamPanel(webcam);
            webcamPanel.setMirrored(true);
            stackPane.getChildren().add(swingNode);

            SwingUtilities.invokeLater(() -> {
                swingNode.setContent(webcamPanel);
                webcamPanel.start();
            });

        } catch (WebcamException ex) {
            System.err.println("Error " + ex.getMessage());
        }
    }

    @FXML
    private void TakeOrTry(ActionEvent event) {
        try {

            if (btnOption.getText().equals("Capturar")) {
                stController.Icon(btnOption, "1497784703_021");
                bufImage = webcam.getImage();
                Image image = SwingFXUtils.toFXImage(bufImage, null);
                webcam.close();
                webcamPanel.stop();
                stackPane.getChildren().remove(0);
                ImageView imgView = new ImageView(image);
                imgView.setFitHeight(338);
                imgView.setFitWidth(450);
                stackPane.getChildren().add(imgView);
                btnSave.setDisable(false);
                btnOption.setLayoutX(400);
                btnOption.setText("Intentelo de nuevo");
            } else {
                stController.Icon(btnOption, "1497784695_035");
                stackPane.getChildren().remove(0);
                btnSave.setDisable(true);
                btnOption.setLayoutX(400);
                btnOption.setText("Capturar");
                startCamera();
            }

        } catch (NullPointerException ex) {
            System.err.println("Error " + ex.getMessage());
        }
    }

    @FXML
    private void save(ActionEvent event) {
        switch (getType()) {
            case 1:
                employee.imgViewEmployee.setImage(SwingFXUtils.toFXImage(bufImage, null));
                employee.setNewImage(true);
                webcam.close();
                dialogStage.close();
                break;

            case 2:
                enrollment.ImageViewPerson.setImage(SwingFXUtils.toFXImage(bufImage, null));
                webcam.close();
                dialogStage.close();
                break;

            case 3:
                enrollment.imgViewTutors.setImage(SwingFXUtils.toFXImage(bufImage, null));
                webcam.close();
                dialogStage.close();
                break;
        }

    }

    @FXML
    private void cancel(ActionEvent event) {
        webcam.close();
        dialogStage.close();
    }

    public void setStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public void setEmployee(EmployeeFXMLController employee) {
        this.employee = employee;
    }

    public void setEmrollment(EnrollmentFXMLController enrollment) {
        this.enrollment = enrollment;
    }

    public static int getType() {
        return type;
    }

    public static void setType(int type) {
        CameraFXMLController.type = type;
    }

}
