/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.controller;

import com.crazycode.custom.elements.SearchField;
import com.developerfriends.edu.smartschool.model.SubjectsModel;
import com.developerfriends.edu.smartschool.model.TeachersSubjectsModel;
import com.developerfriends.edu.smartschool.pojo.Subjects;
import com.developerfriends.edu.smartschool.pojo.TeachersSubjects;
import com.developerfriends.edu.smartschool.util.Dialogs;
import com.developerfriends.edu.smartschool.util.HibernateUtil;
import com.developerfriends.edu.smartschool.util.ImageClass;
import com.developerfriends.edu.smartschool.util.StageController;
import com.developerfriends.edu.smartschool.util.UtilClass;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXToggleButton;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 * FXML Controller class
 *
 * @author Tanieska
 */
public class SubjectFXMLController implements Initializable {

    Dialogs dialog = new Dialogs();
    PrincipalFXMLController principal = new PrincipalFXMLController();
    SubjectsModel subjectsModel = new SubjectsModel();
    TeachersSubjectsModel teachersSubjectsModel = new TeachersSubjectsModel();
    StageController stController = new StageController();
    UtilClass util = new UtilClass();

    private SessionFactory sFactory;
    private Stage primaryStage;
    private int subjectsId;
    private String subjectName;
    private final Boolean ACTIVE = Boolean.TRUE;
    private final Boolean INACTIVE = Boolean.FALSE;

    final SpinnerValueFactory.IntegerSpinnerValueFactory capacityValue
            = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 5);

    @FXML
    private TableView<Subjects> tableSubject;
    @FXML
    private TableColumn<Subjects, String> colSubject;
    @FXML
    private TableColumn<Subjects, String> colFrecuency;
    @FXML
    private TextField txtSubject;
    @FXML
    private Spinner<Integer> spFrecuency;
    @FXML
    private HBox hBBtn;
    @FXML
    private TableView<TeachersSubjects> tableTeacherSubject;
    @FXML
    private TableColumn<TeachersSubjects, String> colTeacher;
    @FXML
    private AnchorPane apElements;
    @FXML
    private JFXToggleButton tgEnable;
    @FXML
    private SearchField txtSearchSubject;
    @FXML
    private SearchField txtSearchTeacherSubject;
    @FXML
    private JFXButton btnNewSubject;
    @FXML
    private JFXButton btnUpdateSubject;
    @FXML
    private JFXButton btnDisableSubject;
    @FXML
    private JFXButton btnSave;
    @FXML
    private JFXButton btnSaveChange;
    @FXML
    private JFXButton btnCancel;
    @FXML
    private JFXButton btnAddTeacher;
    @FXML
    private JFXButton btnDeleteTeacher;
    @FXML
    private JFXButton btnBack;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        Icons();

        getSessionFactory();

        apElements.setVisible(false);
        txtSearchTeacherSubject.setDisable(true);
        spFrecuency.setValueFactory(capacityValue);

        enableButton(1, true);
        enableButton(2, true);

        colSubject.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getSubjectName()));
        colFrecuency.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(String.valueOf(cellData.getValue().getFrecuencyClass())));
        colTeacher.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getEmployees().getPersons().getPersonName()));

        searchSubject();

        loadSubject(ACTIVE);
    }

    public void Icons() {
        stController.Icon(btnNewSubject, ImageClass.newElement);
        stController.Icon(btnUpdateSubject, ImageClass.updateElement);
        stController.Icon(btnDisableSubject, ImageClass.disableElement);
        stController.Icon(btnSave, ImageClass.save);
        stController.Icon(btnSaveChange, ImageClass.saveChange);
        stController.Icon(btnCancel, ImageClass.cancel);
        stController.Icon(btnAddTeacher, ImageClass.newElement);
        stController.Icon(btnDeleteTeacher, ImageClass.disableSubElement);
        stController.Icon(btnBack, ImageClass.back);
    }

    private void loadSubject(Boolean state) {
        try {
            subjectsModel.setSession(openSession());
            tableSubject.setItems(subjectsModel.readSubjects(state));
        } catch (NullPointerException ex) {
            System.err.println("Error en loadSubject " + ex.getMessage());
        }
    }

    protected void loadTeacher(Subjects subjects) {
        try {
            teachersSubjectsModel.setSession(openSession());
            tableTeacherSubject.setItems(teachersSubjectsModel.readTeacherSubject(subjects));
        } catch (NullPointerException ex) {
            System.err.println("Error en loadTeacher " + ex.getMessage());
        }
    }

    private void findSubject(Boolean state, String text) {
        try {
            subjectsModel.setSession(openSession());
            tableSubject.setItems(subjectsModel.findSubjects(state, text));
        } catch (NullPointerException ex) {
            System.err.println("Error en loadSubject " + ex.getMessage());
        }
    }

    protected void findTeacher(Subjects subjects, String text) {
        try {
            teachersSubjectsModel.setSession(openSession());
            tableTeacherSubject.setItems(teachersSubjectsModel.findTeacherSubject(subjects, text));
        } catch (NullPointerException ex) {
            System.err.println("Error en loadTeacher " + ex.getMessage());
        }
    }

    @FXML
    private void newSubject(ActionEvent event) {
        this.apElements.setVisible(true);
        enableButton(1, true);
        enableButton(2, true);
        enableButton(3, true);
        showButton(1);
    }

    @FXML
    private void updateSubject(ActionEvent event) {
        Subjects selectSubject = (Subjects) tableSubject.getSelectionModel().getSelectedItem();
        if (selectSubject != null) {
            setSubject(selectSubject);
            this.apElements.setVisible(true);
            enableButton(1, true);
            enableButton(2, true);
            enableButton(3, true);
            showButton(2);
        } else {
            dialog.Alerta("Advertencia", "No se ha encontrado el aula", "Por favor seleccione un aula en la tabla", Alert.AlertType.WARNING);
        }
    }

    @FXML
    private void disableSubject(ActionEvent event) {
        Subjects selectSubjects = (Subjects) tableSubject.getSelectionModel().getSelectedItem();
        if (selectSubjects != null) {
            if (tgEnable.isSelected()) {
                if (dialog.Confirm("Smart School", "Deshabilitar", "¿Esta seguro que quiere deshabilitar esta materia?")) {
                    active_inactive(selectSubjects, INACTIVE, 0);
                }
            } else {
                if (dialog.Confirm("Smart School", "Habilitar", "¿Esta seguro que quiere habilitar esta materia?")) {
                    active_inactive(selectSubjects, ACTIVE, 1);
                }
            }
        } else {
            dialog.Alerta("Advertencia", "No se ha encontrado la materia", "Por favor seleccione una materia en la tabla", Alert.AlertType.WARNING);
        }
    }

    private void active_inactive(Subjects selectSubject, Boolean state, int type) {
        selectSubject.setState(state);
        subjectsModel.setSession(openSession());
        if (type == 0) {
            subjectsModel.delete(selectSubject);
        } else {
            subjectsModel.active(selectSubject);
        }
        loadSubject(!state);
        tableTeacherSubject.getItems().clear();
    }

    @FXML
    private void showSubjects(ActionEvent event) {
        String tgText = (tgEnable.isSelected()) ? "Habilitados" : "Deshabilitados";
        String btnText = (tgEnable.isSelected()) ? "Deshabilitar" : "Habilitar";
        boolean enable = !(tgEnable.isSelected());
        Boolean state = (tgEnable.isSelected()) ? ACTIVE : INACTIVE;

        tgEnable.setText(tgText);
        btnDisableSubject.setText(btnText);
        enableButton(4, true);
        enableButton(6, enable);
        loadSubject(state);
        tableTeacherSubject.getItems().clear();
    }

    private void searchSubject() {
        txtSearchSubject.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            Boolean state = (tgEnable.isSelected()) ? ACTIVE : INACTIVE;
            if (newValue.isEmpty()) {
                loadSubject(state);
            } else {
                findSubject(state, newValue);
            }
            tableTeacherSubject.getItems().clear();
            txtSearchTeacherSubject.setDisable(true);
        });
    }

    private void searchTeacherSubject(Subjects subject) {
        txtSearchTeacherSubject.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            Boolean state = (tgEnable.isSelected()) ? ACTIVE : INACTIVE;
            if (newValue.isEmpty()) {
                loadTeacher(subject);
            } else {
                findTeacher(subject, newValue);
            }
        });
    }

    @FXML
    private void enableSubjectOption(MouseEvent event) {
        if (tgEnable.isSelected()) {
            enableButton(1, false);
        } else {
            enableButton(5, false);
        }
        Subjects selectSubjects = (Subjects) tableSubject.getSelectionModel().getSelectedItem();
        if (selectSubjects != null) {
            loadTeacher(selectSubjects);
            txtSearchTeacherSubject.setDisable(false);
            searchTeacherSubject(selectSubjects);
        }
    }

    @FXML
    private void save(ActionEvent event) {
        if (isEmpty()) {
            return;
        }
        if (repeat_disable(0, 0)) {
            return;
        }
        if (repeat_disable(0, 1)) {
            return;
        }

        Subjects c = getSubject();
        subjectsModel.setSession(openSession());
        subjectsModel.save(c);
        clear();
        enableButton(3, false);
        apElements.setVisible(false);
        loadSubject(ACTIVE);
    }

    @FXML
    private void saveChange(ActionEvent event) {
        if (isEmpty()) {
            return;
        }
        if (repeat_disable(1, 0)) {
            return;
        }
        if (repeat_disable(1, 1)) {
            return;
        }

        Subjects classrooms = getSubject();
        classrooms.setSubjectId(getSubjectsId());
        subjectsModel.setSession(openSession());
        subjectsModel.update(classrooms);
        clear();
        enableButton(3, false);
        apElements.setVisible(false);
        loadSubject(ACTIVE);
    }

    @FXML
    private void cancel(ActionEvent event) {
        apElements.setVisible(false);
        enableButton(3, false);
        clear();
    }

    @FXML
    private void enableTeacherOption(MouseEvent event) {
        enableButton(2, false);
    }

    @FXML
    private void addTeacher(ActionEvent event) {
        Subjects selectSubject = (Subjects) tableSubject.getSelectionModel().getSelectedItem();
        if (selectSubject != null) {
            FXMLLoader load = new FXMLLoader();
            URL url = AddTeachersToSubjetsFXMLController.class.getResource("/fxml/AddTeachersToSubjetsFXML.fxml");
            Stage stage = stController.showStage(url, "Selección de profesor", primaryStage, load);
            AddTeachersToSubjetsFXMLController controller = load.getController();
            controller.setStage(stage);
            controller.setController(this);
            controller.setSubjects(selectSubject);
            controller.setSubjectId(selectSubject.getSubjectId());
            controller.load();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.resizableProperty().setValue(Boolean.FALSE);
            stage.show();
        } else {
            dialog.Alerta("Advertencia", "No se ha encontrado la materia", "Por favor seleccione una materia en la tabla", Alert.AlertType.WARNING);
        }
    }

    @FXML
    private void deleteteacher(ActionEvent event) {
        Subjects selectSubject = (Subjects) tableSubject.getSelectionModel().getSelectedItem();
        TeachersSubjects selectSectionClassrom = (TeachersSubjects) tableTeacherSubject.getSelectionModel().getSelectedItem();
        if (selectSectionClassrom != null) {
            selectSectionClassrom.setState(INACTIVE);
            teachersSubjectsModel.setSession(openSession());
            teachersSubjectsModel.delete(selectSectionClassrom);
            loadTeacher(selectSubject);
        } else {
            dialog.Alerta("Advertencia", "No se ha encontrado el profesor", "Por favor seleccione al profesor en la tabla", Alert.AlertType.WARNING);
        }
    }

    @FXML
    private void back(ActionEvent event) {
        principal.back(event);
    }

    private Subjects getSubject() {
        return new Subjects(this.txtSubject.getText().toUpperCase(), this.spFrecuency.getValue(), ACTIVE);
    }

    private void setSubject(Subjects subjects) {
        setSubjectsId(subjects.getSubjectId());
        setSubjectName(subjects.getSubjectName());
        this.txtSubject.setText(subjects.getSubjectName());
        this.spFrecuency.getValueFactory().setValue(subjects.getFrecuencyClass());
    }

    private boolean isEmpty() {
        if (this.txtSubject.getText().isEmpty()) {
            dialog.Alerta("Smart School", "Error", "No puede dejar campos vacios", Alert.AlertType.ERROR);
            return true;
        }
        return false;
    }

    private boolean repeat_disable(int type, int opc) {
        String subject = this.txtSubject.getText().toUpperCase();
        Boolean state = (opc == 0) ? INACTIVE : ACTIVE;
        boolean isDisable = (opc == 0);

        if (type == 0) {
            return isInsert(fillToCheck(state), subject, isDisable);
        } else {
            if (!subject.equals(getSubjectName())) {
                return isInsert(fillToCheck(state), subject, isDisable);
            }
        }
        return false;
    }

    private List<String> fillToCheck(Boolean state) {
        List<String> oldSubject = new ArrayList<>();

        for (Subjects item : subjectsModel.readSubjects(state)) {
            oldSubject.add(item.getSubjectName());
        }
        return oldSubject;
    }

    private boolean isInsert(List<String> oldSubject, String subject, boolean isDisable) {
        if (oldSubject.contains(subject)) {
            String message = (isDisable)
                    ? "Esta materia esta deshabilitada, chequee en la sección de deshabilitados"
                    : "Esta materia ya existe";
            dialog.Alerta("Smart School", "Error", message, AlertType.ERROR);
            return true;
        }
        return false;
    }

    private void clear() {
        this.txtSubject.setText("");
        this.spFrecuency.getValueFactory().setValue(1);
    }

    public int getSubjectsId() {
        return subjectsId;
    }

    public void setSubjectsId(int subjectsId) {
        this.subjectsId = subjectsId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    private Session openSession() {
        return sFactory.openSession();
    }

    private void getSessionFactory() {
        sFactory = HibernateUtil.getSessionFactory();
    }

    private void enableButton(int type, boolean opc) {
        switch (type) {
            case 1:
                this.btnUpdateSubject.setDisable(opc);
                this.btnDisableSubject.setDisable(opc);
                this.btnAddTeacher.setDisable(opc);
                break;
            case 2:
                this.btnDeleteTeacher.setDisable(opc);
                break;
            case 3:
                this.btnNewSubject.setDisable(opc);
                this.btnBack.setDisable(opc);
                this.txtSearchSubject.setDisable(opc);
                this.txtSearchTeacherSubject.setDisable(opc);
                break;
            case 4:
                this.btnUpdateSubject.setDisable(opc);
                this.btnAddTeacher.setDisable(opc);
                this.btnDisableSubject.setDisable(true);
                break;
            case 5:
                this.btnDisableSubject.setDisable(opc);
                break;
            case 6:
                this.btnNewSubject.setDisable(opc);
                break;
        }
    }

    private void showButton(int opc) {
        switch (opc) {
            //Nuevo
            case 1:
                add_remove(2, btnCancel);
                add_remove(2, btnSaveChange);
                add_remove(1, btnSave);
                add_remove(1, btnCancel);
                break;
            //Actualizar    
            case 2:
                add_remove(2, btnCancel);
                add_remove(2, btnSave);
                add_remove(1, btnSaveChange);
                add_remove(1, btnCancel);
                break;
        }
    }

    private void add_remove(int opc, Node node) {
        switch (opc) {
            //Agregar
            case 1:
                if (!this.hBBtn.getChildren().contains(node)) {
                    this.hBBtn.getChildren().add(node);
                }
                break;
            //Remover    
            case 2:
                if (this.hBBtn.getChildren().contains(node)) {
                    this.hBBtn.getChildren().remove(node);
                }
                break;
        }
    }
}
