
package com.developerfriends.edu.smartschool.model;

/**
 *
 * @author Enmnauel Calero
 * @param <T>
 */
public interface EntitiesInterface <T> {
    void save(T object);   
    void update(T object);
    void delete(T object);
}
