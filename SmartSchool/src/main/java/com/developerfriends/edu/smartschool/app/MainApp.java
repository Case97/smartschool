package com.developerfriends.edu.smartschool.app;

import com.developerfriends.edu.smartschool.controller.SplashFXMLController;
import com.developerfriends.edu.smartschool.util.StageController;
import java.net.URL;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class MainApp extends Application {

    StageController stController = new StageController();    

    private Stage primaryStage;

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader load = new FXMLLoader();
        URL url = SplashFXMLController.class.getResource("/fxml/SplashFXML.fxml");
        stage = stController.showStage(url, "", primaryStage, load);
        SplashFXMLController controller = load.getController();
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initStyle(StageStyle.TRANSPARENT);
        controller.start(stage);
    }

    
//    @Override
//    public void start(Stage stage) throws Exception {
//       Parent root = FXMLLoader.load(getClass().getResource("/fxml/UserMenuFXML.fxml"));
//
//        Scene scene = new Scene(root);
//
//        stage.setTitle("Camera");
//        stage.setScene(scene);
//        stage.initStyle(StageStyle.UTILITY);
//        root.requestFocus();
//        stage.show();
//    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    /* #d3d45d, #4dc6d6, estos si celeste;#0096c9, verde:#b3bf2d*/
}
