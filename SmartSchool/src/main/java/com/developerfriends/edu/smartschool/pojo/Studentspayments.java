package com.developerfriends.edu.smartschool.pojo;
// Generated 27-jun-2017 19:15:47 by Hibernate Tools 4.3.1



/**
 * Studentspayments generated by hbm2java
 */
public class Studentspayments  implements java.io.Serializable {


     private Integer studentPaymentId;
     private Payments payments;
     private Students students;
     private Float cost;
     private Float surcharge;

    public Studentspayments() {
    }

    public Studentspayments(Payments payments, Students students, Float cost, Float surcharge) {
       this.payments = payments;
       this.students = students;
       this.cost = cost;
       this.surcharge = surcharge;
    }
   
    public Integer getStudentPaymentId() {
        return this.studentPaymentId;
    }
    
    public void setStudentPaymentId(Integer studentPaymentId) {
        this.studentPaymentId = studentPaymentId;
    }
    public Payments getPayments() {
        return this.payments;
    }
    
    public void setPayments(Payments payments) {
        this.payments = payments;
    }
    public Students getStudents() {
        return this.students;
    }
    
    public void setStudents(Students students) {
        this.students = students;
    }
    public Float getCost() {
        return this.cost;
    }
    
    public void setCost(Float cost) {
        this.cost = cost;
    }
    public Float getSurcharge() {
        return this.surcharge;
    }
    
    public void setSurcharge(Float surcharge) {
        this.surcharge = surcharge;
    }




}


