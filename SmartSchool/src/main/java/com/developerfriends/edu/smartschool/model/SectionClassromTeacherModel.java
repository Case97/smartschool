/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.model;

import com.developerfriends.edu.smartschool.pojo.Classrooms;
import com.developerfriends.edu.smartschool.pojo.Employees;
import com.developerfriends.edu.smartschool.pojo.Grades;
import com.developerfriends.edu.smartschool.pojo.Persons;
import com.developerfriends.edu.smartschool.pojo.Sections;
import com.developerfriends.edu.smartschool.pojo.SectionsClassroomTeacherSubject;
import com.developerfriends.edu.smartschool.pojo.SectionsClassrooms;
import com.developerfriends.edu.smartschool.pojo.SectionsclassroomsTeachers;
import com.developerfriends.edu.smartschool.pojo.Subjects;
import com.developerfriends.edu.smartschool.pojo.TeachersSubjects;
import com.developerfriends.edu.smartschool.util.Dialogs;
import com.developerfriends.edu.smartschool.util.UtilClass;
import java.util.Iterator;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Tanieska
 */
public class SectionClassromTeacherModel implements EntitiesInterface<SectionsclassroomsTeachers> {

    Dialogs dialog = new Dialogs();

    UtilClass util = new UtilClass();

    private Session session;

    @Override
    public void save(SectionsclassroomsTeachers object) {
        Transaction tx = null;
        try {
            tx = getSession().beginTransaction();
            getSession().save(object);

            tx.commit();
            dialog.Alerta("Completo", "", "Se guardo correctamente el registro", Alert.AlertType.INFORMATION);

        } catch (RuntimeException e) {
            if (tx != null) {
                tx.rollback();
            }
            dialog.Alerta("Error", "", "Sucedio un error al guardar", Alert.AlertType.ERROR);
            throw e;
        } finally {
            closeSession();
        }
    }

    public ObservableList<SectionsclassroomsTeachers> readSectionsClassroomTeacherSubject() {
        assert getSession() != null;

        List results = getSession().createCriteria(SectionsclassroomsTeachers.class, "sct")
                .createAlias("sct.sectionsClassrooms", "sc")
                .createAlias("sct.teachersSubjects", "ts")
                .createAlias("sc.classrooms", "c")
                .createAlias("sc.sections", "sn")
                .createAlias("ts.employees", "e")
                .createAlias("e.persons", "p")
                .createAlias("sn.grades", "g")
                .createAlias("ts.subjects", "s")
                .setProjection(Projections.projectionList()
                        .add(Projections.property("s.subjectName")) //0                    
                        .add(Projections.property("p.firstName")) //1
                        .add(Projections.property("p.secondName")) //2
                        .add(Projections.property("p.firstLastname")) //3
                        .add(Projections.property("p.secondLastname")) //4
                        .add(Projections.property("c.codeClassroom")) // 5
                        .add(Projections.property("g.gradeName")) //6
                        .add(Projections.property("sn.letter")) //7
                        .add(Projections.property("sc.turn")) //8
                )
                .list();

        return setSectionsClassroomTeacherSubject(results);
    }

    public ObservableList<SectionsclassroomsTeachers> findSectionsClassroomTeacherSubject(String text) {
        assert getSession() != null;

        List results = getSession().createCriteria(SectionsclassroomsTeachers.class, "sct")
                .createAlias("sct.sectionsClassrooms", "sc")
                .createAlias("sct.teachersSubjects", "ts")
                .createAlias("sc.classrooms", "c")
                .createAlias("sc.sections", "sn")
                .createAlias("ts.employees", "e")
                .createAlias("e.persons", "p")
                .createAlias("sn.grades", "g")
                .createAlias("ts.subjects", "s")
                .setProjection(Projections.projectionList()
                        .add(Projections.property("s.subjectName")) //0                    
                        .add(Projections.property("p.firstName")) //1
                        .add(Projections.property("p.secondName")) //2
                        .add(Projections.property("p.firstLastname")) //3
                        .add(Projections.property("p.secondLastname")) //4
                        .add(Projections.property("c.codeClassroom")) // 5
                        .add(Projections.property("g.gradeName")) //6
                        .add(Projections.property("sn.letter")) //7
                        .add(Projections.property("sc.turn")) //8
                )
                .add(Restrictions.or(
                        Restrictions.like("p.firstName", text + "%"),
                        Restrictions.like("p.firstLastname", text + "%"),
                        Restrictions.like("c.codeClassroom", text + "%"),
                        Restrictions.like("s.subjectName", text + "%")
                ))
                .list();
        return setSectionsClassroomTeacherSubject(results);
    }

    private ObservableList<SectionsclassroomsTeachers> setSectionsClassroomTeacherSubject(List results) throws NumberFormatException {
        ObservableList<SectionsclassroomsTeachers> sectionsClassroomTeacher = FXCollections.observableArrayList();
        try {

            Iterator i = results.iterator();

            while (i.hasNext()) {
                Object[] obj = (Object[]) i.next();

                Persons person = new Persons(String.valueOf(obj[1]), String.valueOf(obj[2]),
                        String.valueOf(obj[3]), String.valueOf(obj[4]));

                Employees employees = new Employees(person);

                Subjects subjects = new Subjects(String.valueOf(obj[0]));

                Classrooms classrooms = new Classrooms(String.valueOf(obj[5]));

                Grades grades = new Grades(Integer.parseInt(String.valueOf(obj[6])));

                Sections section = new Sections(grades, Integer.parseInt(String.valueOf(obj[7])));

                TeachersSubjects teacherSubject = new TeachersSubjects(employees, subjects);

                SectionsClassrooms sectionsClassrooms = new SectionsClassrooms(
                        classrooms,
                        section,
                        Integer.parseInt(String.valueOf(obj[8])));

                sectionsClassroomTeacher.add(
                        new SectionsclassroomsTeachers(sectionsClassrooms, teacherSubject)
                );

            }
        } catch (java.lang.ClassCastException ex) {
            System.err.println("Error en el setSectionsClassroomTeacherSubject " + ex.getMessage());
        }

        return sectionsClassroomTeacher;
    }

    @Override
    public void update(SectionsclassroomsTeachers object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(SectionsclassroomsTeachers object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public void closeSession() {
        if ((session != null) && (session.isOpen())) {
            session.close();
        }
    }

}
