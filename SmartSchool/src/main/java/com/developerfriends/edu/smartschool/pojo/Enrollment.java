package com.developerfriends.edu.smartschool.pojo;
// Generated 27-jun-2017 19:15:47 by Hibernate Tools 4.3.1


import java.util.Date;

/**
 * Enrollment generated by hbm2java
 */
public class Enrollment  implements java.io.Serializable {


     private Integer enrollmentId;
     private Advancepayments advancepayments;
     private Classrooms classrooms;
     private Employees employees;
     private Students students;
     private Date dataEnrollemet;

    public Enrollment() {
    }

    public Enrollment(Advancepayments advancepayments, Classrooms classrooms, Employees employees, Students students, Date dataEnrollemet) {
       this.advancepayments = advancepayments;
       this.classrooms = classrooms;
       this.employees = employees;
       this.students = students;
       this.dataEnrollemet = dataEnrollemet;
    }
   
    public Integer getEnrollmentId() {
        return this.enrollmentId;
    }
    
    public void setEnrollmentId(Integer enrollmentId) {
        this.enrollmentId = enrollmentId;
    }
    public Advancepayments getAdvancepayments() {
        return this.advancepayments;
    }
    
    public void setAdvancepayments(Advancepayments advancepayments) {
        this.advancepayments = advancepayments;
    }
    public Classrooms getClassrooms() {
        return this.classrooms;
    }
    
    public void setClassrooms(Classrooms classrooms) {
        this.classrooms = classrooms;
    }
    public Employees getEmployees() {
        return this.employees;
    }
    
    public void setEmployees(Employees employees) {
        this.employees = employees;
    }
    public Students getStudents() {
        return this.students;
    }
    
    public void setStudents(Students students) {
        this.students = students;
    }
    public Date getDataEnrollemet() {
        return this.dataEnrollemet;
    }
    
    public void setDataEnrollemet(Date dataEnrollemet) {
        this.dataEnrollemet = dataEnrollemet;
    }




}


