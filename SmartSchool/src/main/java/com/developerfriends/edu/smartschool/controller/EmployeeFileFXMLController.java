/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.controller;

import com.developerfriends.edu.smartschool.pojo.Employees;
import com.developerfriends.edu.smartschool.util.UtilClass;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;

/**
 * FXML Controller class
 *
 * @author Enmnauel Calero
 */
public class EmployeeFileFXMLController implements Initializable {

    private Employees employee;
    UtilClass util = new UtilClass();
    @FXML
    private Label personName;
    @FXML
    private Label gender;
    @FXML
    private Label birdthday;
    @FXML
    private Label age;
    @FXML
    private Label docId;
    @FXML
    private Label role;
    @FXML
    private Label phone;
    @FXML
    private Label email;
    @FXML
    private Label address;
    @FXML
    private ImageView personPhoto;
    @FXML
    private ListView<String> loglist;

    ObservableList<String> log = FXCollections.observableArrayList();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public void setEmployee(Employees e) {
        this.employee = e;
        this.personName.setText(employee.getPersons().getPersonName());
        this.gender.setText(util.gender(employee.getPersons().getGender()));
        this.birdthday.setText(employee.getPersons().getBirthday().toString());
        this.age.setText(util.age(employee.getPersons().getBirthday()) + "");
        this.docId.setText(employee.getIdentityCard());
        this.role.setText(util.getRoles(this.employee.getRole()));
        this.phone.setText(employee.getPhone());
        this.email.setText(employee.getEmail());
        this.address.setText(employee.getPersons().getAddress());
        this.personPhoto.setImage(util.getImage(employee.getPersons().getPhoto()));
        getLogList();
    }

    public void getLogList() {
        for (int i = 0; i < 10; i++) {
            log.add("Aqui apareceran los log de la persona seleccionada");
            log.add("Etapa en desarrollo");
        }
        loglist.setItems(log);
    }

}
