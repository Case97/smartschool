/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.util;

/**
 *
 * @author Tanieska
 */
public class ImageClass {

    public static final String logoWithLetter = "smartschool";
    public static final String onlyLogo = "LogoSmartSchool";
    public static final String newElement = "1497784464_001";
    public static final String updateElement = "1497784606_077";
    public static final String disableElement = "1497784443_004";
    public static final String disableSubElement = "1497784641_008";
    public static final String cancel = "1497784463_009";
    public static final String save = "1497784460_139";
    public static final String saveChange = "1497784629_174";
    public static final String camera = "1497784695_035";
    public static final String undo = "1497784703_021";
    public static final String back = "1497784546_012";
    public static final String defaultUser = "default-user-image";
    public static final String setting = "1497784473_160";
    public static final String check = "1497784468_006";
    public static final String selectPicture = "1497784895_129";
    public static final String lock = "1497784750_106";
    public static final String refresh = "1497784455_019";
    public static final String key = "1497784674_104";
    public static final String moneybag = "1497784619_074";
    public static final String user = "1497784573_167";
    public static final String users = "1497784447_173";
    public static final String pencil = "1497784606_077";    
    public static final String pencils = "1497784862_076";
    public static final String signMoney = "1497784762_075";
    public static final String wand = "1497784765_178";
    public static final String clock = "1497784691_054";
    public static final String star = "1497784772_154";
    public static final String barGraph = "1497784860_156";
    public static final String blueBook = "1497784635_011";
    public static final String pieChart = "1497784876_155";
    public static final String clipboard = "1497784892_050";
    public static final String house = "1497784746_103";
    public static final String orangePoint = "1497784726_063";
    public static final String graphics = "1497784683_157";
    public static final String bigPayment = "Payments";
    public static final String thunderbolt = "1497784655_105";
    public static final String briefcase = "1497784565_038";

    public static String logoWithLetter_A() {
        return addAddress(logoWithLetter);
    }

    public static String onlyLogo_A() {
        return addAddress(onlyLogo);
    }

    public static String defaultUser_A() {
        return addAddress(defaultUser);
    }

    private static String addAddress(String icon) {
        return "/images/" + icon + ".png";
    }

}
