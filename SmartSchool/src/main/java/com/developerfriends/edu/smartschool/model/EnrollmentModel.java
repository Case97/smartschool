/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.model;

import com.developerfriends.edu.smartschool.pojo.Enrollment;
import com.developerfriends.edu.smartschool.util.Dialogs;
import com.developerfriends.edu.smartschool.util.UtilClass;
import javafx.scene.control.Alert;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Enmnauel Calero
 */
public class EnrollmentModel implements EntitiesInterface<Enrollment> {

    Dialogs dialog = new Dialogs();
    UtilClass util = new UtilClass();

    private Session session;

    @Override
    public void save(Enrollment object) {
        Transaction tx = null;
        try {
            tx = getSession().beginTransaction();
            getSession().save(object.getStudents().getPersons());
            getSession().save(object.getStudents());
            getSession().save(object);

            tx.commit();
            dialog.Alerta("Smart School", "Completo", "Se guardo correctamente el registro", Alert.AlertType.INFORMATION);

        } catch (RuntimeException e) {
            if (tx != null) {
                tx.rollback();
            }
            dialog.Alerta("Smart School", "Error", "Sucedio un error al guardar", Alert.AlertType.ERROR);
            throw e;
        } finally {
            closeSession();
        }
    }

    @Override
    public void update(Enrollment object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(Enrollment object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public void closeSession() {
        if ((session != null) && (session.isOpen())) {
            session.close();
        }
    }
}
