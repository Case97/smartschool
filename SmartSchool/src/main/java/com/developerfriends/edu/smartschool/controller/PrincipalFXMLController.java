/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.controller;

import com.developerfriends.edu.smartschool.model.UserModel;
import com.developerfriends.edu.smartschool.pojo.Employees;
import com.developerfriends.edu.smartschool.pojo.Role;
import com.developerfriends.edu.smartschool.util.Dialogs;
import com.developerfriends.edu.smartschool.util.HibernateUtil;
import com.developerfriends.edu.smartschool.util.ImageClass;
import com.developerfriends.edu.smartschool.util.StageController;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.TextFlow;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * FXML Controller class
 *
 * @author 27gma
 */
public class PrincipalFXMLController implements Initializable {

    StageController stController = new StageController();
    Dialogs dialog = new Dialogs();
    protected static int type;
    private static Role roles;
    private static Employees employees;
    private Stage primaryStage;

    public static Role getRoles() {
        return roles;
    }

    public static void setRoles(Role roles) {
        PrincipalFXMLController.roles = roles;
    }

    public static Employees getEmployees() {
        return employees;
    }

    public static void setEmployees(Employees employees) {
        PrincipalFXMLController.employees = employees;
    }

    @FXML
    protected static BorderPane bpPrincipal;
    @FXML
    private AnchorPane CenterPane;
    @FXML
    private TextFlow tfMenu;
    @FXML
    private JFXButton tbbtnCatalog;
    @FXML
    private JFXButton tbbtnOperations;
    @FXML
    private JFXButton tbbtnReports;
    @FXML
    private JFXButton tfbtnPayments;
    @FXML
    private JFXButton tfbtnEmployee;
    @FXML
    private JFXButton tfbtnEnrollement;
    @FXML
    private JFXButton tfbtnUser;
    @FXML
    private JFXButton tfbtnSubject;
    @FXML
    private JFXButton tfbtnHistory;
    @FXML
    private JFXButton tfbtnClassroom;
    @FXML
    private JFXButton tfbtnGrade;
    @FXML
    private JFXButton tfbtnAssignmentClassroom;
    @FXML
    private JFXButton tfbtnHorary;
    @FXML
    private JFXButton tfbtnBestStudents;
    @FXML
    private JFXButton tfbtnPaymentsReports;
    @FXML
    private JFXButton tfbtnMonthlyPaymentReports;
    @FXML
    private JFXButton tfbtnAddNote;
    @FXML
    private Label currentUserName;
    @FXML
    private JFXButton tfbtnDuty;
    @FXML
    private JFXButton btnCloseSession;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO        
        Start();
        Icon();

        currentUserName.setText((UserModel.getCurrentUser() == null) ? "Desarrollador" : UserModel.getCurrentUser().getPersons().getPersonName());
    }

    public void Icon() {
        stController.Icon(tbbtnCatalog, ImageClass.briefcase);
        stController.Icon(tbbtnOperations, ImageClass.thunderbolt);
        stController.Icon(tbbtnReports, ImageClass.graphics);
        stController.Icon(tfbtnEmployee, ImageClass.users);
        stController.Icon(tfbtnUser, ImageClass.user);
        stController.Icon(tfbtnSubject, ImageClass.pencils);
        stController.Icon(tfbtnGrade, ImageClass.orangePoint);
        stController.Icon(tfbtnClassroom, ImageClass.house);
        stController.Icon(tfbtnEnrollement, ImageClass.clipboard);
        stController.Icon(tfbtnPayments, ImageClass.signMoney);
        stController.Icon(tfbtnAddNote, ImageClass.wand);
        stController.Icon(tfbtnAssignmentClassroom, ImageClass.clipboard);
        stController.Icon(tfbtnHorary, ImageClass.clock);
        stController.Icon(tfbtnBestStudents, ImageClass.star);
        stController.Icon(tfbtnPaymentsReports, ImageClass.pieChart);
        stController.Icon(tfbtnMonthlyPaymentReports, ImageClass.barGraph);
        stController.Icon(tfbtnHistory, ImageClass.blueBook);
        stController.Icon(tfbtnDuty, ImageClass.moneybag);
        stController.Icon(btnCloseSession, ImageClass.key);
    }

    public PrincipalFXMLController() {
    }

    public void Start() {
        this.tfMenu.setVisible(false);
        this.tfbtnEmployee.setDisable(!roles.getEmployeAccess());
        this.tfbtnUser.setDisable(!roles.getUsersAccess());
        this.tfbtnSubject.setDisable(!roles.getSubjectsAccess());
        this.tfbtnGrade.setDisable(!roles.getGradesAccess());
        this.tfbtnClassroom.setDisable(!roles.getSectionsAccess());
        this.tfbtnDuty.setDisable(!roles.getDutyAccess());
        this.tfbtnEnrollement.setDisable(!roles.getEnrollmentAccess());
        this.tfbtnPayments.setDisable(!roles.getCashAccess());
        this.tfbtnAddNote.setDisable(!roles.getAddGradeNoteAccess());
        this.tfbtnAssignmentClassroom.setDisable(!roles.getAssignmentSubjectsAccess());
        this.tfbtnHorary.setDisable(!roles.getHoraryAccess());
        this.tfbtnBestStudents.setDisable(!roles.getBestStudentsReportAccess());
        this.tfbtnPaymentsReports.setDisable(!roles.getPaymentsReportAccess());
        this.tfbtnMonthlyPaymentReports.setDisable(!roles.getMonthlyPaymentsReportAccess());
        this.tfbtnHistory.setDisable(!roles.getRecordReportAccess());
    }

    @FXML
    private void showCatalog(ActionEvent event) {
        this.tfMenu.setVisible(true);
        add_remove(1, true);
        add_remove(2, false);
        add_remove(3, false);
    }

    @FXML
    private void ShowOperations(ActionEvent event) {
        this.tfMenu.setVisible(true);
        add_remove(1, false);
        add_remove(2, true);
        add_remove(3, false);
    }

    @FXML
    private void ShowReports(ActionEvent event) {
        this.tfMenu.setVisible(true);
        add_remove(1, false);
        add_remove(2, false);
        add_remove(3, true);
    }

    private Node[] nodes(int opc) {
        Node[] obj = null;
        switch (opc) {
            //Catalogo
            case 1:
                obj = new Node[]{this.tfbtnEmployee, this.tfbtnUser, this.tfbtnSubject, this.tfbtnGrade, this.tfbtnClassroom, this.tfbtnDuty};
                break;
            //Operaciones    
            case 2:
                obj = new Node[]{this.tfbtnPayments, this.tfbtnEnrollement, this.tfbtnAssignmentClassroom, this.tfbtnHorary, this.tfbtnAddNote};
                break;
            //Reportes    
            case 3:
                obj = new Node[]{this.tfbtnBestStudents, this.tfbtnMonthlyPaymentReports, this.tfbtnPaymentsReports, this.tfbtnHistory};
                break;
        }
        return obj;
    }

    private void add_remove(int opc, boolean ra) {
        try {
            for (Node node : nodes(opc)) {
                if (ra) {
                    if (!this.tfMenu.getChildren().contains(node)) {
                        this.tfMenu.getChildren().add(node);
                    }
                } else {
                    if (this.tfMenu.getChildren().contains(node)) {
                        this.tfMenu.getChildren().remove(node);
                    }
                }
            }
        } catch (IllegalArgumentException ex) {
            System.err.println("Error " + ex.getMessage());
        }
    }

    @FXML
    private void ShowEmployee(ActionEvent event) {
        stController.MountScene(event, "/fxml/EmployeeFXML.fxml", "Empleados");
    }

    @FXML
    private void ShowUser(ActionEvent event) {
        stController.MountScene(event, "/fxml/UserFXML.fxml", "Usuarios");
    }

    public void back(ActionEvent event) {
        stController.clear(event);
    }

    @FXML
    private void ShowSubject(ActionEvent event) {
        stController.MountScene(event, "/fxml/SubjectFXML.fxml", "Materias");
    }

    @FXML
    private void ShowGrades(ActionEvent event) {
        stController.MountScene(event, "/fxml/GradeFXML.fxml", "Grados");
    }

    @FXML
    private void ShowClassroom(ActionEvent event) {
        stController.MountScene(event, "/fxml/ClassroomFXML.fxml", "Aula");
    }

    @FXML
    private void ShowEnrollment(ActionEvent event) {
        stController.MountScene(event, "/fxml/EnrollmentFXML.fxml", "Matricula");
    }

    @FXML
    private void ShowPayments(ActionEvent event) {
        PaymentsFXMLController controller = new PaymentsFXMLController();
        controller.setEmployees(getEmployees());
        stController.MountScene(event, "/fxml/PaymentsFXML.fxml", "Pago");
    }

    @FXML
    private void ShowAddNote(ActionEvent event) {
    }

    @FXML
    private void ShowAssignmentClassroom(ActionEvent event) {
        stController.MountScene(event, "/fxml/SectionsClassroomsTeachersFXML.fxml", "Materias");
    }

    @FXML
    private void ShowHorary(ActionEvent event) {
        stController.MountScene(event, "/fxml/HoraryFXML.fxml", "Materias");

    }

    @FXML
    private void ShowBestStudents(ActionEvent event) {
    }

    @FXML
    private void ShowPaymentsReports(ActionEvent event) {
    }

    @FXML
    private void ShowMonthlyPaymentReports(ActionEvent event) {
    }

    @FXML
    private void ShowHistory(ActionEvent event) {
    }

    @FXML
    private void ShowDuty(ActionEvent event) {
        stController.MountScene(event, "/fxml/DutyFXML.fxml", "Materias");
    }

    @FXML
    private void logout(ActionEvent event) {

        if (dialog.Confirm("SmartSchool", "Confirmación", "¿Desea cerrar sesión..?")) {
            Node source = (Node) event.getSource();
            Stage theStage = (Stage) source.getScene().getWindow();
            theStage.close();
            FXMLLoader load = new FXMLLoader();
            URL url = LoginFXMLController.class.getResource("/fxml/LoginFXML.fxml");
            Stage stage = stController.showStage(url, "Smart School", primaryStage, load);
            LoginFXMLController controller = load.getController();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.centerOnScreen();
            controller.setDialogStage(stage);
            stage.setOnCloseRequest((WindowEvent e) -> {
                HibernateUtil.closeSessionFactory();
                System.exit(0);
            });
            stage.show();
        }

    }

}
