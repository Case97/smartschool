/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.controller;

import com.crazycode.custom.elements.SearchField;
import com.crazycode.mask.elements.IntegerField;
import com.developerfriends.edu.smartschool.model.GradesModel;
import com.developerfriends.edu.smartschool.model.SectionsModel;
import com.developerfriends.edu.smartschool.pojo.Grades;
import com.developerfriends.edu.smartschool.pojo.Sections;
import com.developerfriends.edu.smartschool.util.Dialogs;
import com.developerfriends.edu.smartschool.util.HibernateUtil;
import com.developerfriends.edu.smartschool.util.ImageClass;
import com.developerfriends.edu.smartschool.util.StageController;
import com.developerfriends.edu.smartschool.util.UtilClass;
import com.developerfriends.edu.smartschool.util.Validation;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXToggleButton;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 * FXML Controller class
 *
 * @author 27gma
 */
public class GradeFXMLController implements Initializable {

    Dialogs dialog = new Dialogs();
    PrincipalFXMLController principal = new PrincipalFXMLController();
    GradesModel gradesModel = new GradesModel();
    SectionsModel sectionModel = new SectionsModel();
    UtilClass util = new UtilClass();
    Validation validation = new Validation();
    StageController stController = new StageController();

    private SessionFactory sFactory;
    private int gradesId;
    private int sectionId;
    private int gradeName;
    private boolean isEdit;

    private final Boolean ACTIVE = Boolean.TRUE;
    private final Boolean INACTIVE = Boolean.FALSE;

    List<Sections> toAdd = new ArrayList<>();
    List<Sections> toDelete = new ArrayList<>();
    List<Sections> isExist = new ArrayList<>();
    ObservableList<Sections> olSection = FXCollections.observableArrayList();

    @FXML
    private JFXButton btnBack;
    @FXML
    private TableView<Grades> tableGrade;
    @FXML
    private TableView<Sections> tableSection;
    @FXML
    private TableColumn<Grades, String> colGradeName;
    @FXML
    private TableColumn<Sections, String> colSectionLetter;
    @FXML
    private JFXButton btnNewGrade;
    @FXML
    private JFXButton btnUpdateGrade;
    @FXML
    private JFXButton btnDisableGrade;
    @FXML
    private IntegerField txtGradeName;
    @FXML
    private JFXButton btnSave;
    @FXML
    private JFXButton btnSaveChange;
    @FXML
    private JFXButton btnCancel;
    @FXML
    private JFXButton btnAddSection;
    @FXML
    private JFXButton btnDeleteSection;
    @FXML
    private HBox hBBtn;
    @FXML
    private AnchorPane apElements;
    @FXML
    private JFXToggleButton tgEnable;
    @FXML
    private ListView<String> listSections;
    @FXML
    private SearchField txtSearch;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        Icons();

        getSessionFactory();

        apElements.setVisible(false);

        enableButton(1, true);
        enableButton(2, true);

        colGradeName.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(util.getGrade(cellData.getValue().getGradeName())));
        colSectionLetter.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(String.valueOf(util.getLetter(cellData.getValue().getLetter()))));

        search();

        loadGrade(ACTIVE);
    }

    public void Icons() {
        stController.Icon(btnNewGrade, ImageClass.newElement);
        stController.Icon(btnUpdateGrade, ImageClass.updateElement);
        stController.Icon(btnDisableGrade, ImageClass.disableElement);
        stController.Icon(btnSave, ImageClass.save);
        stController.Icon(btnSaveChange, ImageClass.saveChange);
        stController.Icon(btnCancel, ImageClass.cancel);
        stController.Icon(btnAddSection, ImageClass.newElement);
        stController.Icon(btnDeleteSection, ImageClass.disableSubElement);
        stController.Icon(btnBack, ImageClass.back);
    }

    private void loadGrade(Boolean state) {
        try {
            gradesModel.setSession(openSession());
            tableGrade.setItems(gradesModel.readGrades(state));
        } catch (NullPointerException ex) {
            System.err.println("Error al mostrar la tabla Grado " + ex.getMessage());
        }

    }

    private void loadSection(Grades grades) {
        try {
            sectionModel.setSession(openSession());
            tableSection.setItems(sectionModel.readSections(grades));
        } catch (NullPointerException ex) {
            System.err.println("Error al mostrar la tabla Grado " + ex.getMessage());
        }
    }

    private void findGrade(Boolean state, String text) {
        try {
            gradesModel.setSession(openSession());
            tableGrade.setItems(gradesModel.findGrades(state, text));
        } catch (NullPointerException ex) {
            System.err.println("Error al mostrar la tabla Grado " + ex.getMessage());
        }

    }

    @FXML
    private void showGrades(ActionEvent event) {
        String tgText = (tgEnable.isSelected()) ? "Habilitados" : "Deshabilitados";
        String btnText = (tgEnable.isSelected()) ? "Deshabilitar" : "Habilitar";
        boolean enable = !(tgEnable.isSelected());
        Boolean state = (tgEnable.isSelected()) ? ACTIVE : INACTIVE;

        tgEnable.setText(tgText);
        btnDisableGrade.setText(btnText);
        enableButton(4, true);
        enableButton(6, enable);
        loadGrade(state);
        listSections.getItems().clear();
    }

    @FXML
    private void newGrade(ActionEvent event) {
        isEdit = false;
        this.apElements.setVisible(true);
        enableButton(1, true);
        enableButton(2, true);
        enableButton(3, true);
        showButton(1);
    }

    @FXML
    private void updateGrade(ActionEvent event) {
        Grades selectGrades = (Grades) tableGrade.getSelectionModel().getSelectedItem();
        if (selectGrades != null) {
            isEdit = true;
            this.apElements.setVisible(true);
            enableButton(1, true);
            enableButton(2, true);
            enableButton(3, true);
            showButton(2);
            setGrades(selectGrades);
            fillIsExist(selectGrades);
        } else {
            dialog.Alerta("Advertencia", "No se ha encontrado el grado", "Por favor seleccione un grado en la tabla", Alert.AlertType.WARNING);
        }
    }

    @FXML
    private void disableGrade(ActionEvent event) {
        Grades selectGrades = (Grades) tableGrade.getSelectionModel().getSelectedItem();
        if (selectGrades != null) {
            if (tgEnable.isSelected()) {
                if (dialog.Confirm("Deshabilitar", "", "¿Esta seguro que quiere deshabilitar este grado?")) {
                    active_inactive(selectGrades, INACTIVE, 0);
                }
            } else {
                if (dialog.Confirm("Habilitar", "", "¿Esta seguro que quiere habilitar este grado?")) {
                    active_inactive(selectGrades, ACTIVE, 1);
                }
            }
        } else {
            dialog.Alerta("Advertencia", "No se ha encontrado el grado", "Por favor seleccione un grado en la tabla", Alert.AlertType.WARNING);
        }
    }

    private void active_inactive(Grades selectGrades, Boolean state, int type) {
        selectGrades.setState(state);
        gradesModel.setSession(openSession());
        if (type == 0) {
            gradesModel.delete(selectGrades);
        } else {
            gradesModel.active(selectGrades);
        }
        loadGrade(!state);
        listSections.getItems().clear();
    }

    @FXML
    private void addSection(ActionEvent event) {
        Sections sections;
        String result = dialog.choiceDialog(
                "Smart School",
                "Asignación de Sección", "Seleccione la sección:",
                util.setLetterAvaible(selectedLetter()));
        if (result != null) {
            sections = new Sections(util.setLetter(result));
            if (isEdit) {
                if (!isExist.contains(sections)) {
                    toAdd.add(sections);
                }
            }
            olSection.add(sections);
            tableSection.setItems(olSection);
        }
    }

    @FXML
    private void deleteSection(ActionEvent event) {
        try {
            Sections selectSections = (Sections) tableSection.getSelectionModel().getSelectedItem();
            if (selectSections != null) {
                if (isEdit) {
                    if (isExist.contains(selectSections)) {
                        toDelete.add(selectSections);
                    }
                }
                olSection.remove(selectSections);
                tableSection.setItems(olSection);
            }
        } catch (IndexOutOfBoundsException ex) {
            System.err.println("Error en deleteSection " + ex.getMessage());
        }
    }

    @FXML
    private void enableGradeOption(MouseEvent event) {
        if (tgEnable.isSelected()) {
            enableButton(1, false);
        } else {
            enableButton(5, false);
        }
        Grades selectGrades = (Grades) tableGrade.getSelectionModel().getSelectedItem();
        if (selectGrades != null) {
            sectionModel.setSession(openSession());
            listSections.setItems(sectionModel.getOnlySection(selectGrades));
        }
    }

    private void search() {
        txtSearch.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            Boolean state = (tgEnable.isSelected()) ? ACTIVE : INACTIVE;
            if (newValue.isEmpty()) {
                loadGrade(state);
            } else {
                findGrade(state, newValue);
            }
            listSections.getItems().clear();
        });
    }

    @FXML
    private void enableSectionOption(MouseEvent event) {
        enableButton(2, false);
    }

    @FXML
    private void save(ActionEvent event) {
       if(isEmpty()) return;
       if(isOkNumber()) return;
       if (repeat_disable(0, 0)) return;
       if (repeat_disable(0, 1)) return;
       if(isMinSection()) return;

        List<Integer> isSave = new ArrayList<>();
        Grades grades = getGrade();
        gradesModel.setSession(openSession());
        gradesModel.save(grades);
        isSave.add(gradesModel.getIsSave());
        Sections section;

        for (Sections item : tableSection.getItems()) {
            section = new Sections(grades, item.getLetter(), ACTIVE);
            sectionModel.setSession(openSession());
            sectionModel.save(section);
            isSave.add(sectionModel.getIsSave());
        }
        if (isSave.contains(0)) {
            dialog.Alerta("Smart School", "Error", "Sucedio un error al guardar", Alert.AlertType.ERROR);
        } else {
            dialog.Alerta("Smart School", "Completo", "Se guardo correctamente el registro", Alert.AlertType.INFORMATION);
        }
        apElements.setVisible(false);
        isSave.clear();
        enableButton(3, false);
        loadGrade(ACTIVE);
        clear();
    }

    @FXML
    private void saveChange(ActionEvent event) {
       if(isEmpty()) return;
       if(isOkNumber()) return;
       if (repeat_disable(1, 0)) return;
       if (repeat_disable(1, 1)) return;
       if(isMinSection()) return;

        List<Integer> isUpdate = new ArrayList<>();
        Grades grade = getGrade();
        grade.setGradeId(getGradesId());
        gradesModel.setSession(openSession());
        gradesModel.update(grade);
        isUpdate.add(gradesModel.getIsUpdate());
        Sections section;
        for (Sections item : toAdd) {
            sectionModel.setSession(openSession());
            section = new Sections(grade, item.getLetter(), ACTIVE);
            sectionModel.save(section);
            isUpdate.add(sectionModel.getIsSave());
        }
        for (Sections item : toDelete) {
            sectionModel.setSession(openSession());
            section = new Sections(grade, item.getLetter(), INACTIVE);
            section.setSectionId(item.getSectionId());
            sectionModel.delete(section);
            isUpdate.add(sectionModel.getIsDelete());
        }
        if (isUpdate.contains(0)) {
            dialog.Alerta("Smart School", "Error", "Sucedio un error al actualizar", Alert.AlertType.ERROR);
        } else {
            dialog.Alerta("Smart School", "Completo", "Se actualizo correctamente los registro", Alert.AlertType.INFORMATION);
        }
        apElements.setVisible(false);
        isUpdate.clear();
        enableButton(3, false);
        loadGrade(ACTIVE);
        clear();
    }

    @FXML
    private void cancel(ActionEvent event) {
        apElements.setVisible(false);
        enableButton(3, false);
        clear();
    }

    @FXML
    private void back(ActionEvent event) {
        principal.back(event);
    }

    private boolean isEmpty() {
        if (this.txtGradeName.getText().isEmpty()) {
            dialog.Alerta("Smart School", "Error", "No puede dejar campos vacios", AlertType.ERROR);
            return true;
        }
        return false;
    }

    private boolean isOkNumber() {
        if (Integer.parseInt(txtGradeName.getText()) == 0
                || Integer.parseInt(txtGradeName.getText()) > 12) {
            dialog.Alerta("Smart School", "Error", "El grado insertado no es valido", AlertType.ERROR);
            return true;
        }
        return false;
    }

    private boolean repeat_disable(int type, int opc) {
        int grade = Integer.parseInt(this.txtGradeName.getText());
        Boolean state = (opc == 0) ? INACTIVE : ACTIVE;
        boolean isDisable = (opc == 0);

        if (type == 0) {
            return isInsert(fillToCheck(state), grade, isDisable);
        } else {
            if (Integer.parseInt(this.txtGradeName.getText()) != getGradeName()) {
                return isInsert(fillToCheck(state), grade, isDisable);
            }
        }
        return false;
    }

    private List<Integer> fillToCheck(Boolean state) {
        List<Integer> oldGrades = new ArrayList<>();

        for (Grades item : gradesModel.readGrades(state)) {
            oldGrades.add(item.getGradeName());
        }
        return oldGrades;
    }

    private boolean isInsert(List<Integer> oldGrades, int grade, boolean isDisable) {
        if (oldGrades.contains(grade)) {
            String message = (isDisable)
                    ? "Ese grado esta deshabilitado, chequee en la sección de deshabilitados"
                    : "Ese grado ya existe";
            dialog.Alerta("Smart School", "Error", message, AlertType.ERROR);
            return true;
        }
        return false;

    }

    private boolean isMinSection() {
        if (tableSection.getItems().isEmpty()) {
            dialog.Alerta("Smart School", "Error", "Agrege como minimo una sección", AlertType.ERROR);
            return true;
        }
        return false;
    }

    private void clear() {
        this.txtGradeName.setText("");
        isEdit = false;
        tableSection.getItems().clear();
        toAdd.clear();
        toDelete.clear();
        isExist.clear();
    }

    private void enableButton(int type, boolean opc) {
        switch (type) {
            case 1:
                this.btnUpdateGrade.setDisable(opc);
                this.btnDisableGrade.setDisable(opc);
                break;
            case 2:
                this.btnDeleteSection.setDisable(opc);
                break;
            case 3:
                this.btnBack.setDisable(opc);
                this.txtSearch.setDisable(opc);
                break;
            case 4:
                this.btnUpdateGrade.setDisable(opc);
                this.btnDisableGrade.setDisable(true);
                break;
            case 5:
                this.btnDisableGrade.setDisable(opc);
                break;
            case 6:
                this.btnNewGrade.setDisable(opc);
                break;
        }
    }

    private Grades getGrade() {
        return new Grades(Integer.parseInt(this.txtGradeName.getText()), ACTIVE);
    }

    private void setGrades(Grades grades) {
        this.txtGradeName.setText(String.valueOf(grades.getGradeName()));
        setGradesId(grades.getGradeId());
        setGradeName(grades.getGradeName());
    }

    private void fillIsExist(Grades selectGrades) {
        loadSection(selectGrades);
        for (Sections item : tableSection.getItems()) {
            isExist.add(new Sections(item.getSectionId(), item.getLetter()));
        }
        olSection.addAll(isExist);
        tableSection.getItems().clear();
        tableSection.setItems(olSection);
    }

    private List<String> selectedLetter() throws IndexOutOfBoundsException {
        List<String> letter = new ArrayList<>();
        for (Sections item : tableSection.getItems()) {
            letter.add(util.getLetter(item.getLetter()));
        }
        return letter;
    }

    private Session openSession() {
        return sFactory.openSession();
    }

    private void getSessionFactory() {
        sFactory = HibernateUtil.getSessionFactory();
    }

    public int getGradesId() {
        return gradesId;
    }

    public void setGradesId(int gradesId) {
        this.gradesId = gradesId;
    }

    public int getSectionId() {
        return sectionId;
    }

    public void setSectionId(int sectionId) {
        this.sectionId = sectionId;
    }

    public int getGradeName() {
        return gradeName;
    }

    public void setGradeName(int gradeName) {
        this.gradeName = gradeName;
    }

    private void showButton(int opc) {
        switch (opc) {
            //Nuevo
            case 1:
                add_remove(2, btnCancel);
                add_remove(2, btnSaveChange);
                add_remove(1, btnSave);
                add_remove(1, btnCancel);
                break;
            //Actualizar    
            case 2:
                add_remove(2, btnCancel);
                add_remove(2, btnSave);
                add_remove(1, btnSaveChange);
                add_remove(1, btnCancel);

                break;
        }
    }

    private void add_remove(int opc, Node node) {
        switch (opc) {
            //Agregar
            case 1:
                if (!this.hBBtn.getChildren().contains(node)) {
                    this.hBBtn.getChildren().add(node);
                }
                break;
            //Remover    
            case 2:
                if (this.hBBtn.getChildren().contains(node)) {
                    this.hBBtn.getChildren().remove(node);
                }
                break;
        }
    }

}
