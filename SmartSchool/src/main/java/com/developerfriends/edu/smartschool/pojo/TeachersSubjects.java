package com.developerfriends.edu.smartschool.pojo;
// Generated 06-30-2017 09:16:31 PM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;

/**
 * TeachersSubjects generated by hbm2java
 */
public class TeachersSubjects  implements java.io.Serializable {


     private Integer teacherSubjectId;
     private Employees employees;
     private Subjects subjects;
     private Boolean state;
     private Set horaries = new HashSet(0);

    public TeachersSubjects() {
    }

    public TeachersSubjects(Employees employees, Subjects subjects, Boolean state, Set horaries) {
       this.employees = employees;
       this.subjects = subjects;
       this.state = state;
       this.horaries = horaries;
    }

    public TeachersSubjects(Integer teacherSubjectId, Employees employees, Subjects subjects) {
        this.teacherSubjectId = teacherSubjectId;
        this.employees = employees;
        this.subjects = subjects;
    }

    public TeachersSubjects(Employees employees, Subjects subjects, Boolean state) {
        this.employees = employees;
        this.subjects = subjects;
        this.state = state;
    }

    public TeachersSubjects(Employees employees, Subjects subjects) {
        this.employees = employees;
        this.subjects = subjects;
    }
    
    public Integer getTeacherSubjectId() {
        return this.teacherSubjectId;
    }
    
    public void setTeacherSubjectId(Integer teacherSubjectId) {
        this.teacherSubjectId = teacherSubjectId;
    }
    public Employees getEmployees() {
        return this.employees;
    }
    
    public void setEmployees(Employees employees) {
        this.employees = employees;
    }
    public Subjects getSubjects() {
        return this.subjects;
    }
    
    public void setSubjects(Subjects subjects) {
        this.subjects = subjects;
    }
    public Boolean getState() {
        return this.state;
    }
    
    public void setState(Boolean state) {
        this.state = state;
    }
    public Set getHoraries() {
        return this.horaries;
    }
    
    public void setHoraries(Set horaries) {
        this.horaries = horaries;
    }




}


