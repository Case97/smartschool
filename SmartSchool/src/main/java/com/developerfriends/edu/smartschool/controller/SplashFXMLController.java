/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.controller;

import com.developerfriends.edu.smartschool.util.HibernateUtil;
import com.developerfriends.edu.smartschool.util.ImageClass;
import com.developerfriends.edu.smartschool.util.StageController;
import com.jfoenix.controls.JFXProgressBar;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 * FXML Controller class
 *
 * @author Tani Aguirre
 */
public class SplashFXMLController implements Initializable {

    StageController stController = new StageController();

    private Stage primaryStage;

    SessionFactory sFactory;

    Session session;

    @FXML
    private JFXProgressBar loadingBar;
    @FXML
    private Label progressText;
    @FXML
    private Pane splashLayout;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        init();
    }

    public void init() {
        try {
            splashLayout = new Pane();
            splashLayout.setPrefHeight(200);
            splashLayout.setPrefWidth(500);
            ImageView splash = new ImageView(
                    new Image(
                            getClass().getResource(ImageClass.logoWithLetter_A()).toString()
                    ));
            splash.setFitHeight(200);
            splash.setFitWidth(300);
            splash.setLayoutX(100);
            splash.setLayoutY(15);
            splash.setPickOnBounds(true);
            splash.setPreserveRatio(true);
            progressText = new Label("Cargando Modulos...!!");
            progressText.setLayoutX(15);
            progressText.setLayoutY(150);
            progressText.fontProperty().setValue(new Font(18));
            loadingBar.setLayoutX(0);
            loadingBar.setLayoutY(180);            
            splashLayout.setStyle(
                    "-fx-padding: 5; "
                    + "-fx-background-color: white; "
            );
            splashLayout.getChildren().addAll(splash, loadingBar, progressText);
            splashLayout.setEffect(new DropShadow());
        } catch (Exception ex) {
            System.err.println("Error " + ex.getMessage());
        }

    }

    public void start(final Stage initStage) throws Exception {
        final Task<ObservableList<Boolean>> connectionTask = new Task<ObservableList<Boolean>>() {
            @Override
            protected ObservableList<Boolean> call() throws InterruptedException {
                ObservableList<Boolean> listConnection
                        = FXCollections.<Boolean>observableArrayList(sFactory != null, session != null);
                boolean isSFactoryOpen;
                boolean isSessionOpen;
                updateProgress(0, listConnection.size());
                updateMessage("Estableciendo conexión. . .");
                Thread.sleep(400);

                sFactory = HibernateUtil.getSessionFactory();
                isSFactoryOpen = HibernateUtil.isOpen();

                if (isSFactoryOpen) {
                    updateProgress(1, listConnection.size());

                    updateMessage("Estableciendo . . .");
                } else {
                    Thread.sleep(100);
                    updateMessage("No se logro establecer la conexión");
                    cancel();
                }

                Thread.sleep(400);

                session = sFactory.openSession();
                isSessionOpen = session != null;
                Thread.sleep(400);

                if (isSessionOpen) {
                    updateProgress(2, listConnection.size());
                    updateMessage("Espere . . .");
                } else {
                    updateMessage("No se logro establecer la conexión");
                    Thread.sleep(600);
                    cancel();
                }

                updateMessage("Conexión establecida");
                Thread.sleep(400);
                return listConnection;
            }
        };

        showSplash(initStage,
                connectionTask,
                () -> showMainStage()
        );
        new Thread(connectionTask).start();
    }

    private void showMainStage() {
        FXMLLoader load = new FXMLLoader();
        URL url = LoginFXMLController.class.getResource("/fxml/LoginFXML.fxml");
        Stage stage = stController.showStage(url, "Smart School", primaryStage, load);
        LoginFXMLController controller = load.getController();
        stage.initModality(Modality.WINDOW_MODAL);  
        stage.centerOnScreen();
        controller.setDialogStage(stage);

        stage.setOnCloseRequest((WindowEvent event) -> {
            HibernateUtil.closeSessionFactory();
            System.exit(0);
        });

        stage.show();

    }

    private void showSplash(final Stage initStage,
            Task<?> task, InitCompletionHandler initCompletionHandler) {
        progressText.textProperty().bind(task.messageProperty());
        loadingBar.progressProperty().bind(task.progressProperty());
        task.stateProperty().addListener((observableValue, oldState, newState) -> {

            if (newState == Worker.State.SUCCEEDED) {
                loadingBar.progressProperty().unbind();
                loadingBar.setProgress(1);
                initStage.hide();

                initCompletionHandler.complete();
            } // todo add code to gracefully handle other task states..
            else if (newState == Worker.State.CANCELLED) {
                session.close();
                HibernateUtil.closeSessionFactory();
                System.exit(0);
                initStage.close();
            }
        });

        Scene splashScene = new Scene(splashLayout, Color.TRANSPARENT);
        initStage.setScene(splashScene);
        initStage.setAlwaysOnTop(true);
        initStage.show();
    }

    public interface InitCompletionHandler {

        void complete();
    }

}
