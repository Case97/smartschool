/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.controller;

import com.developerfriends.edu.smartschool.model.EmployeesModel;
import com.developerfriends.edu.smartschool.model.RoleModel;
import com.developerfriends.edu.smartschool.model.UserModel;
import com.developerfriends.edu.smartschool.pojo.Employees;
import com.developerfriends.edu.smartschool.pojo.Role;
import com.developerfriends.edu.smartschool.pojo.Users;
import com.developerfriends.edu.smartschool.util.Dialogs;
import com.developerfriends.edu.smartschool.util.Hash;
import com.developerfriends.edu.smartschool.util.HibernateUtil;
import com.developerfriends.edu.smartschool.util.ImageClass;
import com.developerfriends.edu.smartschool.util.Mail;
import com.developerfriends.edu.smartschool.util.PopUpMessage;
import com.developerfriends.edu.smartschool.util.StageController;
import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamException;
import com.github.sarxos.webcam.WebcamResolution;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 * FXML Controller class
 *
 * @author Tani Aguirre
 */
public class LoginFXMLController implements Initializable {

    PopUpMessage popUpMessage = new PopUpMessage();
    StageController stController = new StageController();
    Hash hash = new Hash();
    Users userCurrentSession = new Users();
    UserModel userModel = new UserModel();
    EmployeesModel employeesModel = new EmployeesModel();
    RoleModel roleModel = new RoleModel();
    Dialogs dialog = new Dialogs();
    Mail mail;
    private Stage primaryStage;
    private Webcam webcam = null;
    Image image;
    Session session;
    int count;
    private Stage dialogStage;
    protected String nameImage;
    protected BufferedImage imageOutSider;
    private SessionFactory sFactory;
    @FXML
    private JFXTextField txtUserName;
    @FXML
    private JFXPasswordField txtPass;
    @FXML
    private ImageView imgView;
    @FXML
    private JFXButton Acept;
    @FXML
    private JFXButton cancel;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        getSessionFactory();
        image = new Image(getClass().getResource(ImageClass.logoWithLetter_A()).toString());
        imgView.setImage(image);
        Icons();
    }

    private Session openSession() {
        return sFactory.openSession();
    }

    private void getSessionFactory() {
        sFactory = HibernateUtil.getSessionFactory();
    }

    public void Icons() {
        stController.Icon(Acept, ImageClass.check);
        stController.Icon(cancel, ImageClass.cancel);
    }

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    @FXML
    private void Acept(ActionEvent event) {
        userModel.setSession(openSession());
        roleModel.setSession(openSession());
        employeesModel.setSession(openSession());
        userCurrentSession = userModel.getUser(this.txtUserName.getText());

        if (userCurrentSession != null) {
            try {
                if (userCurrentSession.getPasswordU().equals(hash.Sha256Hash(this.txtPass.getText()))) {
                    try {
                        popUpMessage.informationMessage("Bienvenido " + UserModel.getCurrentUser().getPersons().getPersonName());
                        Role role = roleModel.getRoleByUserId(UserModel.getCurrentUser().getUserId());
                        Employees employees = employeesModel.getEmployees(UserModel.getCurrentUser().getUserId());
                        PrincipalFXMLController p = new PrincipalFXMLController();
                        if (role != null) {
                            p.setRoles(role);
                            p.setEmployees(employees);
                            stController.changeStage(p.getClass().getResource("/fxml/PrincipalFXML.fxml"), ((Node) event.getSource()));
                        } else {
                            Role roleTemp = new Role(new Users(), Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE);
                            p.setRoles(roleTemp);
                            stController.changeStage(p.getClass().getResource("/fxml/PrincipalFXML.fxml"), ((Node) event.getSource()));
                        }

                    } catch (InstantiationError ex) {
                        System.err.println("Matracaso " + ex.getMessage());
                    }
                } else {
                    count++;
                    popUpMessage.errorMessage("CLAVE INCORRECTA...!!");
                    Paint ColorRed = Paint.valueOf("Red");
                    txtPass.setFocusColor(ColorRed);
                    txtPass.setUnFocusColor(ColorRed);
                }
            } catch (NoSuchAlgorithmException ex) {
                System.err.println("Error " + ex.getMessage());
            }
        } else {
            if (this.txtUserName.getText().equals("Admin") && this.txtPass.getText().equals("root")) {
                try {
                    popUpMessage.informationMessage("Bienvenido Desarrollador");
                    PrincipalFXMLController p = new PrincipalFXMLController();
                    Role role = new Role(new Users(), Boolean.TRUE, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE);
                    p.setRoles(role);
                    stController.changeStage(p.getClass().getResource("/fxml/PrincipalFXML.fxml"), ((Node) event.getSource()));

                } catch (InstantiationError ex) {
                    System.err.println("Matracaso " + ex.getMessage());
                }
            } else {
                popUpMessage.errorMessage("El Usuario No Existe");
            }
        }
        if (count == 3) {
            mail = new Mail("tani_ac", "20150018u");
            String to[] = {userModel.getEmailByUserId(userCurrentSession.getUserId())};
            String from = "smartschool@noreply";
            String body = "Ingreso fallida al sistema";
            takeWrite();
            sendEmail(to, from, body);
            count = 0;
        }
    }

    @FXML
    private void Cancelar(ActionEvent event) {
        HibernateUtil.closeSessionFactory();
        System.exit(0);
    }

    public void takeWrite() {
        try {
            webcam = Webcam.getDefault();
            webcam.setViewSize(WebcamResolution.VGA.getSize());
            webcam.open();
            imageOutSider = webcam.getImage();
            nameImage = String.format("intruso-%d.jpg", System.currentTimeMillis());
            webcam.close();
            File tempDir = new File(System.getProperty("java.io.tmpdir"));
            System.out.println("Temp File Path: " + tempDir);

            String path = tempDir.getAbsolutePath() + "\\" + "SmartSchool";
            File file = new File(path);
            System.out.println("new Path " + path);
            file.mkdir();

            if (ImageIO.write(imageOutSider, "jpg", new File(path + "\\" + nameImage))) {
                System.out.println("path Image " + path + "\\" + nameImage);
                mail.addAttachment(path, nameImage);
            }
        } catch (WebcamException ex) {
            System.err.println("Error en al tomar la foto " + ex.getMessage());
        } catch (IOException ex) {
            System.err.println("Error al escribir la imagen " + ex.getMessage());
        } catch (NullPointerException ex) {
            System.err.println("Null al escribir o tomar la foto " + ex.getMessage());
        }
    }

    private void sendEmail(String[] to, String from, String body) {
        mail.setTo(to);
        mail.setFrom(from);
        mail.setSubject("Alerta de extraño");
        mail.setBody(body);

        if (mail.send()) {
            System.out.println("se envio el correo");
        } else {
            System.out.println("no se envio el correo");
        }
    }

}
