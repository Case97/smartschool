/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.util;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Dialog;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 *
 *
 * @author 27gma
 */
public class Dialogs {
    
    public void IconDialog(Dialog dialog) {
        Stage stage = (Stage) dialog.getDialogPane().getScene().getWindow();
        IconStage(stage);
    }

    public void IconStage(Stage stage) {
        stage.getIcons().add(new Image(ImageClass.onlyLogo_A()));
    }

    public void Alerta(String title, String header, String content, AlertType type) {
        Alert alert = new Alert(type);
        alert.setTitle("SmartSchool");
        alert.setHeaderText(header);
        alert.setContentText(content);
        IconDialog(alert);
        alert.showAndWait();
    }

    public boolean Confirm(String title, String header, String content) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("SmartSchool");
        alert.setHeaderText(header);
        alert.setContentText(content);
        IconDialog(alert);    
        Optional<ButtonType> option = alert.showAndWait();

        return option.get() == ButtonType.OK;
    }

    public String choiceDialog(String title, String header, String content,List<String> letter)throws NoSuchElementException {
        ChoiceDialog<String> choiceDialog = new ChoiceDialog<>("", letter);
        choiceDialog.setTitle("SmartSchool");
        choiceDialog.setHeaderText(header);
        choiceDialog.setContentText(content);
        IconDialog(choiceDialog);
        Optional<String> result = choiceDialog.showAndWait();
        return result.orElse(null);
    }
}
