/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.model;

import com.developerfriends.edu.smartschool.pojo.Grades;
import com.developerfriends.edu.smartschool.util.Dialogs;
import com.developerfriends.edu.smartschool.util.UtilClass;
import java.util.Iterator;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Tanieska
 */
public class GradesModel implements EntitiesInterface<Grades> {

    Dialogs dialog = new Dialogs();
    UtilClass util = new UtilClass();
    private Session session;

    private int isSave;
    private int isUpdate;
    private int isDelete;    

    @Override
    public void save(Grades object) {
        Transaction tx = null;
        try {
            tx = getSession().beginTransaction();
            getSession().save(object);

            tx.commit();
            setIsSave(1);
            
        } catch (RuntimeException e) {
            if (tx != null) {
                tx.rollback();
            }
            setIsSave(0);
            throw e;
        } finally {
            closeSession();
        }
    }

    public boolean isExist(int gradeName) {
        assert getSession() != null;
        List results = getSession().createCriteria(Grades.class)
                .setProjection(Projections.projectionList()
                        .add(Projections.property("gradeId")) // 0
                )
                .add(Restrictions.eq("gradeName", gradeName))
                //.addOrder(Order.desc("name"))
                .list();
        return !results.isEmpty();
    }

    public ObservableList<Grades> readGrades(Boolean state) {
        assert getSession() != null;
        List results = getSession().createCriteria(Grades.class)
                .setProjection(Projections.projectionList()
                        .add(Projections.property("gradeId")) // 0
                        .add(Projections.property("gradeName"), "name") //1
                )
                .add(Restrictions.eq("state", state))
                .list();
        return setGrades(results);
    }

    public ObservableList<Grades> findGrades(Boolean state, String text) {
        assert getSession() != null;
        List results = getSession().createCriteria(Grades.class)
                .setProjection(Projections.projectionList()
                        .add(Projections.property("gradeId")) // 0
                        .add(Projections.property("gradeName"), "name") //1
                )
                .add(Restrictions.eq("state", state))
                .add(Restrictions.like("gradeName", getGradeName(text)))
                .list();
        return setGrades(results);
    }

    private ObservableList<Grades> setGrades(List results) throws NumberFormatException {
        ObservableList<Grades> grades = FXCollections.observableArrayList();
        try {

            Iterator i = results.iterator();

            while (i.hasNext()) {
                Object[] obj = (Object[]) i.next();

                grades.add(
                        new Grades(
                                (Integer.parseInt(String.valueOf(obj[0]))),
                                (Integer.parseInt(String.valueOf(obj[1]))))
                );

            }
        } catch (java.lang.ClassCastException ex) {
            System.err.println("Error en el setGrades " + ex.getMessage());
        }

        return grades;
    }

    @Override
    public void update(Grades object) {
        Transaction tx = null;
        try {
            tx = getSession().beginTransaction();
            getSession().update(object);

            tx.commit();
            setIsUpdate(1);

        } catch (RuntimeException e) {
            if (tx != null) {
                tx.rollback();
            }
            setIsUpdate(0);
            throw e;
        } finally {
            closeSession();
        }
    }

    @Override
    public void delete(Grades object) {
        Transaction tx = null;
        try {
            tx = getSession().beginTransaction();
            getSession().update(object);

            tx.commit();
            setIsDelete(1);

        } catch (RuntimeException e) {
            if (tx != null) {
                tx.rollback();
            }
            setIsDelete(0);
            throw e;
        } finally {
            closeSession();
        }
    }

    public void active(Grades object) {
        Transaction tx = null;
        try {
            tx = getSession().beginTransaction();
            getSession().update(object);

            tx.commit();
            dialog.Alerta("Completo", "", "Se habilitado ese grado", Alert.AlertType.INFORMATION);

        } catch (RuntimeException e) {
            if (tx != null) {
                tx.rollback();
            }
            dialog.Alerta("Error", "", "Sucedio un error al habilitar", Alert.AlertType.ERROR);
            throw e;
        } finally {
            closeSession();
        }
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public void closeSession() {
        if ((session != null) && (session.isOpen())) {
            session.close();
        }
    }

    private int getGradeName(String text) {
        try {
            return Integer.parseInt(text);
        } catch (NumberFormatException ex) {
            System.err.println("Error " + ex.getMessage());
            return 0;
        }
    }

    public int getIsSave() {
        return isSave;
    }

    public void setIsSave(int isSave) {
        this.isSave = isSave;
    }

    public int getIsUpdate() {
        return isUpdate;
    }

    public void setIsUpdate(int isUpdate) {
        this.isUpdate = isUpdate;
    }

    public int getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(int isDelete) {
        this.isDelete = isDelete;
    }
   
    
    

}
