/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.controller;

import com.crazycode.custom.elements.SearchField;
import com.developerfriends.edu.smartschool.model.SectionsClassroomsModel;
import com.developerfriends.edu.smartschool.pojo.Classrooms;
import com.developerfriends.edu.smartschool.pojo.Sections;
import com.developerfriends.edu.smartschool.pojo.SectionsClassrooms;
import com.developerfriends.edu.smartschool.util.Dialogs;
import com.developerfriends.edu.smartschool.util.HibernateUtil;
import com.developerfriends.edu.smartschool.util.ImageClass;
import com.developerfriends.edu.smartschool.util.StageController;
import com.developerfriends.edu.smartschool.util.UtilClass;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 * FXML Controller class
 *
 * @author Tanieska
 */
public class SectionsClassroomsFXMLController implements Initializable {

    ClassroomFXMLController controller = new ClassroomFXMLController();
    UtilClass util = new UtilClass();
    Dialogs dialog = new Dialogs();
    Classrooms classrooms = new Classrooms();
    SectionsClassroomsModel sectionsClassroomsModel = new SectionsClassroomsModel();
    StageController stController = new StageController();

    private Stage dialogStage;
    private SessionFactory sFactory;

    @FXML
    private TableView<Sections> tableSections;
    @FXML
    private TableColumn<Sections, String> colGrade;
    @FXML
    private TableColumn<Sections, String> colSection;
    @FXML
    private JFXRadioButton rbMorning;
    @FXML
    private JFXRadioButton rbEvening;
    @FXML
    private SearchField txtSearch;
    @FXML
    private JFXButton btnSave;
    @FXML
    private JFXButton btnCancel;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        Icons();

        getSessionFactory();

        colGrade.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(util.getGrade(cellData.getValue().getGrades().getGradeName())));
        colSection.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(util.getLetter(cellData.getValue().getLetter())));

        search();
        load();
    }

    public void Icons() {
        stController.Icon(btnSave, ImageClass.save);
        stController.Icon(btnCancel, ImageClass.cancel);
    }

    private void load() {
        try {
            sectionsClassroomsModel.setSession(openSession());
            tableSections.setItems(sectionsClassroomsModel.readSections());
        } catch (NullPointerException ex) {
            System.err.println("Error en load " + ex.getMessage());
        }
    }

    private void find(String text) {
        try {
            sectionsClassroomsModel.setSession(openSession());
            tableSections.setItems(sectionsClassroomsModel.findSections(text));
        } catch (NullPointerException ex) {
            System.err.println("Error en find " + ex.getMessage());
        }
    }

    @FXML
    private void save(ActionEvent event) {
        Sections selectSections = (Sections) tableSections.getSelectionModel().getSelectedItem();
        if (selectSections != null) {
            if (isEmpty()) {
                dialog.Alerta("Smart School", "Error", "Seleccione un turno", Alert.AlertType.ERROR);
            } else {
                SectionsClassrooms s = getSectionsClassrooms(selectSections);
                sectionsClassroomsModel.setSession(openSession());
                sectionsClassroomsModel.save(s);
                dialogStage.close();
                controller.loadSectionsClassroom(getClassrooms());
            }
        } else {
            dialog.Alerta("Advertencia", "No se ha encontrado la sección", "Por favor seleccione una sección en la tabla", Alert.AlertType.WARNING);
        }
    }

    private void search() {
        txtSearch.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (newValue.isEmpty()) {
                load();
            } else {
                find(newValue);
            }
        });
    }

    @FXML
    private void cancel(ActionEvent event) {
        dialogStage.close();
    }

    private boolean isEmpty() {
        return ((!this.rbMorning.isSelected())
                && (!this.rbEvening.isSelected()));
    }

    private SectionsClassrooms getSectionsClassrooms(Sections sections) {
        return new SectionsClassrooms(getClassrooms(), sections, getTurn(), 1);
    }

    @FXML
    private void checkMorning(ActionEvent event) {
        util.enableRB(rbMorning, rbEvening);
    }

    @FXML
    private void checkEvening(ActionEvent event) {
        util.enableRB(rbEvening, rbMorning);
    }

    protected void validRd(List<String> turn) {
        rbMorning.setDisable(turn.contains("Matutino"));
        rbEvening.setDisable(turn.contains("Vespertino"));
    }

    private int getTurn() {
        if (rbMorning.isSelected()) {
            return 0;
        } else {
            return 1;
        }
    }

    public Stage getDialogStage() {
        return dialogStage;
    }

    public void setStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    private Session openSession() {
        return sFactory.openSession();
    }

    private void getSessionFactory() {
        sFactory = HibernateUtil.getSessionFactory();
    }

    public void setController(ClassroomFXMLController controller) {
        this.controller = controller;
    }

    public Classrooms getClassrooms() {
        return classrooms;
    }

    public void setClassrooms(Classrooms classrooms) {
        this.classrooms = classrooms;
    }
}
