/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.model;

import com.developerfriends.edu.smartschool.pojo.Classrooms;
import com.developerfriends.edu.smartschool.pojo.Grades;
import com.developerfriends.edu.smartschool.pojo.Sections;
import com.developerfriends.edu.smartschool.pojo.SectionsClassrooms;
import com.developerfriends.edu.smartschool.util.Dialogs;
import com.developerfriends.edu.smartschool.util.UtilClass;
import java.util.Iterator;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Tanieska
 */
public class SectionsClassroomsModel implements EntitiesInterface<SectionsClassrooms> {

    Dialogs dialog = new Dialogs();

    UtilClass util = new UtilClass();

    private Session session;

    Classrooms classroom = new Classrooms();

    @Override
    public void save(SectionsClassrooms object) {
        Transaction tx = null;
        try {
            tx = getSession().beginTransaction();
            getSession().save(object);

            tx.commit();
            dialog.Alerta("Completo", "", "Se guardo correctamente el registro", Alert.AlertType.INFORMATION);

        } catch (RuntimeException e) {
            if (tx != null) {
                tx.rollback();
            }
            dialog.Alerta("Error", "", "Sucedio un error al guardar", Alert.AlertType.ERROR);
            throw e;
        } finally {
            closeSession();
        }

    }

    public Classrooms getClassrooms(int sectionsClassroomsId) {
        try {
            assert getSession() != null;
            classroom = (Classrooms) getSession().createCriteria(SectionsClassrooms.class).
                    setProjection(Projections.projectionList()
                            .add(Projections.property("classrooms")))
                    .add(Restrictions.eq("sectionClassroomId", sectionsClassroomsId)).uniqueResult();
        } catch (HibernateException ex) {
            dialog.Alerta("Error", "Error en usuario Revisar UserModel getPersonByUserId() ", ex.getMessage(), Alert.AlertType.ERROR);
        }
        return classroom;
    }

    public ObservableList<SectionsClassrooms> readSectionsClassrooms(Classrooms classrooms) {
        assert getSession() != null;

        List results = getSession().createCriteria(SectionsClassrooms.class, "sc")
                .createAlias("sections", "s")
                .createAlias("s.grades", "g")
                .setProjection(Projections.projectionList()
                        .add(Projections.property("s.sectionId")) // 0
                        .add(Projections.property("s.letter"), "name") //1
                        .add(Projections.property("g.gradeName")) //2
                        .add(Projections.property("sc.sectionClassroomId")) //3
                        .add(Projections.property("sc.turn")) //4
                )
                .add(Restrictions.eq("state", 1))
                .add(Restrictions.eq("sc.classrooms", classrooms))
                .list();
        return setSectionsClassrooms(results);
    }

    public ObservableList<SectionsClassrooms> readAllSectionsClassrooms() {
        assert getSession() != null;

        List results = getSession().createCriteria(SectionsClassrooms.class, "sc")
                .createAlias("sections", "s")
                .createAlias("s.grades", "g")
                .setProjection(Projections.projectionList()
                        .add(Projections.property("s.sectionId")) // 0
                        .add(Projections.property("s.letter"), "name") //1
                        .add(Projections.property("g.gradeName")) //2
                        .add(Projections.property("sc.sectionClassroomId")) //3
                        .add(Projections.property("sc.turn")) //4
                )
                .add(Restrictions.eq("state", 1))
                .list();
        return setSectionsClassrooms(results);
    }

    public ObservableList<SectionsClassrooms> findSectionsClassrooms(Classrooms classrooms, String text) {
        assert getSession() != null;

        List results = getSession().createCriteria(SectionsClassrooms.class, "sc")
                .createAlias("sections", "s")
                .createAlias("s.grades", "g")
                .setProjection(Projections.projectionList()
                        .add(Projections.property("s.sectionId")) // 0
                        .add(Projections.property("s.letter"), "name") //1
                        .add(Projections.property("g.gradeName")) //2
                        .add(Projections.property("sc.sectionClassroomId")) //3
                        .add(Projections.property("sc.turn")) //4
                )
                .add(Restrictions.eq("state", 1))
                .add(Restrictions.eq("sc.classrooms", classrooms))
                .add(Restrictions.like("g.gradeName", getGrade(text)))
                .list();
        return setSectionsClassrooms(results);
    }

    public ObservableList<SectionsClassrooms> findSectionsClassrooms( String text) {
        assert getSession() != null;

        List results = getSession().createCriteria(SectionsClassrooms.class, "sc")
                .createAlias("sections", "s")
                .createAlias("s.grades", "g")
                .setProjection(Projections.projectionList()
                        .add(Projections.property("s.sectionId")) // 0
                        .add(Projections.property("s.letter"), "name") //1
                        .add(Projections.property("g.gradeName")) //2
                        .add(Projections.property("sc.sectionClassroomId")) //3
                        .add(Projections.property("sc.turn")) //4
                )
                .add(Restrictions.eq("state", 1))
                .add(Restrictions.like("g.gradeName", getGrade(text)))
                .list();
        return setSectionsClassrooms(results);
    }

   public ObservableList<SectionsClassrooms> readSectionsClassroomsWhitoutTeacher(int subjectId) {
        assert getSession() != null;

        String sql = "select "
                + "s.sectionId, " //0
                + "g.gradeId, " //1 
                + "sc.section_ClassroomId, " //2
                + "c.classroomId, " //3
                + "c.codeClassroom, " //4
                + "g.gradeName, " //5
                + "s.letter," //6
                + "sc.turn " //7
                + "from classrooms c "
                + "inner join sections_classrooms sc "
                + "on sc.classroomId = c.classroomId "
                + "inner join sections s "
                + "on s.sectionId = sc.sectionId "
                + "inner join grades g "
                + "on s.gradeId = g.gradeId "
                + "where "
                + "sc.section_ClassroomId "
                + "not in "
                + "(select sct.section_ClassroomId "
                + "from sectionsclassrooms_teachers sct "
                + "inner join teachers_subjects ts "
                + "on ts.teacher_SubjectId = sct.teacherSubjectId "
                + "where ts.subjectId = " + subjectId
                + " )";

        SQLQuery query = getSession().createSQLQuery(sql);
        List<Object[]> results = query.list();

        return setSectionsClassroomsWhitoutTeacher(results);
    }

    public ObservableList<SectionsClassrooms> findSectionsClassroomWhitoutTeacher(int subjectId, String text) {
        assert getSession() != null;

        String sql = "select "
                + "s.sectionId, " //0
                + "g.gradeId, " //1 
                + "sc.section_ClassroomId, " //2
                + "c.classroomId, " //3
                + "c.codeClassroom, " //4
                + "g.gradeName, " //5
                + "s.letter," //6
                + "sc.turn " //7
                + "from classrooms c "
                + "inner join sections_classrooms sc "
                + "on sc.classroomId = c.classroomId "
                + "inner join sections s "
                + "on s.sectionId = sc.sectionId "
                + "inner join grades g "
                + "on s.gradeId = g.gradeId "
                + "where "
                + "sc.section_ClassroomId "
                + "not in "
                + "(select sct.section_ClassroomId "
                + "from sectionsclassrooms_teachers sct "
                + "inner join teachers_subjects ts "
                + "on ts.teacher_SubjectId = sct.teacherSubjectId "
                + "where ts.subjectId = "
                + subjectId
                + " ) "
                + "and c.codeClassroom like '"
                + text
                + "%'";

        SQLQuery query = getSession().createSQLQuery(sql);
        List<Object[]> results = query.list();

        return setSectionsClassroomsWhitoutTeacher(results);
    }
    
    private ObservableList<SectionsClassrooms> setSectionsClassrooms(List results) throws NumberFormatException {
        ObservableList<SectionsClassrooms> sectionsClassrooms = FXCollections.observableArrayList();
        try {

            Iterator i = results.iterator();

            while (i.hasNext()) {
                Object[] obj = (Object[]) i.next();

                Grades grades = new Grades(Integer.parseInt(String.valueOf(obj[2])));

                Sections sections = new Sections((Integer.parseInt(String.valueOf(obj[0]))),
                        grades,
                        (Integer.parseInt(String.valueOf(obj[1]))));

                sectionsClassrooms.add(
                        new SectionsClassrooms(
                                Integer.parseInt(String.valueOf(obj[3])),
                                sections,
                                Integer.parseInt(String.valueOf(obj[4])))
                );

            }
        } catch (java.lang.ClassCastException ex) {
            System.err.println("Error en el setSectionsClassrooms " + ex.getMessage());
        }

        return sectionsClassrooms;
    }

    private ObservableList<SectionsClassrooms> setSectionsClassroomsWhitoutTeacher(List results) throws NumberFormatException {
        ObservableList<SectionsClassrooms> sectionsClassrooms = FXCollections.observableArrayList();
        try {

            Iterator i = results.iterator();

            while (i.hasNext()) {
                Object[] obj = (Object[]) i.next();

                Grades grades = new Grades(Integer.parseInt(String.valueOf(obj[1])),
                        Integer.parseInt(String.valueOf(obj[5])));

                Sections sections = new Sections((Integer.parseInt(String.valueOf(obj[0]))),
                        grades,
                        (Integer.parseInt(String.valueOf(obj[6]))));

                Classrooms classrooms = new Classrooms(Integer.parseInt(String.valueOf(obj[3])),
                        String.valueOf(obj[4]));

                sectionsClassrooms.add(
                        new SectionsClassrooms(
                                Integer.parseInt(String.valueOf(obj[2])),
                                classrooms,
                                sections,
                                Integer.parseInt(String.valueOf(obj[7])))
                );

            }
        } catch (java.lang.ClassCastException ex) {
            System.err.println("Error en el setSectionsClassrooms " + ex.getMessage());
        }

        return sectionsClassrooms;
    }

    public ObservableList<Sections> readSections() {
        assert getSession() != null;

        String sql = "select "
                + "sectionId, "
                + "letter,"
                + "gradeName "
                + "from grades inner join sections "
                + "on grades.gradeId = sections.gradeId "
                + "where sectionId "
                + "not in(select sections_classrooms.sectionId from sections_classrooms "
                + "inner join sections on sections.sectionId=sections_classrooms.sectionId "
                + "where sections_classrooms.state=1) "
                + "and grades.state=1 "
                + "and sections.state=1";
        SQLQuery query = getSession().createSQLQuery(sql);
        List<Object[]> results = query.list();

        return setSections(results);
    }

    public ObservableList<Sections> findSections(String text) {
        assert getSession() != null;

        String sql = "select "
                + "sectionId, "
                + "letter, "
                + "gradeName, "
                + "sections.state "
                + "from grades "
                + "inner join sections "
                + "on grades.gradeId = sections.gradeId "
                + "where "
                + "sectionId "
                + "not in(select sections_classrooms.sectionId from sections_classrooms  "
                + "inner join sections on sections.sectionId=sections_classrooms.sectionId "
                + "where sections_classrooms.state=1) "
                + "and sections.state=1 "
                + "and gradeName = "
                + getGrade(text);

        SQLQuery query = getSession().createSQLQuery(sql);
        List<Object[]> results = query.list();

        return setSections(results);
    }

    private ObservableList<Sections> setSections(List results) throws NumberFormatException {
        ObservableList<Sections> sections = FXCollections.observableArrayList();
        try {

            Iterator i = results.iterator();

            while (i.hasNext()) {
                Object[] obj = (Object[]) i.next();

                Grades grades = new Grades(Integer.parseInt(String.valueOf(obj[2])));

                sections.add(new Sections((Integer.parseInt(String.valueOf(obj[0]))),
                        grades,
                        (Integer.parseInt(String.valueOf(obj[1]))))
                );

            }
        } catch (java.lang.ClassCastException ex) {
            System.err.println("Error en el setSections " + ex.getMessage());
        }

        return sections;
    }

    @Override
    public void update(SectionsClassrooms object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(SectionsClassrooms object) {
        Transaction tx = null;
        try {
            tx = getSession().beginTransaction();
            getSession().update(object);

            tx.commit();
            dialog.Alerta("Completo", "", "Se elimo el registro", Alert.AlertType.INFORMATION);

        } catch (RuntimeException e) {
            if (tx != null) {
                tx.rollback();
            }
            dialog.Alerta("Error", "", "Sucedio un error al eliminar", Alert.AlertType.ERROR);
            throw e;
        } finally {
            closeSession();
        }
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public void closeSession() {
        if ((session != null) && (session.isOpen())) {
            session.close();
        }
    }

    private int getGrade(String text) {
        try {
            return Integer.parseInt(text);
        } catch (NumberFormatException ex) {
            return 0;
        }
    }

}
