package com.developerfriends.edu.smartschool.pojo;
// Generated 07-01-2017 04:26:40 AM by Hibernate Tools 4.3.1

import java.util.HashSet;
import java.util.Set;

/**
 * Duty generated by hbm2java
 */
public class Duty implements java.io.Serializable {

    private Integer tariffId;
    private String nameTariff;
    private Float price;
    private Boolean state;
    private Set paymentses = new HashSet(0);

    public Duty() {
    }

    public Duty(String nameTariff, Float price, Boolean state, Set paymentses) {
        this.nameTariff = nameTariff;
        this.price = price;
        this.state = state;
        this.paymentses = paymentses;
    }

    public Duty(String nameTariff, Float price, Boolean state) {
        this.nameTariff = nameTariff;
        this.price = price;
        this.state = state;
    }

    public Duty(Integer tariffId, String nameTariff, Float price) {
        this.tariffId = tariffId;
        this.nameTariff = nameTariff;
        this.price = price;
    }
    
    public Integer getTariffId() {
        return this.tariffId;
    }

    public void setTariffId(Integer tariffId) {
        this.tariffId = tariffId;
    }

    public String getNameTariff() {
        return this.nameTariff;
    }

    public void setNameTariff(String nameTariff) {
        this.nameTariff = nameTariff;
    }

    public Float getPrice() {
        return this.price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Boolean getState() {
        return this.state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    public Set getPaymentses() {
        return this.paymentses;
    }

    public void setPaymentses(Set paymentses) {
        this.paymentses = paymentses;
    }

}
