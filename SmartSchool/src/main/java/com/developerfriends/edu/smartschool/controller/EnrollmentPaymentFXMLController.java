/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.controller;

import com.crazycode.mask.elements.IntegerField;
import com.developerfriends.edu.smartschool.model.AdvancePaymentsModel;
import com.developerfriends.edu.smartschool.model.DutyModel;
import com.developerfriends.edu.smartschool.pojo.Advancepayments;
import com.developerfriends.edu.smartschool.pojo.Duty;
import com.developerfriends.edu.smartschool.pojo.Employees;
import com.developerfriends.edu.smartschool.pojo.Payments;
import com.developerfriends.edu.smartschool.util.Dialogs;
import com.developerfriends.edu.smartschool.util.HibernateUtil;
import com.developerfriends.edu.smartschool.util.ImageClass;
import com.developerfriends.edu.smartschool.util.StageController;
import com.developerfriends.edu.smartschool.util.UtilClass;
import com.jfoenix.controls.JFXButton;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 * FXML Controller class
 *
 * @author Tanieska
 */
public class EnrollmentPaymentFXMLController implements Initializable {

    Dialogs dialog = new Dialogs();
    UtilClass util = new UtilClass();
    DutyModel dutyModel = new DutyModel();
    AdvancePaymentsModel advancePaymentsModel = new AdvancePaymentsModel();
    StageController stController = new StageController();

    private Stage dialogStage;
    private SessionFactory sFactory;
    Employees employees;
    Duty duty;
    private final Boolean ACTIVE = Boolean.TRUE;

    @FXML
    private TextField txtName;
    @FXML
    private TextField txtLastName;
    @FXML
    private Label lblPrice;
    @FXML
    private IntegerField txtPreEnrrollment;
    @FXML
    private JFXButton btnSave;
    @FXML
    private JFXButton btnCancel;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        Icons();

        getSessionFactory();
        dutyModel.setSession(openSession());

        duty = dutyModel.getDuty(1);
        enableTxt(true);
        this.lblPrice.setText(String.valueOf(duty.getPrice()));
    }

    public void Icons() {
        stController.Icon(btnSave, ImageClass.save);
        stController.Icon(btnCancel, ImageClass.cancel);
    }

    @FXML
    private void save(ActionEvent event) {
        if (isEmpty()) return;
        Advancepayments ap = getAdvancepayments();
        advancePaymentsModel.setSession(openSession());
        advancePaymentsModel.save(ap);
        if (advancePaymentsModel.getIsSave() == 1) {
            advancePaymentsModel.setSession(openSession());
            dialog.Alerta("Smart School", "Completo", "Se genero el pago con No. Factura "
                    + advancePaymentsModel.getLastId(), Alert.AlertType.INFORMATION);
        }
        getStage().close();
    }

    @FXML
    private void cancel(ActionEvent event) {
        getStage().close();
    }

    @FXML
    private void active(ActionEvent event) {
        advancePaymentsModel.setSession(openSession());
        Advancepayments advancepayments = advancePaymentsModel.getPreEnroollmentPayment(
                Integer.parseInt(this.txtPreEnrrollment.getText()));
        if (advancepayments != null) {
            this.txtName.setText(advancepayments.getFirstName());
            this.txtLastName.setText(advancepayments.getFirstLastname());
            enableTxt(false);
        }
    }

    private boolean isEmpty() {
        if (this.txtName.getText().isEmpty() || this.txtLastName.getText().isEmpty()) {
            dialog.Alerta("Smart School", "Error", "No puede dejar campos vacios", Alert.AlertType.ERROR);
            return true;
        }
        return false;
    }

    private Advancepayments getAdvancepayments() {
        return new Advancepayments(getPayments(),
                this.txtName.getText(), this.txtLastName.getText(),
                (Float.parseFloat(this.lblPrice.getText())),
                ACTIVE);
    }

    private Payments getPayments() {
        return new Payments(duty, getEmployees(), util.getDate());
    }

    private void enableTxt(boolean opc) {
        this.txtName.setDisable(opc);
        this.txtLastName.setDisable(opc);
    }

    public Stage getStage() {
        return dialogStage;
    }

    public void setStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    private Session openSession() {
        return sFactory.openSession();
    }

    private void getSessionFactory() {
        sFactory = HibernateUtil.getSessionFactory();
    }

    public Employees getEmployees() {
        return employees;
    }

    public void setEmployees(Employees employees) {
        this.employees = employees;
    }

}
