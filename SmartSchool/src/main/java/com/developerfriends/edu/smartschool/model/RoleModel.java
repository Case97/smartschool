/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.developerfriends.edu.smartschool.model;

import com.developerfriends.edu.smartschool.pojo.Role;
import com.developerfriends.edu.smartschool.pojo.Users;
import com.developerfriends.edu.smartschool.util.Dialogs;
import javafx.scene.control.Alert;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Enmnauel Calero
 */
public class RoleModel implements EntitiesInterface<Role> {

    private Session session;
    private Role role;
    Dialogs dialog = new Dialogs();

    public RoleModel() {
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public void closeSession() {
        if ((session != null) && (session.isOpen())) {
            session.close();
        }
    }

    @Override
    public void save(Role object) {
        Transaction transaction = null;
        try {
            transaction = getSession().beginTransaction();
            getSession().save(object);
            transaction.commit();
            dialog.Alerta("Completo", "", "Se guardo correctamente", Alert.AlertType.INFORMATION);
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            dialog.Alerta("Error", "", "Sucedio un error al guardar", Alert.AlertType.ERROR);
            throw e;
        }
    }

    @Override
    public void update(Role object) {
        Transaction tx = null;
        try {
            tx = getSession().beginTransaction();
            getSession().update(object);
            tx.commit();
            dialog.Alerta("Completo", "", "Se actualizo correctamente", Alert.AlertType.INFORMATION);

        } catch (RuntimeException e) {
            if (tx != null) {
                tx.rollback();
            }
            dialog.Alerta("Error", "", "Sucedio un error al actualizar", Alert.AlertType.ERROR);
            throw e;
        }
    }

    @Override
    public void delete(Role object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Role getRoleByUserId(int userId) {
        try {
            assert getSession() != null;
            role = (Role) getSession().createCriteria(Role.class).
                    createAlias("users", "users").
                    add(Restrictions.eq("users.userId", userId)).uniqueResult();
        } catch (HibernateException ex) {
            dialog.Alerta("Error", "Error en usuario Revisar UserModel getUser() ", ex.getMessage(), Alert.AlertType.ERROR);
        }
        return role;
    }

}
