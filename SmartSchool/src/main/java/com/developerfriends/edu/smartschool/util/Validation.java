package com.developerfriends.edu.smartschool.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Tanieska
 */
public class Validation {

    public Validation() {
    }

    public boolean isFloat(String value) {
        try {
            return Float.parseFloat(value) > 0;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    public boolean isInt(String value) {
        try {
            return Integer.parseInt(value) > 0;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    public boolean isIdentityCard(String value) {
        Pattern path = Pattern.compile("[\\d{2}]*[-]*[\\d{5}]*[-][\\d{3}]+[\\w]");
        Matcher match = path.matcher(value);
        return match.matches();
    }

    public boolean isPhone(String value) {
        Pattern path = Pattern.compile("[\\d{3}[-]*]*\\d{3}");
        Matcher match = path.matcher(value);

        return match.matches();
    }

    public boolean isEmail(String value) {
        Pattern path = Pattern.compile("^[\\w-]+(\\.[\\w-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher match = path.matcher(value);
        return match.matches();
    }

}
